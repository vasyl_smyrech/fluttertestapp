import 'package:bloc/bloc.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:perfect_day/core/presentation/widgets/inherited/app_drawer_provider.dart';

import 'core/data/repositories/task_repository/task_repository.dart';
import 'core/data/repositories/user_repository/user_repository.dart';
import 'core/presentation/blocs/blocs.dart';
import 'core/presentation/blocs/simple_bloc_delegate.dart';
import 'core/presentation/widgets/widgets.dart';
import 'features/distinct_task/presentation/screens/screens.dart';
import 'features/home/domain/models/models.dart';
import 'features/home/presentation/blocs/blocs.dart';
import 'features/home/presentation/screens/screens.dart';
import 'features/user_information/presentation/screens/screens.dart';
import 'features/user_verification/presentation/screens/screens.dart';

void main() {
  runApp(AppConfig(child: CustomTheme(child: PerfectDay())));

  BlocSupervisor.delegate = SimpleBlocDelegate();
}

class PerfectDay extends StatelessWidget {
  final _userRepository = FirebaseUserRepository();
  final _taskRepository = FirebaseTaskRepository();

  @override
  Widget build(BuildContext context) {
    return MultiRepositoryProvider(
      providers: [
        RepositoryProvider<UserRepository>(
            builder: (context) => _userRepository),
      ],
      child: MultiBlocProvider(
        providers: [
          BlocProvider<AuthenticationBloc>(
            builder: (context) {
              return AuthenticationBloc(
                userRepository: _userRepository,
              )..dispatch(AppStarted());
            },
          ),
          BlocProvider<TasksBloc>(
            builder: (context) {
              return TasksBloc(
                tasksRepository: _taskRepository,
              )..dispatch(LoadTasks());
            },
          ),
          BlocProvider<UserInfoBloc>(
              builder: (context) =>
                  UserInfoBloc(userRepository: _userRepository))
        ],
        child: AppDrawerProvider(
          drawerFieldTapped: (buildContext, intendedScreenRout) =>
              _navigateToIntendedScreen(buildContext, intendedScreenRout),
          child: MaterialApp(
            routes: {
              '/': (context) {
                return BlocBuilder<AuthenticationBloc, AuthenticationState>(
                  builder: (context, state) {
                    if (state is Authenticated) {
                      var userInfoBloc = BlocProvider.of<UserInfoBloc>(context);
                      userInfoBloc.dispatch(CurrentUserInfoLoad());
                      final tasksBloc = BlocProvider.of<TasksBloc>(context);
                      return MultiBlocProvider(
                        providers: [
                          BlocProvider<UpcomingTasksBloc>(
                            builder: (context) =>
                                UpcomingTasksBloc(tasksBloc: tasksBloc)
                                  ..dispatch(
                                      UpdateFilter(VisibilityFilter.upcoming)),
                          ),
                          BlocProvider<PastTasksBloc>(
                            builder: (context) => PastTasksBloc(
                                tasksBloc: tasksBloc)
                              ..dispatch(UpdateFilter(VisibilityFilter.past)),
                          ),
                          BlocProvider<FilteredTasksBloc>(
                              builder: (context) =>
                                  FilteredTasksBloc(tasksBloc: tasksBloc)),
                        ],
                        child: HomeScreen(
                          onCardTap: (taskId) =>
                              _navigateToTaskDetailsScreen(taskId, context),
                        ),
                      );
                    }
                    if (state is Unauthenticated) {
                      return LoginScreen(userRepository: _userRepository);
                    }
                    if (state is Loading) {
                      return AppBackground(
                          child: Center(child: LoadingIndicator.color(0)));
                    }
                    return AppBackground();
                  },
                );
              },
              TaskDetails.routeName: (context) {
                return RepositoryProvider<TaskRepository>(
                    builder: (context) => _taskRepository,
                    child: TaskDetails());
              },

              ProfileScreen.routeName: (context) {
                return MultiBlocProvider(
                  providers: [
                    BlocProvider<ProfileImageBloc>(
                        builder: (context) => ProfileImageBloc()),
                  ],
                  child: ProfileScreen(),
                );
              }

//              '/addTodo': (context) {
//                final tasksBloc = BlocProvider.of<TasksBloc>(context);
////    return AddEditScreen(
////    onSave: (task, note) {
////    tasksBloc.dispatch(
////    AddTodo(Todo(task, note: note)),
////    );
////    },
////    isEditing: false,
////    );
//              },
            },
            debugShowCheckedModeBanner: false,
            theme: CustomTheme.of(context),
//
//
//                  ),
          ),
        ),
      ),
    );
  }

  _navigateToTaskDetailsScreen(String taskId, BuildContext context) {
    Navigator.pushNamed(
      context,
      TaskDetails.routeName,
      arguments: TaskDetailsArguments(
        taskId,
      ),
    );
  }

  _navigateToIntendedScreen(BuildContext context, String intendedScreenRout) {
    Navigator.pushNamed(context, intendedScreenRout);
  }
}
