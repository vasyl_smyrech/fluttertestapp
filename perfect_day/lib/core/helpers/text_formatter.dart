import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class PhoneNumberTextInputFormatter extends TextInputFormatter {
  @override
  TextEditingValue formatEditUpdate(
      TextEditingValue oldValue, TextEditingValue newValue) {
    String newValueText = newValue.text.length > 11
        ? newValue.text.substring(0, 12)
        : newValue.text;
    final int newTextLength = newValueText.length;
    int selectionIndex = newValue.selection.end;
    int usedSubstringIndex = 0;
    final StringBuffer newText = StringBuffer();
    if (newTextLength > 0) {
      newText.write('+');
      if (newValue.selection.end > 0) selectionIndex++;
    }
    if (newTextLength > 2) {
      newText.write(newValueText.substring(0, usedSubstringIndex = 2) + ' ');
      if (newValue.selection.end > 2) selectionIndex += 1;
    }
    if (newTextLength > 5) {
      newText.write(newValueText.substring(2, usedSubstringIndex = 5) + ' ');
      if (newValue.selection.end > 5) selectionIndex += 1;
    }
    if (newTextLength > 8) {
      newText.write(newValueText.substring(5, usedSubstringIndex = 8) + ' ');
      if (newValue.selection.end > 8) selectionIndex += 1;
    }
    if (newTextLength > 10) {
      newText.write(newValueText.substring(8, usedSubstringIndex = 10) + ' ');
      if (newValue.selection.end > 10) selectionIndex += 1;
    }
    if (selectionIndex > 17) {
      selectionIndex = 17;
    }
    if (newTextLength >= usedSubstringIndex)
      newText.write(newValueText.substring(usedSubstringIndex));
    return TextEditingValue(
      text: newText.toString(),
      selection: TextSelection.collapsed(offset: selectionIndex),
    );
  }
}
