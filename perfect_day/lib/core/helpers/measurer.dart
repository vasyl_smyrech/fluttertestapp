import 'dart:ui';

import 'package:flutter/widgets.dart';

import '../domain/models/models.dart';
import '../presentation/widgets/widgets.dart';

class MeasurerSingleton {
  static final MeasurerSingleton _measurer = MeasurerSingleton._internal();

  factory MeasurerSingleton() {
    return _measurer;
  }

  MeasurerSingleton._internal();

  final double baseHeight = 640.0;
  final double baseWidth = 360.0;

  double screenAwareSize(double size, BuildContext context) {
    return size * MediaQuery.of(context).size.height / baseHeight;
  }

  double densityCoefficient(Window window, bool isOrientationPortrait) {
    var verticalHeight = isOrientationPortrait
        ? window.physicalSize.height / baseHeight
        : window.physicalSize.width / baseWidth;
    return verticalHeight;
  }

  Size getWidgetSize(GlobalKey key) {
    final RenderBox widgetRenderBox = key.currentContext.findRenderObject();
    final containerSize = widgetRenderBox.size;
    return containerSize;
  }

  Offset getWidgetPosition(GlobalKey key) {
    final RenderBox containerRenderBox = key.currentContext.findRenderObject();
    final containerPosition = containerRenderBox.localToGlobal(Offset.zero);
    return containerPosition;
  }

  void saveKeyboardHeight(BuildContext context) {
    final value = WidgetsBinding.instance.window.viewInsets.bottom;
    final isOrientationPortrait =
        MediaQuery.of(context).orientation == Orientation.portrait;
    if (value != 0 &&
        isOrientationPortrait &&
        value != AppConfig.of(context).keyboardPortraitHeight) {
      AppConfig.instanceOf(context)
          .changeAppConfig(AppConfigData(keyboardPortraitHeight: value));
    } else if (value != 0 &&
        !isOrientationPortrait &&
        value != AppConfig.of(context).keyboardLandscapeHeight) {
      AppConfig.instanceOf(context)
          .changeAppConfig(AppConfigData(keyboardLandscapeHeight: value));
    }
  }
}
