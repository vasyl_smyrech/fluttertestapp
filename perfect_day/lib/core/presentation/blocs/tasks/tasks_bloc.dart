import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';

import '../../../../core/data/repositories/task_repository/task_repository.dart';
import '../blocs.dart';

class TasksBloc extends Bloc<TasksEvent, TasksState> {
  final TaskRepository _tasksRepository;
  StreamSubscription _tasksSubscription;
  StreamSubscription _internetConnectivity;

  TasksBloc({@required TaskRepository tasksRepository})
      : assert(tasksRepository != null),
        _tasksRepository = tasksRepository;

  @override
  TasksState get initialState {
//    _internetConnectivity = InternetConnectivityService()
//        .connectionChange
//        .listen((hasConnection) {
//      if (hasConnection) LoadTasks();
//    });
    return TasksLoading();
  }

  @override
  Stream<TasksState> mapEventToState(TasksEvent event) async* {
    if (event is LoadTasks) {
      yield* _mapLoadTasksToState();
    } else if (event is AddTask) {
      yield* _mapAddTaskToState(event);
    } else if (event is UpdateTask) {
      yield* _mapUpdateTaskToState(event);
    } else if (event is DeleteTask) {
      yield* _mapDeleteTaskToState(event);
    } else if (event is ToggleAll) {
      yield* _mapToggleAllToState();
    } else if (event is ClearCompleted) {
      yield* _mapClearCompletedToState();
    } else if (event is TasksUpdated) {
      yield* _mapTasksUpdateToState(event);
    }
  }

  Stream<TasksState> _mapLoadTasksToState() async* {
    _tasksSubscription?.cancel();
    _tasksSubscription = _tasksRepository.tasks().listen(
      (tasks) {
        dispatch(TasksUpdated(tasks));
      },
    );
  }

  Stream<TasksState> _mapAddTaskToState(AddTask event) async* {
    _tasksRepository.addNewTask(event.task);
  }

  Stream<TasksState> _mapUpdateTaskToState(UpdateTask event) async* {
    _tasksRepository.updateTask(event.updatedTask);
  }

  Stream<TasksState> _mapDeleteTaskToState(DeleteTask event) async* {
    _tasksRepository.deleteTask(event.task);
  }

  Stream<TasksState> _mapToggleAllToState() async* {
    final state = currentState;
    if (state is TasksLoaded) {
      final allComplete = state.tasks.every((task) => task.completed);
      final List<Task> updatedTasks = state.tasks
          .map((task) => task.copyWith(completed: !allComplete))
          .toList();
      updatedTasks.forEach((updatedTask) {
        _tasksRepository.updateTask(updatedTask);
      });
    }
  }

  Stream<TasksState> _mapClearCompletedToState() async* {
    final state = currentState;
    if (state is TasksLoaded) {
      final List<Task> completedTasks =
          state.tasks.where((task) => task.completed).toList();
      completedTasks.forEach((completedTask) {
        _tasksRepository.deleteTask(completedTask);
      });
    }
  }

  Stream<TasksState> _mapTasksUpdateToState(TasksUpdated event) async* {
    yield TasksLoaded(event.tasks);
  }

  @override
  void dispose() {
    _internetConnectivity?.cancel();
    _tasksSubscription?.cancel();
    super.dispose();
  }
}
