import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

import '../../../../core/data/repositories/task_repository/task_repository.dart';

@immutable
abstract class TasksEvent extends Equatable {
  TasksEvent([List props = const []]) : super(props);
}

class LoadTasks extends TasksEvent {
  @override
  String toString() => 'LoadTasks';
}

class AddTask extends TasksEvent {
  final Task task;

  AddTask(this.task) : super([task]);

  @override
  String toString() => 'AddTask { task: $task }';
}

class UpdateTask extends TasksEvent {
  final Task updatedTask;

  UpdateTask(this.updatedTask) : super([updatedTask]);

  @override
  String toString() => 'UpdateTask { updatedTask: $updatedTask }';
}

class DeleteTask extends TasksEvent {
  final Task task;

  DeleteTask(this.task) : super([task]);

  @override
  String toString() => 'DeleteTask { task: $task }';
}

class ClearCompleted extends TasksEvent {
  @override
  String toString() => 'ClearCompleted';
}

class ToggleAll extends TasksEvent {
  @override
  String toString() => 'ToggleAll';
}

class TasksUpdated extends TasksEvent {
  final List<Task> tasks;

  TasksUpdated(this.tasks);

  @override
  String toString() => 'TasksUpdated';
}
