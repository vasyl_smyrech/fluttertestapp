import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

import '../../../../core/data/repositories/task_repository/task_repository.dart';

@immutable
abstract class TasksState extends Equatable {
  TasksState([List props = const []]) : super(props);
}

class TasksLoading extends TasksState {
  @override
  String toString() => 'TasksLoading';
}

class TasksLoaded extends TasksState {
  final List<Task> tasks;

  TasksLoaded([this.tasks = const []]) : super([tasks]);

  @override
  String toString() => 'TasksLoaded { tasks: $tasks }';
}

class TasksNotLoaded extends TasksState {
  @override
  String toString() => 'TasksNotLoaded';
}
