import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

@immutable
abstract class TaskCompletionState extends Equatable {
  TaskCompletionState([List props = const []]) : super(props);
}

class TaskCompletionLoaded extends TaskCompletionState {
  final double completion;

  TaskCompletionLoaded(this.completion) : super([completion]);

  @override
  String toString() => 'TaskCompletionLoaded {completion: $completion}';
}

class TaskCompletionLoading extends TaskCompletionState {
  @override
  String toString() => 'TaskCompletionLoading';
}

class TaskCompletionInitial extends TaskCompletionState {
  @override
  String toString() => 'TaskCompletionInitial';
}

class TaskCompletionUpdated extends TaskCompletionState {
  @override
  String toString() => 'TaskCompletionLoaded';
}
