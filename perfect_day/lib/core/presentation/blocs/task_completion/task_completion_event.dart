import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

@immutable
abstract class TaskCompletionEvent extends Equatable {
  TaskCompletionEvent([List props = const []]) : super(props);
}

class TaskCompletionLoad extends TaskCompletionEvent {
  @override
  String toString() => 'TaskCompletionLoad';
}

class TaskCompletionUpdate extends TaskCompletionEvent {
  final double completion;

  TaskCompletionUpdate(this.completion) : super([completion]);

  @override
  String toString() => 'TaskCompletionUpdate { completion: $completion }';
}
