import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:flutter/cupertino.dart';
import 'package:meta/meta.dart';

import '../../../../core/data/repositories/task_repository/task_repository.dart';
import '../blocs.dart';

class TaskCompletionBloc
    extends Bloc<TaskCompletionEvent, TaskCompletionState> {
  final TaskRepository _taskRepository;
  final String _taskId;
  StreamSubscription _subTasksSubscription;

  TaskCompletionBloc(
      {@required TaskRepository taskRepository, @required String taskId})
      : assert(taskRepository != null),
        assert(taskId != null),
        _taskRepository = taskRepository,
        _taskId = taskId;

  @override
  TaskCompletionState get initialState => TaskCompletionLoading();

  @override
  Stream<TaskCompletionState> mapEventToState(
      TaskCompletionEvent event) async* {
    if (event is TaskCompletionLoad) {
      yield* _mapTaskCompletionLoadState(event);
    }
    if (event is TaskCompletionUpdate) {
      yield* _mapTaskCompletionUpdateState(event);
    }
  }

  Stream<TaskCompletionState> _mapTaskCompletionLoadState(
    TaskCompletionLoad event,
  ) async* {
    int subTasksCompletedLength = 0;
    int overallLength = 0;
    double completion;

    _taskRepository.subTasksForTask(_taskId).listen((subTasks) {
      subTasks.forEach((item) {
        overallLength++;

        if (item.isCompleted == true) subTasksCompletedLength++;
      });

      if (subTasksCompletedLength == 0) {
        completion = 0;
        subTasksCompletedLength = 0;
        overallLength = 0;
      } else {
        completion = subTasksCompletedLength / overallLength;
        subTasksCompletedLength = 0;
        overallLength = 0;
      }
      dispatch(
        TaskCompletionUpdate(completion),
      );
    });
  }

  Stream<TaskCompletionState> _mapTaskCompletionUpdateState(
    TaskCompletionUpdate event,
  ) async* {
    yield TaskCompletionLoaded(event.completion);
  }

  @override
  void dispose() {
    _subTasksSubscription?.cancel();
    super.dispose();
  }
}
