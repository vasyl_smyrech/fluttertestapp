import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

@immutable
abstract class UserInfoState extends Equatable {
  UserInfoState([List props = const []]) : super(props);
}

class UserInfoLoading extends UserInfoState {
  @override
  String toString() => 'UserInfoLoading';
}

class UserInfoLoaded extends UserInfoState {
  final String id;
  final String fullName;
  final String email;
  final String phone;
  final String imageUrl;
  final String location;
  final String about;
  final DateTime joinedDate;

  UserInfoLoaded(
      {this.id,
      this.fullName,
      this.email,
      this.phone,
      this.imageUrl,
      this.location,
      this.about,
      this.joinedDate})
      : super([
          id,
          fullName,
          email,
          phone,
          imageUrl,
          location,
          about,
          joinedDate
        ]);

  @override
  String toString() =>
      'UserInfoLoaded {id: $id, fullName: $fullName, email: $email, phone: $phone, '
      'imageUrl: $imageUrl, location: $location, about: $about, joinedDate: $joinedDate}';
}

class UserInfoLoadingFailed extends UserInfoState {
  @override
  String toString() => 'UserInfoLoadingFailed';
}
