import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';

import '../../../../core/data/repositories/user_repository/user_repository.dart';
import '../blocs.dart';

class UserInfoBloc extends Bloc<UserInfoEvent, UserInfoState> {
  final UserRepository _userRepository;

  UserInfoBloc({@required UserRepository userRepository})
      : assert(userRepository != null),
        _userRepository = userRepository;

  @override
  UserInfoState get initialState => UserInfoLoading();

  @override
  Stream<UserInfoState> mapEventToState(UserInfoEvent event) async* {
    if (event is CurrentUserInfoLoad) yield* _mapUserInfoLoadToState(event);
  }

  Stream<UserInfoState> _mapUserInfoLoadToState(
    CurrentUserInfoLoad event,
  ) async* {
    UserAdditionalInfo userInfo;
    String uid;
    try {
      uid = await _userRepository.currentUserId;
      userInfo = await _userRepository.fetchUserAdditionalInfo(uid);
    } on NoSuchMethodError {
      yield UserInfoLoading();
      if (uid != null) {
        userInfo = await _addAndReturnUserInfo(uid);
        yield UserInfoLoaded(
            id: userInfo.id,
            fullName: userInfo.fullName,
            email: userInfo.email,
            phone: userInfo.phone,
            imageUrl: userInfo.imageUrl,
            location: userInfo.location,
            about: userInfo.about,
            joinedDate: userInfo.joinedDate);
      }
      yield UserInfoLoadingFailed();
    } catch (_) {
      yield UserInfoLoadingFailed();
    }

    yield UserInfoLoaded(
        id: userInfo.id,
        fullName: userInfo.fullName,
        email: userInfo.email,
        phone: userInfo.phone,
        imageUrl: userInfo.imageUrl,
        location: userInfo.location,
        about: userInfo.about,
        joinedDate: userInfo.joinedDate);
  }

  Future<UserAdditionalInfo> _addAndReturnUserInfo(String uid) async {
    var userInfo;
    var googleSignIn = _userRepository.googleSignIn;
    if (googleSignIn.currentUser != null) {
      userInfo = UserAdditionalInfo(
          id: uid,
          fullName: googleSignIn.currentUser.displayName,
          imageUrl: googleSignIn.currentUser.photoUrl,
          joinedDate: DateTime.now(),
          email: googleSignIn.currentUser.email);
    } else {
      var userEmail = await _userRepository.currentUser;
      userInfo = UserAdditionalInfo(
          id: uid,
          fullName: userEmail,
          imageUrl: "https://firebasestorage.googleapis.com/v0/b/"
              "perfectly-9b4ab.appspot.com/o/"
              "user.png?alt=media&token=c9420427-f659-4e78-99a3-b0ad7bada6ad",
          joinedDate: DateTime.now(),
          email: userEmail);
    }
    try {
      await _userRepository.addCurrentUserAdditionalInfo(userInfo);
    } catch (_) {
      rethrow;
    }

    return userInfo;
  }
}
