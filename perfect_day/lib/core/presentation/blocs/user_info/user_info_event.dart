import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

import '../../../../core/data/repositories/user_repository/user_repository.dart';

@immutable
abstract class UserInfoEvent extends Equatable {
  UserInfoEvent([List props = const []]) : super(props);
}

class CurrentUserInfoLoad extends UserInfoEvent {
  CurrentUserInfoLoad();

  @override
  String toString() => 'CurrentUserInfoLoad';
}

class UserInfoLoad extends UserInfoEvent {
  final String userId;

  UserInfoLoad(this.userId) : super([userId]);

  @override
  String toString() => 'CurrentUserInfoLoad { userId: $userId}';
}

class UserInfoChange extends UserInfoEvent {
  final UserAdditionalInfo userAdditionalInfo;

  UserInfoChange(this.userAdditionalInfo) : super([userAdditionalInfo]);

  @override
  String toString() =>
      'UserInfoChange { userAdditionalInfo :$userAdditionalInfo }';
}
