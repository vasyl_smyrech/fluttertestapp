import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

import '../../../domain/models/models.dart';

@immutable
abstract class PreferencesState extends Equatable {
  PreferencesState([List props = const []]) : super(props);
}

class PreferencesLoading extends PreferencesState {
  @override
  String toString() => 'PreferencesLoading';
}

class AppThemeLoading extends PreferencesState {
  @override
  String toString() => 'AppThemeLoading';
}

class AppThemeLoaded extends PreferencesState {
  final AppThemeKey appThemeKey;

  AppThemeLoaded([this.appThemeKey]) : super([appThemeKey]);

  @override
  String toString() => 'AppThemeLoaded { appThemeKey: $appThemeKey }';
}

class AppThemeLoadingFailed extends PreferencesState {
  final AppThemeKey defaultAppThemeKey;

  AppThemeLoadingFailed([this.defaultAppThemeKey])
      : super([defaultAppThemeKey]);

  @override
  String toString() =>
      'AppThemeLoadingFailed { defaultAppThemeKey: $defaultAppThemeKey}';
}

class AppThemeSet extends PreferencesState {
  final AppThemeKey appThemeKey;

  AppThemeSet([this.appThemeKey]) : super([appThemeKey]);

  @override
  String toString() => 'AppThemeSet';
}

class AppThemeSetFailed extends PreferencesState {
  @override
  String toString() => 'AppThemeSetFailed';
}

class AppConfigLoaded extends PreferencesState {
  final AppConfigData appConfigData;

  AppConfigLoaded([this.appConfigData]) : super([appConfigData]);

  @override
  String toString() => 'AppConfigLoaded { appConfigData: $appConfigData }';
}

class AppConfigLoadingFailed extends PreferencesState {
  final AppConfigData defaultAppConfigData;

  AppConfigLoadingFailed([this.defaultAppConfigData])
      : super([defaultAppConfigData]);

  @override
  String toString() =>
      'AppConfigLoadingFailed { defaultAppConfigData: $defaultAppConfigData}';
}

class AppConfigSet extends PreferencesState {
  final AppConfigData appConfigData;

  AppConfigSet([this.appConfigData]) : super([appConfigData]);

  @override
  String toString() => 'AppConfigSet { appConfigData: $appConfigData }';
}

class AppConfigSetFailed extends PreferencesState {
  final AppConfigData appConfigData;

  AppConfigSetFailed([this.appConfigData]) : super([appConfigData]);

  @override
  String toString() => 'AppConfigSetFailed { appConfigData: $appConfigData }';
}
