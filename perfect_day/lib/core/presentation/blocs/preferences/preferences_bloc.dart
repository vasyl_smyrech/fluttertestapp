import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:enum_to_string/enum_to_string.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../../../domain/models/models.dart';
import '../blocs.dart';

class PreferencesBloc extends Bloc<PreferencesEvent, PreferencesState> {
  SharedPreferences _preferences;
  static AppConfigData _currentAppConfigData;

  @override
  PreferencesState get initialState {
    return PreferencesLoading();
  }

  @override
  Stream<PreferencesState> mapEventToState(PreferencesEvent event) async* {
    _preferences ??= await SharedPreferences.getInstance();

    if (event is LoadAppConfig) yield* _mapLoadAppConfigToState(event);
    if (event is SetAppConfig) yield* _mapSetAppConfigToState(event);
  }

  Stream<PreferencesState> _mapLoadAppConfigToState(
    LoadAppConfig event,
  ) async* {
    if (_currentAppConfigData != null) {
      yield AppConfigLoaded(_currentAppConfigData);
    }

    AppConfigData appConfigData;

    try {
      var appThemeKeyFromPreferences = _preferences.getString('appThemeKey');
      var appThemeKey = appThemeKeyFromPreferences == null
          ? AppThemeKey.standard
          : await EnumToString.fromString(
              AppThemeKey.values, appThemeKeyFromPreferences);
      var appLanguageFromPreferences = _preferences.getString('appLanguage');
      var appLanguage = appLanguageFromPreferences == null
          ? AppLanguages.english
          : await EnumToString.fromString(
              AppLanguages.values, appLanguageFromPreferences);
      appConfigData = AppConfigData(
          appThemeKey: appThemeKey,
          keyboardLandscapeHeight:
              _preferences.getDouble('keyboardLandscapeHeight'),
          keyboardPortraitHeight:
              _preferences.getDouble('keyboardPortraitHeight'),
          appLanguage: appLanguage);
      _currentAppConfigData = appConfigData;
      yield AppConfigLoaded(appConfigData);
    } catch (_) {
      appConfigData = AppConfigData(appThemeKey: AppThemeKey.standard);
      _currentAppConfigData = appConfigData;
      yield AppConfigLoadingFailed(appConfigData);
    }
  }

  Stream<PreferencesState> _mapSetAppConfigToState(
    SetAppConfig event,
  ) async* {
    try {
      if (event.appConfigData.appThemeKey != null) {
        await _preferences.setString(
            'appThemeKey', EnumToString.parse(event.appConfigData.appThemeKey));
      }
      if (event.appConfigData.keyboardLandscapeHeight != null) {
        await _preferences.setDouble('keyboardLandscapeHeight',
            event.appConfigData.keyboardLandscapeHeight);
      }
      if (event.appConfigData.keyboardPortraitHeight != null) {
        await _preferences.setDouble('keyboardPortraitHeight',
            event.appConfigData.keyboardPortraitHeight);
      }
      if (event.appConfigData.appLanguage != null) {
        await _preferences.setString(
            'appLanguage', EnumToString.parse(event.appConfigData.appLanguage));
      }
      _currentAppConfigData = AppConfigData(
          appThemeKey: event.appConfigData.appThemeKey ??
              _currentAppConfigData.appThemeKey,
          keyboardLandscapeHeight:
              event.appConfigData.keyboardLandscapeHeight ??
                  _currentAppConfigData.keyboardLandscapeHeight,
          keyboardPortraitHeight: event.appConfigData.keyboardPortraitHeight ??
              _currentAppConfigData.keyboardPortraitHeight,
          appLanguage: event.appConfigData.appLanguage ??
              _currentAppConfigData.appLanguage);
      yield AppConfigSet(_currentAppConfigData);
    } catch (_) {
      yield AppConfigSetFailed(_currentAppConfigData);
    }
  }
}
