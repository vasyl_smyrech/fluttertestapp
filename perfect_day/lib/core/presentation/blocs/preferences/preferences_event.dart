import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

import '../../../domain/models/models.dart';

@immutable
abstract class PreferencesEvent extends Equatable {
  PreferencesEvent([List props = const []]) : super(props);
}

class LoadAppConfig extends PreferencesEvent {
  @override
  String toString() => 'LoadAppConfig';
}

class SetAppConfig extends PreferencesEvent {
  final AppConfigData appConfigData;

  SetAppConfig(this.appConfigData) : super([appConfigData]);

  @override
  String toString() => 'SetAppConfig {appConfigData: $appConfigData}';
}

class LoadAppTheme extends PreferencesEvent {
  @override
  String toString() => 'LoadAppTheme';
}

class SetAppTheme extends PreferencesEvent {
  final AppThemeKey appThemeKey;

  SetAppTheme(this.appThemeKey) : super([appThemeKey]);

  @override
  String toString() => 'SetAppTheme {appThemeKey: $appThemeKey}';
}
