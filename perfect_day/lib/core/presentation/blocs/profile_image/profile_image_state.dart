import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

@immutable
abstract class ProfileImageState extends Equatable {
  ProfileImageState([List props = const []]) : super(props);
}

class ProfileImageLoading extends ProfileImageState {
  @override
  String toString() => 'ImageLoading';
}

class ProfileImageLoaded extends ProfileImageState {
  final String imagePath;

  ProfileImageLoaded([this.imagePath]) : super([imagePath]);

  @override
  String toString() => 'ProfileImageLoaded { imagePath: $imagePath }';
}

class ProfileImageNotLoaded extends ProfileImageState {
  @override
  String toString() => 'ImageNotLoaded';
}
