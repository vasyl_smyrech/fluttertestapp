import 'package:equatable/equatable.dart';
import 'package:flutter/cupertino.dart';
import 'package:meta/meta.dart';

@immutable
abstract class ProfileImageEvent extends Equatable {
  ProfileImageEvent([List props = const []]) : super(props);
}

class LoadProfileImage extends ProfileImageEvent {
  final String imageUrl;

  LoadProfileImage(this.imageUrl) : super([imageUrl]);

  @override
  String toString() => 'LoadProfileImage';
}

class ProfileImagePickerShow extends ProfileImageEvent {
  @override
  String toString() => 'ProfileImagePickerShow';
}

class ProfileImageCameraShow extends ProfileImageEvent {
  final BuildContext context;

  ProfileImageCameraShow(this.context) : super([context]);

  @override
  String toString() => 'ProfileImageCameraShow';
}
