import 'dart:async';
import 'dart:io';

import 'package:bloc/bloc.dart';
import 'package:camera_camera/page/camera.dart';
import 'package:camera_camera/shared/widgets/focus_widget.dart';
import 'package:file_picker/file_picker.dart';
import 'package:flutter/material.dart';

import '../../../../core/data/repositories/user_repository/user_repository.dart';
import '../../../domain/services/file_service.dart';
import '../blocs.dart';

class ProfileImageBloc extends Bloc<ProfileImageEvent, ProfileImageState> {
  final _fileService = FileService();
  final _userRepository = FirebaseUserRepository();

  @override
  ProfileImageState get initialState {
    return ProfileImageLoading();
  }

  @override
  Stream<ProfileImageState> mapEventToState(ProfileImageEvent event) async* {
    if (event is LoadProfileImage) yield* _mapLoadProfileImageToState(event);
    if (event is ProfileImagePickerShow)
      yield* _mapProfileImagePickerShowToState(event);
    if (event is ProfileImageCameraShow)
      yield* _mapProfileImageCameraShowToState(event);
  }

  Stream<ProfileImageState> _mapLoadProfileImageToState(
    LoadProfileImage event,
  ) async* {
    ProfileImageLoading();
    try {
      final imageFilePath = await _fileService.fetchFile(event.imageUrl);
      yield ProfileImageLoaded(imageFilePath);
    } catch (_) {
      yield ProfileImageNotLoaded();
    }
  }

  Stream<ProfileImageState> _mapProfileImagePickerShowToState(
    ProfileImagePickerShow event,
  ) async* {
    yield ProfileImageLoading();

    var filePath = await FilePicker.getFilePath(type: FileType.IMAGE);
    try {
      var imageUrl =
          await _fileService.saveFile(filePath, storageFolderPath: "images");
      _userRepository.updateCurrentUserAdditionalInfo(imageUrl: imageUrl);
      dispatch(LoadProfileImage(imageUrl));
    } catch (_) {
      yield ProfileImageNotLoaded();
    }
  }

  Stream<ProfileImageState> _mapProfileImageCameraShowToState(
      ProfileImageCameraShow event) async* {
    yield ProfileImageLoading();

    File file = await Navigator.push(
        event.context,
        MaterialPageRoute(
            builder: (context) => Camera(
                mode: CameraMode.normal,
                imageMask: CameraFocus.circle(
                  color: Colors.black.withOpacity(0.5),
                ),
                onFile: (file) {})));
    if (file == null) {
      yield ProfileImageNotLoaded();
    } else {
      try {
        var imageUrl =
            await _fileService.saveFile(file.path, storageFolderPath: "images");
        _userRepository.updateCurrentUserAdditionalInfo(imageUrl: imageUrl);
        dispatch(LoadProfileImage(imageUrl));
      } catch (_) {
        yield ProfileImageNotLoaded();
      }
    }
  }
}
