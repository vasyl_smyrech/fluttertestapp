import 'package:flutter/material.dart';

import '../widgets.dart';

class AppBackground extends StatelessWidget {
  final child;
  AppBackground({this.child});

  @override
  Widget build(BuildContext context) {
    var additionalThemeData =
        CustomTheme.instanceOf(context).additionalThemeData;
    if (additionalThemeData == null ||
        additionalThemeData.startAppBackgroundGradientColor == null ||
        additionalThemeData.endAppBackgroundGradientColor == null)
      return Container(
          color: CustomTheme.instanceOf(context).theme.backgroundColor,
          height: double.infinity,
          width: double.infinity,
          child: this.child);
    return Container(
        decoration: BoxDecoration(
          gradient: LinearGradient(
            begin: Alignment.topLeft,
            end: Alignment.bottomRight,
            colors: [
              CustomTheme.instanceOf(context)
                  .additionalThemeData
                  .startAppBackgroundGradientColor,
              CustomTheme.instanceOf(context)
                  .additionalThemeData
                  .endAppBackgroundGradientColor
            ],
          ),
        ),
        height: double.infinity,
        width: double.infinity,
        child: this.child);
  }
}
