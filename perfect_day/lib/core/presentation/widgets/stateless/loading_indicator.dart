import 'package:flutter/material.dart';

class LoadingIndicator extends StatelessWidget {
  final int _color;
  LoadingIndicator.color(this._color);

  @override
  Widget build(BuildContext context) {
    if (_color == 1) {
      return Center(
        child: CircularProgressIndicator(
          valueColor: AlwaysStoppedAnimation<Color>(Colors.red),
        ),
      );
    } else {
      return Center(
        child: CircularProgressIndicator(
          valueColor: AlwaysStoppedAnimation<Color>(Colors.white24),
        ),
      );
    }
  }
}
