import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../../domain/models/models.dart';
import '../../blocs/blocs.dart';

class _AppConfig extends InheritedWidget {
  final AppConfigState data;

  const _AppConfig({
    this.data,
    Key key,
    @required Widget child,
  }) : super(key: key, child: child);

  @override
  bool updateShouldNotify(_AppConfig oldWidget) {
    return true;
  }
}

class AppConfig extends StatefulWidget {
  final Widget child;

  const AppConfig({
    Key key,
    @required this.child,
  }) : super(key: key);

  @override
  AppConfigState createState() => AppConfigState();

  static AppConfigData of(BuildContext context) {
    _AppConfig inherited =
        (context.inheritFromWidgetOfExactType(_AppConfig) as _AppConfig);
    return inherited.data.appConfigData;
  }

  static AppConfigState instanceOf(BuildContext context) {
    _AppConfig inherited =
        (context.inheritFromWidgetOfExactType(_AppConfig) as _AppConfig);
    return inherited.data;
  }
}

class AppConfigState extends State<AppConfig> {
  AppConfigData _appConfigData;
  PreferencesBloc _preferencesBloc = PreferencesBloc();

  AppConfigData get appConfigData => _appConfigData;

  void changeAppConfig(AppConfigData appConfigData) {
    var appConfig = AppConfigData(
        appThemeKey: appConfigData.appThemeKey ?? _appConfigData.appThemeKey,
        keyboardLandscapeHeight: appConfigData.keyboardLandscapeHeight ??
            _appConfigData.keyboardLandscapeHeight,
        keyboardPortraitHeight: appConfigData.keyboardPortraitHeight ??
            _appConfigData.keyboardPortraitHeight,
        appLanguage: appConfigData.appLanguage ?? _appConfigData.appLanguage);
    _preferencesBloc.dispatch(SetAppConfig(appConfig));
  }

  @override
  Widget build(BuildContext context) {
    return BlocProvider<PreferencesBloc>(
      builder: (context) => _preferencesBloc..dispatch(LoadAppConfig()),
      child: BlocBuilder<PreferencesBloc, PreferencesState>(
          builder: (context, state) {
        if (state is AppConfigLoadingFailed) {
          _appConfigData = state.defaultAppConfigData;
          return _appConfig();
        } else if (state is AppConfigLoaded) {
          _appConfigData = state.appConfigData;
          return _appConfig();
        } else if (state is AppConfigSet) {
          _appConfigData = state.appConfigData;
          return _appConfig();
        } else if (state is AppConfigSetFailed) {
          _appConfigData = state.appConfigData;
          return _appConfig();
        }
        return Container();
      }),
    );
  }

  Widget _appConfig() => _AppConfig(
        data: this,
        child: widget.child,
      );

  @override
  void dispose() {
    _preferencesBloc.dispose();
    super.dispose();
  }
}
