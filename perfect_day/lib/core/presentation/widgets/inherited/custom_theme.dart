import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

import '../../../constants/constants.dart';
import '../../../domain/models/models.dart';
import '../widgets.dart';

class _CustomTheme extends InheritedWidget {
  final CustomThemeState data;

  const _CustomTheme({
    this.data,
    Key key,
    @required Widget child,
  }) : super(key: key, child: child);

  @override
  bool updateShouldNotify(_CustomTheme oldWidget) {
    return true;
  }
}

class CustomTheme extends StatefulWidget {
  final Widget child;

  const CustomTheme({
    Key key,
    @required this.child,
  }) : super(key: key);

  @override
  CustomThemeState createState() => CustomThemeState();

  static ThemeData of(BuildContext context) {
    _CustomTheme inherited =
        (context.inheritFromWidgetOfExactType(_CustomTheme) as _CustomTheme);
    return inherited.data.theme;
  }

  static CustomThemeState instanceOf(BuildContext context) {
    _CustomTheme inherited =
        (context.inheritFromWidgetOfExactType(_CustomTheme) as _CustomTheme);
    return inherited.data;
  }
}

class CustomThemeState extends State<CustomTheme> {
  ThemeData _theme;
  AdditionalThemeData _additionalThemeData;

  ThemeData get theme => _theme;
  AdditionalThemeData get additionalThemeData => _additionalThemeData;

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    var appThemes =
        AppThemes.receiveThemeForKey(AppConfig.of(context).appThemeKey);
    _theme = appThemes.themeData;
    _additionalThemeData = appThemes.additionalThemeData;
  }

  @override
  Widget build(BuildContext context) {
    return _CustomTheme(
      data: this,
      child: widget.child,
    );
  }
}
