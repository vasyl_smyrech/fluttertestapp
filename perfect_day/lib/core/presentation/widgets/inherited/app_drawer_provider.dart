import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

import '../widgets.dart';

class _AppDrawerProvider extends InheritedWidget {
  final AppDrawerProviderState data;

  const _AppDrawerProvider({
    this.data,
    Key key,
    @required Widget child,
  }) : super(key: key, child: child);

  @override
  bool updateShouldNotify(_AppDrawerProvider oldWidget) {
    return true;
  }
}

class AppDrawerProvider extends StatefulWidget {
  final Widget child;
  final Function(BuildContext, String) drawerFieldTapped;

  const AppDrawerProvider({
    Key key,
    @required this.child,
    this.drawerFieldTapped,
  }) : super(key: key);

  @override
  AppDrawerProviderState createState() => AppDrawerProviderState();

  static AppDrawer of(BuildContext context) {
    _AppDrawerProvider inherited =
        (context.inheritFromWidgetOfExactType(_AppDrawerProvider)
            as _AppDrawerProvider);
    inherited.data.parentWidget = context.widget;

    return inherited.data.drawer;
  }

  static AppDrawerProviderState instanceOf(BuildContext context) {
    _AppDrawerProvider inherited =
        (context.inheritFromWidgetOfExactType(_AppDrawerProvider)
            as _AppDrawerProvider);
    return inherited.data;
  }
}

class AppDrawerProviderState extends State<AppDrawerProvider> {
  Widget parentWidget;

  AppDrawer get drawer => AppDrawer(
      fieldTapped: widget.drawerFieldTapped, parentWidget: parentWidget);

  @override
  Widget build(BuildContext context) {
    return _AppDrawerProvider(
      data: this,
      child: widget.child,
    );
  }
}
