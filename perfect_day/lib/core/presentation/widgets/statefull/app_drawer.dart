import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:perfect_day/features/user_information/presentation/screens/profile.dart';

import '../../../../core/domain/models/models.dart';
import '../../blocs/blocs.dart';
import '../widgets.dart';

class AppDrawer extends StatefulWidget {
  final Function(BuildContext, String) fieldTapped;
  final Widget parentWidget;

  AppDrawer({Key key, this.fieldTapped, this.parentWidget}) : super(key: key);

  @override
  _AppDrawerState createState() => _AppDrawerState();
}

class _AppDrawerState extends State<AppDrawer> {
  final _profileImageBloc = ProfileImageBloc();
  AuthenticationBloc _authBloc;

  @override
  void initState() {
    super.initState();
    _authBloc = BlocProvider.of<AuthenticationBloc>(context);
  }

  @override
  void dispose() {
    _profileImageBloc.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Drawer(
        child: (Container(
            color: CustomTheme.of(context).primaryColorLight,
            child: ListView(
              children: <Widget>[
                BlocBuilder<UserInfoBloc, UserInfoState>(
                    builder: (context, state) {
                  if (state is UserInfoLoading) {
                    return Container(
                      height: 200.0,
                      child: LoadingIndicator.color(0),
                    );
                  } else if (state is UserInfoLoaded) {
                    return BlocProvider<ProfileImageBloc>(
                      builder: (context) => _profileImageBloc,
                      child: SignedCircleImage(
                          height: 250.0,
                          circleDiameter: 150.0,
                          signText: "${state.fullName} ",
                          imageUrl: "${state.imageUrl}"),
                    );
                  } else
                    return Container();
                }),
                SizedBox(height: 30.0),
                _createDrawerListTileItem("Past tasks", null,
                    onTap: () => {}, iconData: FontAwesomeIcons.archive),
                _createDrawerListTileItem("Statistic", null,
                    onTap: () => {}, iconData: FontAwesomeIcons.solidChartBar),
                _createDrawerListTileItem("Profile", ProfileScreen, onTap: () {
                  Navigator.of(context).pushNamedAndRemoveUntil(
                      '/', (Route<dynamic> route) => false);
                  widget.fieldTapped(context, ProfileScreen.routeName);
                }, iconData: FontAwesomeIcons.userCog),
                _createDrawerSwitchListTile("Night mode",
                    AppConfig.of(context).appThemeKey == AppThemeKey.dark,
                    onChanged: _changeAppMode),
                _createDrawerListTileItem("Log out", null, onTap: () {
                  _authBloc.dispatch(LoggedOut());
                  Navigator.of(context).pushNamedAndRemoveUntil(
                      '/', (Route<dynamic> route) => false);
                }, iconData: FontAwesomeIcons.signOutAlt),
              ],
            ))));
  }

  Widget _createDrawerSwitchListTile(String title, bool value,
          {Function(bool) onChanged}) =>
      SizedBox(
          height: 50.0,
          child: ListTile(
              title: Text(title,
                  style: TextStyle(
                      fontSize: 20.0,
                      fontWeight: FontWeight.w500,
                      fontFamily: "Montserrat")),
              selected: false,
              enabled: true,
              trailing: Switch(
                value: value,
                inactiveThumbColor:
                    CustomTheme.of(context).toggleableActiveColor,
                key: UniqueKey(),
                onChanged: (value) => onChanged(value),
              ),
              contentPadding: EdgeInsets.only(left: 22.0, right: 5.0)));

  Widget _createDrawerListTileItem(String title, Type screen,
          {Function onTap, IconData iconData}) =>
      SizedBox(
        height: 50.0,
        child: ListTile(
            title: Text(title,
                style: TextStyle(
                    fontSize: 20.0,
                    fontWeight: FontWeight.w500,
                    fontFamily: "Montserrat")),
            selected: false,
            enabled: _isCurrentScreenDrawerListItemInactive(screen),
            trailing: Icon(
              iconData,
            ),
            contentPadding: EdgeInsets.symmetric(horizontal: 22.0),
            onTap: () => onTap()),
      );

  _changeAppMode(bool changeToNightMode) {
    setState(() {
      if (changeToNightMode) {
        AppConfig.instanceOf(context)
            .changeAppConfig(AppConfigData(appThemeKey: AppThemeKey.dark));
      } else {
        AppConfig.instanceOf(context)
            .changeAppConfig(AppConfigData(appThemeKey: AppThemeKey.standard));
      }
    });
  }

  bool _isCurrentScreenDrawerListItemInactive(Type screenWidgetType) {
    return widget.parentWidget.runtimeType != screenWidgetType;
  }
}
