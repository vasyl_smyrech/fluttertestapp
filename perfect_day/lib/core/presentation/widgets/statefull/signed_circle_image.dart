import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../blocs/blocs.dart';
import '../widgets.dart';

class SignedCircleImage extends StatefulWidget {
  final String imageUrl;
  final String signText;
  final double circleDiameter;
  final double height;

  SignedCircleImage(
      {this.imageUrl,
      this.signText,
      this.circleDiameter = 160.0,
      this.height = 200.0,
      Key key})
      : super(key: key);

  @override
  _SignedCircleImageState createState() => _SignedCircleImageState();
}

class _SignedCircleImageState extends State<SignedCircleImage> {
  ProfileImageBloc _profileImageBloc;

  @override
  void initState() {
    super.initState();
    _profileImageBloc = BlocProvider.of<ProfileImageBloc>(context);
  }

  @override
  Widget build(BuildContext context) {
    _profileImageBloc.dispatch(LoadProfileImage(widget.imageUrl));

    return SizedBox(
      height: widget.height,
      child: Center(
        child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Padding(
                padding: EdgeInsets.only(top: 30.0, bottom: 16.0),
                child: BlocBuilder<ProfileImageBloc, ProfileImageState>(
                    builder: (context, state) {
                  if (state is ProfileImageLoading) {
                    return Container(
                        height: widget.circleDiameter,
                        alignment: Alignment.bottomCenter,
                        child: Center(child: LoadingIndicator.color(0)));
                  }

                  if (state is ProfileImageLoaded) {
                    if (state.imagePath.isEmpty) {
                      return Container();
                    }

                    return Container(
                        width: widget.circleDiameter,
                        height: widget.circleDiameter,
                        decoration: BoxDecoration(
                            shape: BoxShape.circle,
                            image: DecorationImage(
                                fit: BoxFit.cover,
                                image: FileImage(File(state.imagePath)))));
                  }
                  return Container(height: widget.circleDiameter);
                }),
              ),
              Padding(
                padding: EdgeInsets.symmetric(horizontal: 20.0),
                child: Text(
                  widget.signText,
                  style: TextStyle(
                      fontSize: 22.0,
                      height: 1.0,
                      letterSpacing: 0.0,
                      wordSpacing: 0.0,
                      fontWeight: FontWeight.w500,
                      fontFamily: "Montserrat"),
                  overflow: TextOverflow.ellipsis,
                  maxLines: 2,
                  textAlign: TextAlign.center,
                ),
              )
            ]),
      ),
    );
  }
}
