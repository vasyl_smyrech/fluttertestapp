import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

import '../../../data/repositories/task_repository/task_repository.dart';
import '../inherited/custom_theme.dart';

class TaskListItem extends StatefulWidget {
  final Task task;

  const TaskListItem({Key key, this.task}) : super(key: key);

  @override
  _TaskListItemState createState() => _TaskListItemState();
}

class _TaskListItemState extends State<TaskListItem> {
  String _subtasksProgress;


  @override
  Widget build(BuildContext context) {
    _setSubtasksProgress();

    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 4.0, horizontal: 8.0),
      child: ClipRRect(
        borderRadius: BorderRadius.circular(8.0),
        child: Container(
          decoration: BoxDecoration(
              boxShadow: [
                BoxShadow(
                    color: CustomTheme.instanceOf(context)
                        .additionalThemeData
                        .elevationColor,
                    offset: Offset(3.0, 5.0),
                    blurRadius: 14.0,
                    spreadRadius: 5.0),
              ],
                color:  CustomTheme.of(context).cardColor
          ),
          width: double.infinity,
          child: Row(
            children: <Widget>[
              Flexible(
                flex: 1,
                child: Stack(
                  alignment: Alignment.center,
                  children: <Widget> [
                    Image.network(
                      widget.task.imageUrl,
                      fit: BoxFit.fitHeight,
                    ),
                Positioned.fill(
                    child: Container(
                      alignment: Alignment.center,
                        color: Colors.black45,
                      child: Text(
                        "subtasksProgress",
                        textAlign: TextAlign.center,
                        style: TextStyle(
                            fontSize: 18.0,
                            letterSpacing: 4.0,
                            fontWeight: FontWeight.w600,
                            fontFamily: "Montserrat"),
                      ),

                    )),
                  ]
                ),
                ),
              Flexible(
                flex: 3,
                child: Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 8.0, vertical: 4.0),
                  child: Text(
                  widget.task.title,
                  style: TextStyle(

                  )),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }

  _setSubtasksProgress(){
   // _subtasksProgress = '${widget.task.}';
  }
}

