import 'package:flutter/material.dart';

import '../domain/models/models.dart';

class AppThemes {
  static final CustomThemeData _darkTheme = CustomThemeData(
    AppThemeKey.dark,
    ThemeData(
        dividerColor: Colors.white30,
        accentColorBrightness: Brightness.dark,
        // accentTextTheme: TextTheme(),
        textTheme: TextTheme(
          /// Color of the user full name in the Drawer,
          /// The percentage in a CircularPercentIndicator
          body1: TextStyle(color: Colors.white),

          /// Color of the Drawer items titles
          body2: TextStyle(color: Colors.white),

          /// The fields content text color at ProfileScreen
          caption: TextStyle(color: Colors.white70, fontSize: 14.5),

          /// Titles and subtitles color at profile screen
          subhead: TextStyle(color: Colors.white),
          display1: TextStyle(color: Colors.white30),
          display2: TextStyle(color: Colors.tealAccent),
          display3: TextStyle(color: Colors.orange),
          display4: TextStyle(color: Colors.black),
          overline: TextStyle(color: Colors.white12),
          headline: TextStyle(color: Colors.lightGreenAccent),
          title: TextStyle(color: Colors.red),
          button: TextStyle(color: Colors.purpleAccent),
          subtitle: TextStyle(color: Colors.black38),
        ),

        /// Cards background at the Profile screen
        backgroundColor: Color.fromARGB(255, 5, 3, 8),
        scaffoldBackgroundColor: Color.fromARGB(255, 5, 3, 8),
        toggleableActiveColor:
            Color.fromARGB(255, 17, 101, 212), // Switch toggle
        appBarTheme: AppBarTheme(color: Color.fromARGB(150, 55, 56, 56)),
        accentColor: Colors.white,
        primaryColor: Colors.blue,
        brightness: Brightness.dark,
        cardColor: Color.fromARGB(255, 63, 65, 67),
        primaryColorLight: Color.fromARGB(255, 70, 73, 75),
        bottomAppBarColor: Colors.black.withAlpha(150),
        canvasColor: Color.fromARGB(150, 55, 56, 56),
        colorScheme: ColorScheme(
            brightness: Brightness.dark,
            background: Colors.orange,
            onBackground: Colors.green,
            surface: Colors.blue,
            secondaryVariant: Colors.amber,
            primary: Colors.brown,
            onPrimary: Colors.cyan,
            onSurface: Colors.cyanAccent,
            secondary: Colors.green,
            onSecondary: Colors.indigo,
            primaryVariant: Colors.lightBlue,
            onError: Colors.purpleAccent,
            error: Colors.lime)),
    additionalThemeData: AdditionalThemeData(
        elevationColor: Color.fromARGB(255, 17, 101, 212),
        floatingActionButtonColor: Color.fromARGB(255, 200, 200, 200),
        bottomNavigationBarActiveIconColor: Colors.white,
        bottomNavigationBarDisableIconColor: Colors.white54,
        circularPercentIndicatorBackgroundColor: Colors.white24,
        circularPercentIndicatorProgressColor:
            Color.fromARGB(255, 17, 101, 212),
        profileScreenBackgroundColor: Color.fromARGB(255, 5, 3, 8),
        profileScreenCardBackgroundColor: Color.fromARGB(255, 70, 73, 75),
        autocompleteSuggestionsDividerColor: Colors.white54),
  );

  static final CustomThemeData _standardTheme = CustomThemeData(
    AppThemeKey.standard,
    ThemeData(
        dividerColor: Colors.black54,
        textTheme: TextTheme(
          /// Color of the user full name in the Drawer,
          /// The percentage in a CircularPercentIndicator
          body1: TextStyle(color: Colors.white),

          /// Color of the Drawer items titles
          body2: TextStyle(color: Colors.white),

          /// The fields content text color at ProfileScreen
          caption: TextStyle(color: Colors.black54, fontSize: 14.5),

          /// Titles and subtitles color at profile screen
          subhead: TextStyle(color: Colors.black),
          display1: TextStyle(color: Colors.white30),
          display2: TextStyle(color: Colors.tealAccent),
          display3: TextStyle(color: Colors.orange),
          display4: TextStyle(color: Colors.black),
          overline: TextStyle(color: Colors.white12),
          headline: TextStyle(color: Colors.lightGreenAccent),
          title: TextStyle(color: Colors.red),
          button: TextStyle(color: Colors.purpleAccent),
          subtitle: TextStyle(color: Colors.yellow),
        ),

        /// In this case inactiveThumbColor for Switch
        toggleableActiveColor: Colors.white,

        /// Switch toggle
        backgroundColor: Colors.white,
        scaffoldBackgroundColor: Color.fromARGB(255, 181, 51, 137),
        accentColor: Colors.black,
        brightness: Brightness.dark,
        primaryColor: Colors.deepPurple,
        unselectedWidgetColor: Colors.pink,
        textSelectionHandleColor: Colors.orangeAccent,
        bottomAppBarColor: Colors.teal,
        buttonColor: Colors.white,
        canvasColor: Colors.transparent,
        cardColor: Color.fromARGB(255, 98, 108, 192),
        cursorColor: Colors.orangeAccent,
        dialogBackgroundColor: Color.fromARGB(255, 181, 51, 137),
        highlightColor: Colors.white12,
        textSelectionColor: Colors.red.withAlpha(100),
        selectedRowColor: Colors.deepOrange,
        primaryColorLight: Color.fromARGB(255, 181, 51, 137),
        splashColor: Colors.white30,
        primaryColorDark: Colors.white,
        disabledColor: Colors.white30,
        secondaryHeaderColor: Colors.red,
        indicatorColor: Colors.orangeAccent,
        hintColor: Colors.grey,
        hoverColor: Colors.green,
        focusColor: Colors.blue,
        primaryColorBrightness: Brightness.dark,
        accentColorBrightness: Brightness.dark,
        iconTheme: IconThemeData(
          color: Colors.white,
        ),
        primaryIconTheme: IconThemeData(
          color: Colors.black38,
        ),
        colorScheme: ColorScheme(
            brightness: Brightness.dark,
            background: Colors.orange,
            onBackground: Colors.green,
            surface: Colors.blue,
            secondaryVariant: Colors.amber,
            primary: Colors.brown,
            onPrimary: Colors.cyan,
            onSurface: Colors.red,
            secondary: Color.fromARGB(255, 251, 122, 88),
            onSecondary: Colors.indigo,
            primaryVariant: Colors.lightBlue,
            onError: Colors.purpleAccent,
            error: Colors.lime)),
    additionalThemeData: AdditionalThemeData(
        startAppBackgroundGradientColor: Color.fromARGB(255, 165, 160, 233),
        endAppBackgroundGradientColor: Color.fromARGB(255, 102, 111, 196),
        startAppBarGradientColor: Color.fromARGB(150, 65, 60, 33),
        endAppBarGradientColor: Color.fromARGB(150, 2, 11, 96),
        elevationColor: Colors.black12,
        floatingActionButtonColor:
            Color.fromARGB(255, 251, 122, 88), //(0xFFfa7b58),
        bottomNavigationBarActiveIconColor: Colors.white,
        bottomNavigationBarDisableIconColor: Colors.white54,
        circularPercentIndicatorBackgroundColor: Colors.white24,
        circularPercentIndicatorProgressColor:
            Color.fromARGB(255, 181, 51, 137),
        profileScreenBackgroundColor: Color.fromARGB(255, 181, 51, 137),
        profileScreenCardBackgroundColor: Colors.white,
        autocompleteSuggestionsDividerColor: Colors.white),
  );

  static CustomThemeData receiveThemeForKey(AppThemeKey themeKey) {
    switch (themeKey) {
      case AppThemeKey.dark:
        return _darkTheme;
      case AppThemeKey.standard:
        return _standardTheme;
      default:
        return _standardTheme;
    }
  }
}
