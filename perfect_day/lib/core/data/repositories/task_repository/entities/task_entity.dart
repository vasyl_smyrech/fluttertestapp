import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:equatable/equatable.dart';

class TaskEntity extends Equatable {
  final String id;
  final String title;
  final String description;
  final bool completed;
  final int duration;
  final DateTime dueDateTime;
  final DateTime executionDateTime;
  final String imageUrl;
  final String userEmail;

  TaskEntity(
      this.id,
      this.title,
      this.description,
      this.completed,
      this.duration,
      this.dueDateTime,
      this.executionDateTime,
      this.imageUrl,
      this.userEmail);

  Map<String, Object> toJson() {
    return {
      "id": id,
      "title": title,
      "description": description,
      "completed": completed,
      "duration": duration,
      "dueDateTime": dueDateTime,
      "executionDateTime": executionDateTime,
      "imageUrl": imageUrl,
      "userEmail": userEmail
    };
  }

  @override
  String toString() {
    return 'TaskEntity{id: $id, title: $title, description: $description, completed: $completed, duration: $duration,'
        ' dueDateTime: $dueDateTime, executionDateTime: $executionDateTime, imageTemporaryFilePath: $imageUrl, userEmail: $userEmail}';
  }

  static TaskEntity fromJson(Map<String, Object> json) {
    return TaskEntity(
        json["id"] as String,
        json["title"] as String,
        json["description"] as String,
        json["completed"] as bool,
        json["duration"] as int,
        DateTime.fromMillisecondsSinceEpoch(
            (json["dueDateTime"] as Timestamp).millisecondsSinceEpoch),
        DateTime.fromMillisecondsSinceEpoch(
            (json["executionDateTime"] as Timestamp).millisecondsSinceEpoch),
        json["imageUrl"] as String,
        json["userEmail"] as String);
  }

  static TaskEntity fromSnapshot(DocumentSnapshot snap) {
    return TaskEntity(
        snap.documentID,
        snap.data['title'],
        snap.data['description'],
        snap.data['completed'],
        snap.data['duration'],
        DateTime.fromMillisecondsSinceEpoch(
            snap.data['dueDateTime'].millisecondsSinceEpoch),
        DateTime.fromMillisecondsSinceEpoch(
            snap.data['executionDateTime'].millisecondsSinceEpoch),
        snap.data['imageUrl'],
        snap.data['userEmail']);
  }

  Map<String, Object> toDocument() {
    return {
      "id": id,
      "title": title,
      "description": description,
      "completed": completed,
      "duration": duration,
      "dueDateTime": dueDateTime,
      "executionDateTime": executionDateTime,
      "imageUrl": imageUrl,
      "userEmail": userEmail
    };
  }
}
