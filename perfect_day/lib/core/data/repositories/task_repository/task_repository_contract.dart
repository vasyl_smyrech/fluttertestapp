import 'dart:async';

import 'package:cloud_firestore/cloud_firestore.dart';

import 'task_repository.dart';

abstract class TaskRepository {
  Future<void> addNewTask(Task task);

  Future<void> deleteTask(Task task);

  Stream<List<Task>> tasks();

  Stream<List<SubTask>> subTasksForTask(String taskId);

  Stream<QuerySnapshot> snapshots();

  Stream<QuerySnapshot> subTasksSnapshots(String taskId);

  Future<void> updateTask(Task task);

  Future<Task> getTask(String id);

  Future<SubTask> getSubTask({String taskId, String subTaskId});

  Future<Task> updateTaskTitle(String taskId, String title);

  Future<Task> updateTaskImage({String taskId, String fireBaseFileUrl});

  Future<Task> updateTaskDueDate(String taskId, DateTime date);

  Future<Task> updateTaskDuration(String taskId, int duration);

  Future<SubTask> updateSubTaskContent(
      {String taskId, String subTaskId, String content});

  Future<SubTask> updateSubTaskCompletion(
      {String taskId, String subTaskId, bool isCompleted});

  Future<SubTask> addSubTask(SubTask subTask, String taskId);

  Future<void> deleteSubTask(SubTask subTask, String taskId);

  Future<SubTask> updateSubTask(String taskId, SubTask subTask);
}
