import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../../../core/data/repositories/user_repository/user_repository.dart';
import '../../../../core/helpers/helpers.dart';
import '../../../../core/presentation/widgets/widgets.dart';
import '../blocs/blocs.dart';
import '../widgets/widgets.dart';

class RegisterScreen extends StatelessWidget {
  final UserRepository _userRepository;

  RegisterScreen({Key key, @required UserRepository userRepository})
      : assert(userRepository != null),
        _userRepository = userRepository,
        super(key: key);

  @override
  Widget build(BuildContext context) {
    var statusBarHeight = MediaQuery.of(context).padding.top;

    return Scaffold(
        body: BlocProvider<RegisterBloc>(
      builder: (context) => RegisterBloc(userRepository: _userRepository),
      child: ScrollConfiguration(
        behavior: WithoutGlowingBehavior(),
        child: ExtendedGlowingOverscrollIndicator(
            heightFactor: 0.4,
            startOpacity: 0.2,
            maxOpacity: 0.3,
            leadingOffset: statusBarHeight,
            color: Colors.black,
            axisDirection: AxisDirection.down,
            child: RegisterForm()),
      ),
    ));
  }
}
