import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

@immutable
abstract class RegisterEvent extends Equatable {
  RegisterEvent([List props = const []]) : super(props);
}

class RegistrationEmailChanged extends RegisterEvent {
  final String email;

  RegistrationEmailChanged({@required this.email}) : super([email]);

  @override
  String toString() => 'RegistrationEmailChanged { email :$email }';
}

class RegistrationPasswordChanged extends RegisterEvent {
  final String password;

  RegistrationPasswordChanged({@required this.password}) : super([password]);

  @override
  String toString() => 'RegistrationPasswordChanged { password: $password }';
}

class RegistrationSubmitted extends RegisterEvent {
  final String email;
  final String password;

  RegistrationSubmitted({@required this.email, @required this.password})
      : super([email, password]);

  @override
  String toString() {
    return 'RegistrationSubmitted { email: $email, password: $password }';
  }
}
