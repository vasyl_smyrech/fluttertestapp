class LocationPoint {
  final String city;
  final String country;
  final String subCountry;
  final String geoNameId;

  LocationPoint({this.city, this.country, this.subCountry, this.geoNameId});

  factory LocationPoint.fromJson(Map<String, dynamic> parsedJson) {
    return LocationPoint(
        city: parsedJson ['name'] as String,
        country: parsedJson['country'] as String,
        subCountry: parsedJson['subCountry'] as String,
        geoNameId: parsedJson['geoNameId'] as String);
  }
}
