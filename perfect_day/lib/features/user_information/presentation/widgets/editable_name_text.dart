import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../../../core/data/repositories/user_repository/user_repository.dart';
import '../blocs/blocs.dart';

class EditableNameText extends StatefulWidget {
  final String initialNameText;

  EditableNameText(this.initialNameText);

  @override
  _EditableNameTextState createState() => _EditableNameTextState();
}

class _EditableNameTextState extends State<EditableNameText> {
  final _node = FocusNode(debugLabel: 'Profile name');
  final _textEditingController = TextEditingController();
  ProfileFieldBloc _profileFieldBloc;

  @override
  void initState() {
    super.initState();
    _textEditingController.text = widget.initialNameText;
    _profileFieldBloc = ProfileFieldBloc(
        userRepository: RepositoryProvider.of<UserRepository>(context));
  }

  @override
  void dispose() {
    _node.dispose();
    _textEditingController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return BlocProvider<ProfileFieldBloc>(
      builder: (context) => _profileFieldBloc,
      child: BlocBuilder<ProfileFieldBloc, ProfileFieldState>(
          builder: (context, state) {
        if (state is ProfileFieldChanged) {
          _textEditingController.text = state.field;
        }
        if (state is ProfileFieldNotSaved) {
          _textEditingController.text = widget.initialNameText;
        }
        return _createProfileName();
      }),
    );
  }

  Widget _createProfileName() {
    return Align(
      alignment: Alignment.centerLeft,
      child: Padding(
        padding: EdgeInsets.only(left: 12.0, right: 12.0, bottom: 12.0),
        child: TextField(
          decoration: InputDecoration(
            contentPadding: const EdgeInsets.all(0.0),
            border: InputBorder.none,
            hintText: null,
          ),
          style: TextStyle(
            fontFamily: 'Montserrat',
            fontStyle: FontStyle.normal,
            fontSize: 22.0,
            fontWeight: FontWeight.w500,
          ),
          maxLines: null,
          autocorrect: true,
          enableInteractiveSelection: true,
          keyboardType: TextInputType.text,
          textInputAction: TextInputAction.done,
          focusNode: _node,
          textAlign: TextAlign.center,
          controller: _textEditingController,
          onSubmitted: (newValue) {
            _node.unfocus();
            _profileFieldBloc.dispatch(ProfileFullNameSave(newValue));
          },
          onTap: () {
            if (!_node.hasFocus) _node.requestFocus();
          },
          inputFormatters: <TextInputFormatter>[
            LengthLimitingTextInputFormatter(200)
          ],
        ),
      ),
    );
  }
}
