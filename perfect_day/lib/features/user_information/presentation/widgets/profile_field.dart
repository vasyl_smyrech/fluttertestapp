import 'package:autocomplete_textfield/autocomplete_textfield.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../../../core/data/repositories/user_repository/user_repository.dart';
import '../../../../core/domain/models/models.dart';
import '../../../../core/helpers/helpers.dart';
import '../../../../core/presentation/widgets/widgets.dart';
import '../../presentation/blocs/blocs.dart';

class ProfileField extends StatefulWidget {
  final ValueSetter<double> callback;
  final BuildContext parentContext;
  final ScrollController parentScrollController;
  final String fieldName;
  final String initialProfileFieldText;
  final IconData leadingIconData;
  final List<TextInputFormatter> inputFormatters;
  final TextInputType textInputType;
  final String warningMessage;
  final bool isAutocompleteTextView;
  final Function(String, ProfileFieldBloc) onChanged;
  final Function(String, ProfileFieldBloc) onSave;

  const ProfileField({
    Key key,
    this.callback,
    this.parentContext,
    this.parentScrollController,
    this.fieldName,
    this.initialProfileFieldText,
    this.leadingIconData,
    this.inputFormatters,
    this.textInputType,
    this.warningMessage,
    this.isAutocompleteTextView = false,
    this.onChanged,
    this.onSave,
  }) : super(key: key);

  @override
  _ProfileFieldState createState() => _ProfileFieldState();
}

class _ProfileFieldState extends State<ProfileField>
    with WidgetsBindingObserver {
  final _textEditingController = TextEditingController();
  final FocusNode _node = FocusNode(debugLabel: 'Profile field');
  final int _maxSuggestionsInPopup = 4;
  AdditionalThemeData _additionalThemeData;
  ProfileFieldBloc _profileFieldBloc;
  bool _isFieldTextValid = true;
  bool _shouldSuggestionsDividerBeShown = false;
  List<String> _suggestions;
  var _autocompleteItemBuilderContextHashCode;

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addObserver(this);
    _textEditingController.text = widget.initialProfileFieldText;
    _textEditingController.addListener(() {
      _shouldSuggestionsDividerBeShown = false;
    });
    _profileFieldBloc = ProfileFieldBloc(
        userRepository: RepositoryProvider.of<UserRepository>(context));
    widget.onChanged(widget.initialProfileFieldText, _profileFieldBloc);
  }

  @override
  void dispose() {
    WidgetsBinding.instance.removeObserver(this);
    _node.dispose();
    _textEditingController.dispose();
    super.dispose();
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    _additionalThemeData = CustomTheme.instanceOf(context).additionalThemeData;
  }

  @override
  void didChangeMetrics() {
    final value = WidgetsBinding.instance.window.viewInsets.bottom;
    if (value == 0 && _node.hasFocus) {
      widget.onSave(null, _profileFieldBloc);
      widget.callback(0.0);
      MeasurerSingleton().saveKeyboardHeight(context);
      _node.unfocus();
    }
  }

  @override
  Widget build(BuildContext context) {
    return BlocProvider<ProfileFieldBloc>(
      builder: (context) => _profileFieldBloc,
      child: BlocBuilder<ProfileFieldBloc, ProfileFieldState>(
          builder: (context, state) {
        if (state is ProfileFieldChanged &&
            state.field != _textEditingController.text) {
          _textEditingController.text = state.field;
        }
        if (state is ProfileFieldSaved) {
          _textEditingController.text = state.field;
          _isFieldTextValid = state.isFieldValid;
        }
        if (state is ProfileFieldNotSaved) {
          _textEditingController.text = state.field;
          _isFieldTextValid = state.isFieldValid;
        }
        return _createProfileField();
      }),
    );
  }

  Widget _createProfileField() {
    return ListTile(
      title: Text(
        widget.fieldName,
      ),
      subtitle: widget.isAutocompleteTextView
          ? _createProfileAutocompleteTextView()
          : _createProfileTextFormField(),
      leading: Icon(widget.leadingIconData,
          color: CustomTheme.of(context).primaryIconTheme.color),
    );
  }

  Widget _createProfileAutocompleteTextView() =>
      BlocBuilder<AutocompleteSuggestionsBloc, AutocompleteSuggestionsState>(
          builder: (context, state) {
        if (state is AutocompleteSuggestionsLoaded) {
          _suggestions = state.suggestions;
          var autocompleteKey = GlobalKey<AutoCompleteTextFieldState<String>>();
          return UniversalTapHandler(
            onTap: _scrollToAutocomplete,
            child: AutoCompleteTextField<String>(
              key: autocompleteKey,
              focusNode: _node,
              itemSubmitted: (text) {
                widget.onChanged(text, _profileFieldBloc);
                widget.onSave(text, _profileFieldBloc);
                widget.callback(0.0);
                MeasurerSingleton().saveKeyboardHeight(context);
              },
              textSubmitted: (text) {
                widget.onChanged(text, _profileFieldBloc);
                if (_suggestions.contains(text)) {
                  widget.onSave(text, _profileFieldBloc);
                } else {
                  setState(() {
                    _isFieldTextValid = false;
                  });
                }
                widget.callback(0.0);
                MeasurerSingleton().saveKeyboardHeight(context);
                SystemChannels.textInput.invokeMethod('TextInput.hide');
              },
              submitOnSuggestionTap: true,
              suggestionsAmount: _maxSuggestionsInPopup,
              itemFilter: (item, query) {
                var isItemSuitable =
                    item.toLowerCase().startsWith(query.toLowerCase());
                return isItemSuitable;
              },
              itemSorter: (a, b) {
                return a.compareTo(b);
              },
              suggestions: _suggestions,
              style: CustomTheme.of(context).textTheme.caption,
              decoration: InputDecoration(
                  contentPadding: const EdgeInsets.only(right: 8.0),
                  border: InputBorder.none,
                  hintText: widget.fieldName.toLowerCase(),
                  errorMaxLines: 3,
                  errorText: _isFieldTextValid ? null : widget.warningMessage),
              controller: _textEditingController,
              clearOnSubmit: false,
              itemBuilder: (context, item) {
                // Logic for the divider visibility. Used hashCode because
                // needless contexts with their items processed but aren't shown
                if (context.hashCode !=
                    _autocompleteItemBuilderContextHashCode) {
                  _shouldSuggestionsDividerBeShown = false;
                }
                var suggestionItem = Padding(
                    padding: EdgeInsets.symmetric(horizontal: 8.0),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Visibility(
                            visible: _shouldSuggestionsDividerBeShown,
                            child: Divider(
                              height: 1.0,
                              color: _additionalThemeData
                                  .autocompleteSuggestionsDividerColor,
                            )),
                        Padding(
                          padding: const EdgeInsets.only(bottom: 6.0, top: 4.0),
                          child: Text(
                            item,
                            maxLines: 3,
                          ),
                        )
                      ],
                    ));
                _shouldSuggestionsDividerBeShown = true;
                _autocompleteItemBuilderContextHashCode = context.hashCode;
                return suggestionItem;
              },
            ),
          );
        } else {
          return Text(_textEditingController.text,
              style: CustomTheme.of(context).textTheme.caption);
        }
      });

  Widget _createProfileTextFormField() {
    return TextFormField(
      key: widget.key,
      controller: _textEditingController,
      focusNode: _node,
      decoration: InputDecoration(
          contentPadding: const EdgeInsets.all(0.0),
          border: InputBorder.none,
          hintText: widget.fieldName.toLowerCase(),
          errorMaxLines: 3),
      style: CustomTheme.of(context).textTheme.caption,
      maxLines: null,
      inputFormatters: widget.inputFormatters,
      autocorrect: false,
      autovalidate: true,
      enableInteractiveSelection: true,
      keyboardType: widget.textInputType == null
          ? TextInputType.text
          : widget.textInputType,
      textInputAction: TextInputAction.done,
      textAlign: TextAlign.start,
      validator: (value) {
        return !_isFieldTextValid && value.length > 2
            ? '${widget.warningMessage}'
            : null;
      },
      onTap: () {
        if (!_node.hasFocus) _node.requestFocus();
      },
      onEditingComplete: () {
        _node.unfocus();
        if (_isFieldTextValid) {
          widget.onSave(_textEditingController.text, _profileFieldBloc);
        } else {
          // Set last saved value to the field
          widget.onSave(null, _profileFieldBloc);
        }
      },
      onChanged: (value) {
        setState(() {
          widget.onChanged(_textEditingController.text, _profileFieldBloc);
        });
      },
    );
  }

  void _scrollToAutocomplete() {
    final isOrientationPortrait =
        MediaQuery.of(context).orientation == Orientation.portrait;
    final densityCoefficient = MeasurerSingleton().densityCoefficient(
        WidgetsBinding.instance.window, isOrientationPortrait);
    final keyboardHeight = isOrientationPortrait
        ? AppConfig.of(context).keyboardPortraitHeight
        : AppConfig.of(context).keyboardLandscapeHeight;
    final densityAwareKeyboardHeight =
        keyboardHeight == null ? 320 : keyboardHeight / densityCoefficient;
    final dyAutocompletePosition =
        MeasurerSingleton().getWidgetPosition(widget.key).dy;
    final scrollPositionOffset = widget.parentScrollController.position.pixels;
    final height = MediaQuery.of(context).size.height;
    var fieldPortraitPosition = scrollPositionOffset +
        dyAutocompletePosition -
        (height - densityAwareKeyboardHeight - 200);
    var fieldLandscapePosition =
        dyAutocompletePosition + scrollPositionOffset - 20.0;
    widget.callback(keyboardHeight);
    widget.parentScrollController
        .animateTo(
            isOrientationPortrait
                ? fieldPortraitPosition
                : fieldLandscapePosition,
            duration: Duration(milliseconds: 100),
            curve: Curves.linear)
        .then((_) {
      _node.requestFocus();
    });
  }
}
