import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:intl/intl.dart';

import '../../../../core/domain/models/models.dart';
import '../../../../core/helpers/helpers.dart';
import '../../../../core/presentation/blocs/blocs.dart';
import '../../../../core/presentation/widgets/widgets.dart';
import '../blocs/blocs.dart';
import '../widgets/widgets.dart';

class ProfileScreen extends StatefulWidget {
  static const routeName = '/profile';
  @override
  ProfileScreenState createState() => ProfileScreenState();
}

class ProfileScreenState extends State<ProfileScreen>
    with SingleTickerProviderStateMixin {
  final double _appBarHeight = 76.0;
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey();
  final GlobalKey _locationFieldKey = GlobalKey();
  final ScrollController _scrollController = ScrollController();
  final _autocompleteSuggestionsBloc = AutocompleteSuggestionsBloc();
  AdditionalThemeData _additionalThemeData;
  ProfileImageBloc _profileImageBloc;
  Widget _profileDetailsFields;
  Widget _profileImage;
  double _singleChildScrollViewOffset;
  Widget _signedProfileImage;
  ProfileField _emailField;
  ProfileField _phoneField;
  ProfileField _locationField;
  ProfileField _aboutField;
  double _fakeSizeBoxHeight = 0.0;
  AppThemeKey _lastBuildThemeKey;

  @override
  void initState() {
    super.initState();
    _scrollController.addListener(() {
      setState(() {
        _singleChildScrollViewOffset = _scrollController.offset;
      });
    });
    _profileImageBloc = BlocProvider.of<ProfileImageBloc>(context);
  }

  @override
  void dispose() {
    _scrollController.dispose();
    _autocompleteSuggestionsBloc.dispose();
    super.dispose();
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    _additionalThemeData = CustomTheme.instanceOf(context).additionalThemeData;
  }

  @override
  Widget build(BuildContext context) {
    if (_lastBuildThemeKey != AppConfig.of(context).appThemeKey) {
      _signedProfileImage = _createSignedProfileImage();
      _profileDetailsFields = _createProfileDetailsFields();
      _lastBuildThemeKey = AppConfig.of(context).appThemeKey;
    }

    _signedProfileImage ??= _createSignedProfileImage();
    _profileDetailsFields ??= _createProfileDetailsFields();

    return Scaffold(
      key: _scaffoldKey,
      drawer: AppDrawerProvider.of(context),
      backgroundColor: _additionalThemeData.profileScreenBackgroundColor,
      body: GestureDetector(
        onPanDown: (_) {
          _fakeSizeBoxHeight = 0.0;
        },
        onTap: () {
          FocusScope.of(context).unfocus();
        },
        child: AnnotatedRegion<SystemUiOverlayStyle>(
          value: SystemUiOverlayStyle(
            statusBarColor: Colors.transparent,
          ),
          child: Stack(children: <Widget>[
            ScrollConfiguration(
              behavior: WithoutGlowingBehavior(),
              child: ExtendedGlowingOverscrollIndicator(
                startOpacity: 0.2,
                maxOpacity: 0.35,
                leadingOffset: _extendedGlowingOverscrollIndicatorLeadingOffset,
                axisDirection: AxisDirection.down,
                child: Center(
                  child: SingleChildScrollView(
                      controller: _scrollController,
                      child: Column(children: <Widget>[
                        SizedBox(height: _appBarHeight),
                        _signedProfileImage,
                        _profileDetailsFields,
                        SizedBox(height: _fakeSizeBoxHeight)
                      ])),
                ),
              ),
            ),
            ScrollableAppBar(
              backgroundGradient: (_additionalThemeData == null ||
                      _additionalThemeData.startAppBarGradientColor == null ||
                      _additionalThemeData.endAppBarGradientColor == null)
                  ? null
                  : LinearGradient(
                      begin: Alignment.topCenter,
                      end: Alignment.bottomCenter,
                      colors: [
                        _additionalThemeData.startAppBarGradientColor,
                        _additionalThemeData.endAppBarGradientColor,
                      ],
                    ),
              opacity: 0.7,
              verticalOffset: _singleChildScrollViewOffset,
              title: "Profile",
              leftIcon: FontAwesomeIcons.angleLeft,
              rightIcon: null,
              onLeftIconPressed: () {
                Navigator.pop(context);
              },
            )
          ]),
        ),
      ),
    );
  }

  Widget _createSignedProfileImage() => Padding(
        padding: EdgeInsets.all(16.0),
        child: ClipRRect(
          borderRadius: BorderRadius.circular(10.0),
          child: Container(
            decoration: BoxDecoration(
                color: _additionalThemeData.profileScreenCardBackgroundColor),
            child: Stack(children: <Widget>[
              Column(
                children: <Widget>[
                  Padding(
                    padding: EdgeInsets.only(top: 30.0, bottom: 16.0),
                    child: BlocBuilder<UserInfoBloc, UserInfoState>(
                        builder: (context, state) {
                      if (state is UserInfoLoading) {
                        return LoadingIndicator.color(0);
                      } else if (state is UserInfoLoaded) {
                        _profileImageBloc
                            .dispatch(LoadProfileImage(state.imageUrl));
                        return BlocBuilder<ProfileImageBloc, ProfileImageState>(
                            builder: (context, state) {
                          if (state is ProfileImageLoading) {
                            return Container(
                                height: 150,
                                child:
                                    Center(child: LoadingIndicator.color(0)));
                          }
                          if (state is ProfileImageLoaded) {
                            _profileImage = Container(
                                height: 150.0,
                                width: 150.0,
                                decoration: BoxDecoration(
                                    shape: BoxShape.circle,
                                    image: DecorationImage(
                                        fit: BoxFit.cover,
                                        image:
                                            FileImage(File(state.imagePath)))));
                            return _profileImage;
                          }
                          return _profileImage ?? Container();
                        });
                      }
                      return Container(height: 150.0);
                    }),
                  ),
                  BlocBuilder<UserInfoBloc, UserInfoState>(
                      builder: (context, state) {
                    if (state is UserInfoLoaded) {
                      return EditableNameText(state.fullName);
                    } else
                      return SizedBox();
                  })
                ],
              ),
              Padding(
                padding: EdgeInsets.all(12.0),
                child: Column(
                  children: <Widget>[
                    IconButton(
                      padding: EdgeInsets.all(0.0),
                      icon: Icon(FontAwesomeIcons.image,
                          size: 22.0,
                          color:
                              CustomTheme.of(context).primaryIconTheme.color),
                      tooltip: 'Change profile image',
                      onPressed: () {
                        _profileImageBloc.dispatch(ProfileImagePickerShow());
                      },
                      iconSize: 22.0,
                    ),
                    IconButton(
                      padding: EdgeInsets.all(0.0),
                      icon: Icon(FontAwesomeIcons.camera,
                          size: 22.0,
                          color:
                              CustomTheme.of(context).primaryIconTheme.color),
                      tooltip: 'Change profile image',
                      onPressed: () {
                        _profileImageBloc
                            .dispatch(ProfileImageCameraShow(context));
                      },
                      iconSize: 22.0,
                    ),
                  ],
                ),
              ),
            ]),
          ),
        ),
      );

  Widget _createProfileDetailsFields() => Padding(
        padding: EdgeInsets.only(bottom: 16.0, left: 16.0, right: 16.0),
        child: Container(
          decoration: BoxDecoration(
            color: _additionalThemeData.profileScreenCardBackgroundColor,
            borderRadius: BorderRadius.circular(10.0),
          ),
          child: Padding(
              padding: EdgeInsets.symmetric(horizontal: 10.0),
              child: BlocBuilder<UserInfoBloc, UserInfoState>(
                  builder: (context, state) {
                if (state is UserInfoLoaded) {
                  return Form(
                    child: Column(
                      children: <Widget>[
                        ListTile(
                          title: Text(
                            'User information',
                          ),
                        ),
                        Divider(),
                        // The profile fields are saved for avoiding rebuilding
                        // (loses focus) if ones get focus, after the value had
                        // been submitted earlier at least one time
                        _emailField ??= ProfileField(
                          fieldName: 'Email',
                          leadingIconData: Icons.email,
                          initialProfileFieldText: state.email,
                          warningMessage: 'Invalid email. Wasn\'t saved',
                          onChanged: (value, profileFieldBloc) {
                            profileFieldBloc
                                .dispatch(ProfileEmailChange(value));
                          },
                          onSave: (value, profileFieldBloc) {
                            profileFieldBloc.dispatch(ProfileEmailSave(value));
                          },
                        ),
                        _phoneField ??= ProfileField(
                          fieldName: 'Phone',
                          leadingIconData: Icons.phone,
                          initialProfileFieldText: state.phone,
                          textInputType: TextInputType.phone,
                          warningMessage: 'Invalid phone number',
                          onChanged: (value, profileFieldBloc) {
                            profileFieldBloc
                                .dispatch(ProfilePhoneChange(value));
                          },
                          onSave: (value, profileFieldBloc) {
                            profileFieldBloc.dispatch(ProfilePhoneSave(value));
                          },
                          inputFormatters: <TextInputFormatter>[
                            WhitelistingTextInputFormatter.digitsOnly,
                            PhoneNumberTextInputFormatter(),
                          ],
                        ),
                        BlocProvider<AutocompleteSuggestionsBloc>(
                            builder: (context) => _autocompleteSuggestionsBloc
                              ..dispatch(LocationAutocompleteSuggestionsLoad()),
                            child: _locationField ??= ProfileField(
                                callback: (val) {
                                  setState(() {
                                    _fakeSizeBoxHeight = val;
                                  });
                                },
                                parentContext: context,
                                parentScrollController: _scrollController,
                                key: _locationFieldKey,
                                fieldName: 'Location',
                                leadingIconData: Icons.location_on,
                                initialProfileFieldText: state.location,
                                isAutocompleteTextView: true,
                                onChanged: (value, profileFieldBloc) {
                                  profileFieldBloc
                                      .dispatch(ProfileLocationChange(value));
                                },
                                onSave: (value, profileFieldBloc) {
                                  profileFieldBloc
                                      .dispatch(ProfileLocationSave(value));
                                },
                                warningMessage:
                                    'Chose from suggestion matches')),
                        _aboutField ??= ProfileField(
                          fieldName: 'About',
                          leadingIconData: Icons.person,
                          initialProfileFieldText: state.about,
                          onChanged: (value, profileFieldBloc) {
                            profileFieldBloc
                                .dispatch(ProfileAboutChange(value));
                          },
                          onSave: (value, profileFieldBloc) {
                            profileFieldBloc.dispatch(ProfileAboutSave(value));
                          },
                          inputFormatters: <TextInputFormatter>[
                            LengthLimitingTextInputFormatter(2000)
                          ],
                        ),
                        ListTile(
                          title: Text("Joined Date"),
                          subtitle: Text(DateFormat('MMMM, dd yyyy')
                              .format(state.joinedDate)),
                          leading: Icon(Icons.calendar_view_day,
                              color: CustomTheme.of(context)
                                  .primaryIconTheme
                                  .color),
                        ),
                        SizedBox(height: 10.0)
                      ],
                    ),
                  );
                } else
                  return SizedBox();
              })),
        ),
      );

  double get _extendedGlowingOverscrollIndicatorLeadingOffset {
    return (_singleChildScrollViewOffset != null)
        ? -_singleChildScrollViewOffset + _appBarHeight
        : _appBarHeight;
  }
}
