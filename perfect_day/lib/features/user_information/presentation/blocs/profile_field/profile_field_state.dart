import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

@immutable
abstract class ProfileFieldState extends Equatable {
  ProfileFieldState([List props = const []]) : super(props);
}

class ProfileJoinedDateChanged extends ProfileFieldState {
  final DateTime joinedDate;

  ProfileJoinedDateChanged(this.joinedDate) : super([joinedDate]);

  @override
  String toString() => 'ProfileJoinedDateChanged { title: $joinedDate }';
}

class ProfileFieldChanged extends ProfileFieldState {
  final String field;
  final bool isFieldValid;

  ProfileFieldChanged(this.field, this.isFieldValid)
      : super([field, isFieldValid]);

  @override
  String toString() => 'ProfileFieldChanged { title: $field,'
      ' isFieldValid: $isFieldValid }';
}

class ProfileFieldValidated extends ProfileFieldState {
  final bool isFieldTextValid;

  ProfileFieldValidated(this.isFieldTextValid);

  @override
  String toString() =>
      'ProfileFieldValidated { isFieldTextValid: $isFieldTextValid';
}

class ProfileFieldSaved extends ProfileFieldState {
  final String field;
  final bool isFieldValid;

  ProfileFieldSaved(this.field, this.isFieldValid)
      : super([field, isFieldValid]);

  @override
  String toString() => 'ProfileFieldSaved { title: $field,'
      'isFieldValid: $isFieldValid }';
}

class ProfileFieldNotSaved extends ProfileFieldState {
  final String field;
  final bool isFieldValid;

  ProfileFieldNotSaved(this.field, this.isFieldValid)
      : super([field, isFieldValid]);

  @override
  String toString() => 'ProfileFieldNotSaved { title: $field,'
      'isFieldValid: $isFieldValid }';
}

class ProfileFieldChanging extends ProfileFieldState {
  @override
  String toString() => 'ProfileFieldChanging';
}
