import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

@immutable
abstract class ProfileFieldEvent extends Equatable {
  ProfileFieldEvent([List props = const []]) : super(props);
}

class ProfileFullNameSave extends ProfileFieldEvent {
  final String fullName;

  ProfileFullNameSave(this.fullName) : super([fullName]);

  @override
  String toString() => 'ProfileFullNameChange { fullName :$fullName }';
}

class ProfileImageUrlChange extends ProfileFieldEvent {
  final String imageUrl;

  ProfileImageUrlChange(this.imageUrl) : super([imageUrl]);

  @override
  String toString() => 'ProfileImageUrlChange { imageUrl :$imageUrl }';
}

class ProfileAboutChange extends ProfileFieldEvent {
  final String about;

  ProfileAboutChange(this.about) : super([about]);

  @override
  String toString() => 'ProfileAboutChange { aboute :$about }';
}

class ProfileAboutSave extends ProfileFieldEvent {
  final String about;

  ProfileAboutSave(this.about) : super([about]);

  @override
  String toString() => 'ProfileAboutSave { aboute :$about }';
}

class ProfilePhoneChange extends ProfileFieldEvent {
  final String phone;

  ProfilePhoneChange(this.phone) : super([phone]);

  @override
  String toString() => 'ProfilePhoneChange { phone :$phone }';
}

class ProfilePhoneSave extends ProfileFieldEvent {
  final String phone;

  ProfilePhoneSave(this.phone) : super([phone]);

  @override
  String toString() => 'ProfilePhoneSave { phone :$phone }';
}

class ProfileJoinedDateChange extends ProfileFieldEvent {
  final DateTime joinedDate;

  ProfileJoinedDateChange(this.joinedDate) : super([joinedDate]);

  @override
  String toString() => 'ProfileJoinedDateChange { joinedDate :$joinedDate }';
}

class ProfileEmailSave extends ProfileFieldEvent {
  final String email;

  ProfileEmailSave(this.email) : super([email]);

  @override
  String toString() => 'ProfileEmailSave { email :$email }';
}

class ProfileEmailChange extends ProfileFieldEvent {
  final String email;

  ProfileEmailChange(this.email) : super([email]);

  @override
  String toString() => 'ProfileEmailChange { email :$email }';
}

class ProfileLocationChange extends ProfileFieldEvent {
  final String location;

  ProfileLocationChange(this.location) : super([location]);

  @override
  String toString() => 'ProfileLocationChange { location :$location }';
}

class ProfileLocationSave extends ProfileFieldEvent {
  final String location;

  ProfileLocationSave(this.location) : super([location]);

  @override
  String toString() => 'ProfileLocationSave { location :$location }';
}
