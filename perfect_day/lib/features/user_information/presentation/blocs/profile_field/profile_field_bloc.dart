import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';

import '../../../../../core/data/repositories/user_repository/user_repository.dart';
import '../../../../../core/helpers/helpers.dart';
import 'bloc.dart';

class ProfileFieldBloc extends Bloc<ProfileFieldEvent, ProfileFieldState> {
  final UserRepository _userRepository;

  ProfileFieldBloc({@required UserRepository userRepository})
      : assert(userRepository != null),
        _userRepository = userRepository;

  @override
  ProfileFieldState get initialState {
    return ProfileFieldChanging();
  }

  @override
  Stream<ProfileFieldState> mapEventToState(ProfileFieldEvent event) async* {
    if (event is ProfileFullNameSave) yield* _mapSaveTitleToState(event);
    if (event is ProfileEmailSave) yield* _mapSaveEmailToState(event);
    if (event is ProfileEmailChange) yield* _mapChangeEmailToState(event);
    if (event is ProfilePhoneSave) yield* _mapSavePhoneToState(event);
    if (event is ProfilePhoneChange) yield* _mapChangePhoneToState(event);
    if (event is ProfileAboutSave) yield* _mapSaveAboutToState(event);
    if (event is ProfileAboutChange) yield* _mapChangeAboutToState(event);
    if (event is ProfileLocationSave) yield* _mapSaveLocationToState(event);
    if (event is ProfileLocationChange) yield* _mapChangeLocationToState(event);
  }

  Stream<ProfileFieldState> _mapSaveTitleToState(
    ProfileFullNameSave event,
  ) async* {
    try {
      var oldUserId = await _userRepository.currentUserId;
      var userAdditionalInfo =
          await _userRepository.fetchUserAdditionalInfo(oldUserId);

      if (event.fullName != null &&
          userAdditionalInfo.fullName != event.fullName) {
        await _userRepository.updateCurrentUserAdditionalInfo(
            fullName: event.fullName);
      }
      yield ProfileFieldChanged(event.fullName, true);
    } catch (_) {
      yield ProfileFieldNotSaved(event.fullName, true);
    }
  }

  Stream<ProfileFieldState> _mapSaveEmailToState(
    ProfileEmailSave event,
  ) async* {
    var isEmailValid =
        event.email == null ? true : Validators.isValidEmail(event.email);
    var email;
    if (!isEmailValid) {
      yield ProfileFieldNotSaved(event.email, false);
      return;
    }
    try {
      var oldUserId = await _userRepository.currentUserId;
      var userAdditionalInfo =
          await _userRepository.fetchUserAdditionalInfo(oldUserId);
      email = event.email ?? userAdditionalInfo.email;
      if (event.email != null && userAdditionalInfo.email != event.email) {
        await _userRepository.updateCurrentUserAdditionalInfo(
            email: event.email);
      }
      yield ProfileFieldSaved(email, true);
    } catch (_) {
      yield ProfileFieldNotSaved(event.email, true);
    }
  }

  Stream<ProfileFieldState> _mapChangeEmailToState(
    ProfileEmailChange event,
  ) async* {
    yield ProfileFieldChanged(event.email, true);
  }

  Stream<ProfileFieldState> _mapSavePhoneToState(
    ProfilePhoneSave event,
  ) async* {
    var isPhoneValid =
        event.phone == null ? true : Validators.isValidPhone(event.phone);
    var phone;
    try {
      var oldUserId = await _userRepository.currentUserId;
      var userAdditionalInfo =
          await _userRepository.fetchUserAdditionalInfo(oldUserId);
      phone = event.phone ?? userAdditionalInfo.phone;
      if (event.phone != null && userAdditionalInfo.phone != event.phone) {
        await _userRepository.updateCurrentUserAdditionalInfo(
            phone: event.phone);
      }
      yield ProfileFieldSaved(phone, isPhoneValid);
    } catch (_) {
      yield ProfileFieldNotSaved(event.phone, isPhoneValid);
    }
  }

  Stream<ProfileFieldState> _mapChangePhoneToState(
    ProfilePhoneChange event,
  ) async* {
    yield ProfileFieldChanged(event.phone, true);
  }

  Stream<ProfileFieldState> _mapSaveAboutToState(
    ProfileAboutSave event,
  ) async* {
    String about;
    try {
      var oldUserId = await _userRepository.currentUserId;
      var userAdditionalInfo =
          await _userRepository.fetchUserAdditionalInfo(oldUserId);
      about = event.about ?? userAdditionalInfo.about;
      if (event.about != null && userAdditionalInfo.about != event.about) {
        await _userRepository.updateCurrentUserAdditionalInfo(
            about: event.about);
      }
      yield ProfileFieldSaved(about, true);
    } catch (_) {
      yield ProfileFieldNotSaved(event.about, true);
    }
  }

  Stream<ProfileFieldState> _mapChangeAboutToState(
    ProfileAboutChange event,
  ) async* {
    yield ProfileFieldChanged(event.about, true);
  }

  Stream<ProfileFieldState> _mapSaveLocationToState(
    ProfileLocationSave event,
  ) async* {
    yield ProfileFieldChanging();
    String location;
    try {
      var oldUserId = await _userRepository.currentUserId;
      var userAdditionalInfo =
          await _userRepository.fetchUserAdditionalInfo(oldUserId);
      location = event.location ?? userAdditionalInfo.location;
      if (event.location != null &&
          userAdditionalInfo.location != event.location) {
        await _userRepository.updateCurrentUserAdditionalInfo(
            location: event.location);
      }
      yield ProfileFieldSaved(location, true);
    } catch (_) {
      yield ProfileFieldNotSaved(event.location, true);
    }
  }

  Stream<ProfileFieldState> _mapChangeLocationToState(
    ProfileLocationChange event,
  ) async* {
    yield ProfileFieldChanged(event.location, true);
  }
}
