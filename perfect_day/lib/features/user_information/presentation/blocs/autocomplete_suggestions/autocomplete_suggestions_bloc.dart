import 'dart:convert';
import 'dart:io';

import 'package:bloc/bloc.dart';

import '../../../../../core/domain/services/file_service.dart';
import '../../../domain/models/models.dart';
import '../blocs.dart';

class AutocompleteSuggestionsBloc
    extends Bloc<AutocompleteSuggestionsEvent, AutocompleteSuggestionsState> {
  @override
  AutocompleteSuggestionsState get initialState =>
      AutocompleteSuggestionsLoading();

  @override
  Stream<AutocompleteSuggestionsState> mapEventToState(
      AutocompleteSuggestionsEvent event) async* {
    if (event is LocationAutocompleteSuggestionsLoad)
      yield* _mapLocationAutocompleteSuggestionsLoadToState(event);
  }

  Stream<AutocompleteSuggestionsState>
      _mapLocationAutocompleteSuggestionsLoadToState(
    LocationAutocompleteSuggestionsLoad event,
  ) async* {
    var fileService = FileService();
    String locationsFilePath = await fileService.fetchFile(
        'https://firebasestorage.googleapis.com/v0/b/perfectly-9b4ab.appspot.com/o/location_points%2Flocation_points.json?alt=media&token=c26598bd-982e-4921-a8cd-7171b76bac02');
    var jsonFromFile = await File(locationsFilePath).readAsString();
    final parsed = json.decode(jsonFromFile).cast<Map<String, dynamic>>();

    List<LocationPoint> locationPointsObjects = parsed
        .map<LocationPoint>((json) => LocationPoint.fromJson(json))
        .toList();
    var locationPoints = <String>[];
    for (var point in locationPointsObjects) {
      locationPoints.add('${point.city}, ${point.country}');
    }

    yield AutocompleteSuggestionsLoaded(locationPoints);
  }
}
