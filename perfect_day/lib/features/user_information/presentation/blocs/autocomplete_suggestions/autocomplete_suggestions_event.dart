import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

@immutable
abstract class AutocompleteSuggestionsEvent extends Equatable {
  AutocompleteSuggestionsEvent([List props = const []]) : super(props);
}

class LocationAutocompleteSuggestionsLoad extends AutocompleteSuggestionsEvent {
  @override
  String toString() => 'AutocompleteSuggestionsLoad';
}
