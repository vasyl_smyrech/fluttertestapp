import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

@immutable
abstract class AutocompleteSuggestionsState extends Equatable {
  AutocompleteSuggestionsState([List props = const []]) : super(props);
}

class AutocompleteSuggestionsLoading extends AutocompleteSuggestionsState {
  @override
  String toString() => 'AutocompleteSuggestionLoading';
}

class AutocompleteSuggestionsLoaded extends AutocompleteSuggestionsState {
  final List<String> suggestions;

  AutocompleteSuggestionsLoaded(this.suggestions) : super([suggestions]);

  @override
  String toString() =>
      'AutocompleteSuggestionLoaded { suggestions: $suggestions }';
}
