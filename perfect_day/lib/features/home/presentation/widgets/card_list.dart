import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../../../core/data/repositories/task_repository/task_repository.dart';
import '../../../../core/presentation/widgets/widgets.dart';
import '../../domain/models/models.dart';
import '../blocs/blocs.dart';

class CardList extends StatefulWidget {
  final VisibilityFilter filter;
  final void Function(String) onCardTap;

  CardList({@required this.filter, this.onCardTap});

  @override
  _CardListState createState() => _CardListState();
}

class _CardListState extends State<CardList> {
  List<Task> _tasks;
  List<CardItem> _cards;
  BlocBuilder _blocBuilder;

  @override
  void initState() {
    super.initState();

    switch (widget.filter) {
      case VisibilityFilter.all:
        _blocBuilder = BlocBuilder<AllTasksBloc, FilteredTasksState>(
            builder: (context, state) => _createBlocBuilder(context, state));
        break;
      case VisibilityFilter.active:
        _blocBuilder = BlocBuilder<ActiveTasksBloc, FilteredTasksState>(
            builder: (context, state) => _createBlocBuilder(context, state));
        break;
      case VisibilityFilter.completed:
        _blocBuilder = BlocBuilder<CompletedTasksBloc, FilteredTasksState>(
            builder: (context, state) => _createBlocBuilder(context, state));
        break;
      case VisibilityFilter.undated:
        _blocBuilder = BlocBuilder<UndatedTasksBloc, FilteredTasksState>(
            builder: (context, state) => _createBlocBuilder(context, state));
        break;
      case VisibilityFilter.today:
        _blocBuilder = BlocBuilder<TodayTasksBloc, FilteredTasksState>(
            builder: (context, state) => _createBlocBuilder(context, state));
        break;
      case VisibilityFilter.upcoming:
        _blocBuilder = BlocBuilder<UpcomingTasksBloc, FilteredTasksState>(
            builder: (context, state) => _createBlocBuilder(context, state));
        break;
      case VisibilityFilter.past:
        _blocBuilder = BlocBuilder<PastTasksBloc, FilteredTasksState>(
            builder: (context, state) => _createBlocBuilder(context, state));
        break;
    }
  }

  @override
  Widget build(BuildContext context) {
    return SizedBox(height: 200, child: _blocBuilder);
  }

  _createBlocBuilder(BuildContext context, FilteredTasksState state) {
    if (state is FilteredTasksLoading) {
      return Center(child: LoadingIndicator.color(0));
    } else if (state is FilteredTasksLoaded) {
      if (_tasks != state.filteredTasks) {
        _tasks = state.filteredTasks;
        _fillCards();
      }

      return ListView(
          addAutomaticKeepAlives: true,
          padding: EdgeInsets.only(bottom: 30.0),
          scrollDirection: Axis.horizontal,
          physics:
              AlwaysScrollableScrollPhysics(parent: BouncingScrollPhysics()),
          children: _cards);
    }
    return Container();
  }

  _fillCards() {
    _cards = <CardItem>[];
    for (var i = 0; i < _tasks.length; i++) {
      _cards.add(CardItem(_tasks[i], widget.onCardTap));
    }
  }
}

class CardItem extends StatefulWidget {
  final Task _task;
  final void Function(String) _onCardTap;

  CardItem(this._task, this._onCardTap);

  @override
  _CardItemState createState() => _CardItemState();
}

class _CardItemState extends State<CardItem> {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.only(left: 16.0, right: 16.0, top: 20),
      child: GestureDetector(
        onTap: () {
          widget._onCardTap(widget._task.id);
        },
        child: SizedBox(
          width: 200,
          child: Stack(
            fit: StackFit.expand,
            children: <Widget>[
              Container(
                decoration:
                    BoxDecoration(color: Colors.transparent, boxShadow: [
                  BoxShadow(
                      spreadRadius: -5.0,
                      color: CustomTheme.instanceOf(context)
                          .additionalThemeData
                          .elevationColor,
                      offset: Offset(6.0, 6.0),
                      blurRadius: 12.0)
                ]),
              ),
              ClipRRect(
                borderRadius: BorderRadius.circular(20.0),
                child: Container(
                  decoration: BoxDecoration(
                      color: CustomTheme.of(context).cardColor.withAlpha(230)),
                  child: Stack(
                    fit: StackFit.expand,
                    children: <Widget>[
                      Align(
                        alignment: Alignment.topCenter,
                        child: Column(
                          mainAxisSize: MainAxisSize.max,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            BlocProvider<CardImageBloc>(
                              builder: (context) {
                                return CardImageBloc()
                                  ..dispatch(LoadImage(
                                      widget._task.imageUrl, widget._task.id));
                              },
                              child: LoadableCardListImage(
                                key: UniqueKey(),
                              ),
                            ),
                            Align(
                              alignment: Alignment.bottomLeft,
                              child: Padding(
                                padding: EdgeInsets.symmetric(
                                    horizontal: 16.0, vertical: 8.0),
                                child: Text(widget._task.title,
                                    maxLines: 3,
                                    overflow: TextOverflow.ellipsis,
                                    style: TextStyle(
                                        color: Colors.white,
                                        fontSize: 18.0,
                                        height: 1.0,
                                        letterSpacing: 0.0,
                                        wordSpacing: 0.0,
                                        fontWeight: FontWeight.w500,
                                        fontFamily: "Montserrat")),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}

class LoadableCardListImage extends StatefulWidget {
  LoadableCardListImage({Key key}) : super(key: key);

  @override
  _LoadableCardListImageState createState() => _LoadableCardListImageState();
}

class _LoadableCardListImageState extends State<LoadableCardListImage> {
  @override
  Widget build(BuildContext context) {
    return BlocBuilder<CardImageBloc, CardImageState>(
        builder: (context, state) {
      if (state is CardImageLoading) {
        return Container(
            alignment: Alignment.bottomCenter,
            height: 60.0,
            child: LoadingIndicator.color(0));
      }

      if (state is CardImageLoaded) {
        if (state.imagePath.isEmpty) return Container();

        return Image.file(File(state.imagePath),
            width: 200,
            height: 60,
            fit: BoxFit.cover,
            alignment: Alignment.center);
      }
      return Container(height: 0.0);
    });
  }
}
