import 'dart:io';
import 'dart:math';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../../../core/data/repositories/task_repository/task_repository.dart';
import '../../../../core/presentation/widgets/widgets.dart';
import '../../domain/models/models.dart';
import '../blocs/blocs.dart';

class CardStack extends StatefulWidget {
  final void Function(String) onCardTap;
  final void Function(String, DragStartDetails) onVerticalCardDrugStart;
  final VisibilityFilter filter;
  final _cardAspectRatio = 14.4 / 16.0;

  CardStack({this.onCardTap, this.onVerticalCardDrugStart, this.filter});

  @override
  _CardStackState createState() => _CardStackState();
}

class _CardStackState extends State<CardStack> {
  List<Task> _tasks;
  double _widgetHeight;
  double _currentPage;
  double _padding = 16.0;
  double _widgetAspectRatio;
  PageController _controller;
  MediaQueryData _queryData;
  List<Widget> _pageViewCards;

  @override
  void initState() {
    super.initState();
    _widgetAspectRatio = widget._cardAspectRatio * 1.2;
    BlocProvider.of<FilteredTasksBloc>(context)
      ..dispatch(UpdateFilter(widget.filter));
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();

    _queryData = MediaQuery.of(context);
    _widgetHeight = _queryData.size.width - _padding * 2;
  }

  @override
  Widget build(BuildContext context) {
    return Container(
        height: _widgetHeight,
        width: double.infinity,
        child: BlocBuilder<FilteredTasksBloc, FilteredTasksState>(
            builder: (context, state) {
          if (state is FilteredTasksLoading) {
            return Center(child: LoadingIndicator.color(0));
          } else if (state is FilteredTasksLoaded) {
            _tasks = state.filteredTasks.reversed.toList();
            _pageViewCards ??= _fillPageViewItems();
            _currentPage =
                _currentPage == null ? _tasks.length - 1.0 : _currentPage;
            if (_controller == null) {
              _controller = PageController(initialPage: _tasks.length - 1);
              _controller.addListener(() {
                setState(() {
                  _currentPage = _controller.page;
                });
              });
            }
            return Stack(overflow: Overflow.visible, children: <Widget>[
              _createPositionedCards(),
              PageView(
                physics: BouncingScrollPhysics(),
                controller: _controller,
                reverse: true,
                children: _listWithCurrentItem,
              )
            ]);
          }
          return Container();
        }));
  }

  List<Widget> get _listWithCurrentItem {
    List<Widget> listWithCurrentItem = <Widget>[];
    for (var i = 0; i < _tasks.length; i++) {
      var value = _currentPage == i ? _pageViewCards[i] : Container();

      listWithCurrentItem.add(value);
    }

    return listWithCurrentItem;
  }

  _fillPageViewItems() {
    List<Widget> items = <Widget>[];

    for (var i = 0; i < _tasks.length; i++) {
      items.add(AspectRatio(
        aspectRatio: _widgetAspectRatio,
        child: LayoutBuilder(builder: (context, constraints) {
          var width = constraints.maxWidth;
          var height = constraints.maxHeight;

          var safeWidth = width - 2 * _padding;
          var safeHeight = height - 2 * _padding;
          var widthOfPrimaryCard = safeHeight * widget._cardAspectRatio;

          var primaryCardLeft = safeWidth - widthOfPrimaryCard;
          var horizontalInset = primaryCardLeft / 2;

          List<Widget> cardList = List();

          var delta = 0; //i - currentPage;
          var isOnRight = delta > 0;
          var cardTop = _padding + _padding * max(-delta, 0.0);
          var cardBottom = _padding * 3 -
              _padding * max(-delta, 0.0) / (_tasks.length - i * 0.9);
          var cardStart = 44 +
              max(
                  primaryCardLeft -
                      horizontalInset / 2 * -delta * (isOnRight ? 30 : 0.5),
                  0.0);
          var cardHeight = _widgetHeight - cardTop - cardBottom;
          var cardWidth = cardHeight * widget._cardAspectRatio;
          var nextCardTop = _padding +
              _padding * max(-delta + _currentPage - _currentPage.floor(), 0.0);
          var nextCardBottom = _padding * 3 -
              _padding *
                  max(-delta + _currentPage - _currentPage.floor(), 0.0) /
                  (_tasks.length - i * 0.9);
          var nextCardHeight = _widgetHeight - nextCardTop - nextCardBottom;
          var multiplier = nextCardHeight / cardHeight;

          cardList.add(_createCardItem(cardTop, cardBottom, cardStart, 0.0,
              cardHeight, cardWidth, i, multiplier));
          return Stack(
            children: cardList,
          );
        }),
      ));
    }
    return items;
  }

  _createPositionedCards() {
    return AspectRatio(
      aspectRatio: _widgetAspectRatio,
      child: LayoutBuilder(builder: (context, constraints) {
        var cardOpacity;
        var width = constraints.maxWidth;
        var height = constraints.maxHeight;

        var safeWidth = width - 2 * _padding;
        var safeHeight = height - 2 * _padding;
        var widthOfPrimaryCard = safeHeight * widget._cardAspectRatio;

        var primaryCardLeft = safeWidth - widthOfPrimaryCard;
        var horizontalInset = primaryCardLeft / 2;

        List<Widget> cardList = <Widget>[];

        for (var i = 0; i < _tasks.length; i++) {
          cardOpacity = 1.0;

          if (_currentPage - 9 > i) continue;

          var delta = i - _currentPage;
          var isOnRight = delta > 0;
          var cardTop = _padding + _padding * max(-delta, 0.0);
          var cardBottom = _padding * 3 -
              _padding * max(-delta, 0.0) / (_tasks.length - i * 0.9);
          var cardStart = 44 +
              max(
                  primaryCardLeft -
                      horizontalInset / 2 * -delta * (isOnRight ? 30 : 0.5),
                  0.0);
          var cardHeight = _widgetHeight - cardTop - cardBottom;
          var cardWidth = cardHeight * widget._cardAspectRatio;
          var nextCardTop = _padding +
              _padding * max(-delta + _currentPage - _currentPage.floor(), 0.0);
          var nextCardBottom = _padding * 3 -
              _padding *
                  max(-delta + _currentPage - _currentPage.floor(), 0.0) /
                  (_tasks.length - i * 0.9);
          var nextCardHeight = _widgetHeight - nextCardTop - nextCardBottom;
          var multiplier = nextCardHeight / cardHeight;

          cardList.add(_createCardItem(cardTop, cardBottom, cardStart,
              cardOpacity, cardHeight, cardWidth, i, multiplier));
        }
        return Stack(
          children: cardList,
        );
      }),
    );
  }

  _createCardItem(
      double cardTop,
      double cardBottom,
      double cardStart,
      double cardOpacity,
      double cardHeight,
      double cardWidth,
      int i,
      double multiplier) {
    return Positioned.directional(
      top: cardTop,
      bottom: cardBottom,
      start: cardStart,
      textDirection: TextDirection.rtl,
      child: GestureDetector(
        onTap: () {
          widget.onCardTap(_tasks[i].id);
        },
//              onVerticalDragStart: (DragStartDetails dragStartDetails) {
//                widget.onVerticalCardDrugStart(widget.cards[i].id, dragStartDetails);
        //             },
        child: Opacity(
          opacity: cardOpacity,
          child: Container(
            decoration: BoxDecoration(color: Colors.transparent, boxShadow: [
              BoxShadow(
                  color: CustomTheme.instanceOf(context)
                      .additionalThemeData
                      .elevationColor,
                  offset: Offset(3.0, 5.0),
                  blurRadius: 14.0,
                  spreadRadius: -5.0),
            ]),
            child: ClipRRect(
              borderRadius: BorderRadius.circular(20.0),
              child: Container(
                decoration: BoxDecoration(
                  color: CustomTheme.of(context).cardColor,
                ),
                child: AspectRatio(
                  aspectRatio: widget._cardAspectRatio,
                  child: Stack(
                    overflow: Overflow.clip,
                    fit: StackFit.loose,
                    children: <Widget>[
                      Container(
                          alignment: AlignmentDirectional.topCenter,
                          height: cardHeight * 0.7,
                          width: cardWidth,
                          child: BlocProvider<CardImageBloc>(
                            builder: (context) {
                              return CardImageBloc()
                                ..dispatch(LoadImage(
                                    _tasks[i].imageUrl, _tasks[i].id));
                            },
                            child: LoadableCardStackImage(
                              cardHeight: cardHeight,
                              cardWidth: cardWidth,
                            ),
                          )),
                      Align(
                        alignment: AlignmentDirectional.bottomCenter,
                        child: Container(
                          height: cardHeight * 0.3,
                          width: cardWidth,
                          alignment: AlignmentDirectional.bottomCenter,
                          child: Align(
                            alignment: Alignment.topLeft,
                            child: Padding(
                              padding: EdgeInsets.symmetric(
                                  horizontal: 16, vertical: 8.0),
                              child: Text(_tasks[i].title,
                                  maxLines: 3,
                                  overflow: TextOverflow.ellipsis,
                                  style: TextStyle(
                                      color: Colors.white,
                                      fontSize: (_currentPage <= i)
                                          ? 18.0
                                          : 18.0 * multiplier,
                                      height: 1.0,
                                      letterSpacing: 0.0,
                                      wordSpacing: 0.0,
                                      fontWeight: FontWeight.w500,
                                      fontFamily: "Montserrat")),
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }
}

class LoadableCardStackImage extends StatefulWidget {
  final double cardHeight;
  final double cardWidth;

  LoadableCardStackImage({this.cardHeight, this.cardWidth, Key key})
      : super(key: key);

  @override
  _LoadableCardStackImageState createState() => _LoadableCardStackImageState();
}

class _LoadableCardStackImageState extends State<LoadableCardStackImage> {
  @override
  Widget build(BuildContext context) {
    return BlocBuilder<CardImageBloc, CardImageState>(
        builder: (context, state) {
      if (state is CardImageLoading) {
        return Container(
            alignment: Alignment.bottomCenter,
            height: widget.cardHeight,
            child: LoadingIndicator.color(0));
      }

      if (state is CardImageLoaded) {
        if (state.imagePath.isEmpty) return Container();

        return Image.file(
          File(state.imagePath),
          alignment: AlignmentDirectional.center,
          height: widget.cardHeight * 0.7,
          width: widget.cardWidth,
          fit: BoxFit.cover,
        );
      }
      return Container(height: 0.0);
    });
  }
}
