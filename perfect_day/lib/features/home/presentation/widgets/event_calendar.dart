import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:icon_shadow/icon_shadow.dart';
import 'package:table_calendar/table_calendar.dart';

class EventCalendar extends StatefulWidget {
  final Color mainCalendarColor;
  final Color weekendColor;
  final Color todayDecorationColor;
  final Color chosenDecorationColor;
  final Color shadeColor;
  final Color shadeBorderColor;
  final Color todayEventMarkerBackground;
  final Color chosenEventMarkerBackground;
  final Color dayEventMarkerBackground;
  final Map<DateTime, List> events;

  const EventCalendar({
    Key key,
    this.mainCalendarColor = Colors.white,
    this.weekendColor = const Color.fromARGB(255, 255, 160, 136),
    this.todayDecorationColor = Colors.white30,
    this.chosenDecorationColor = const Color.fromARGB(255, 210, 80, 165),
    this.shadeColor = Colors.black38,
    this.shadeBorderColor = Colors.black12,
    this.todayEventMarkerBackground = Colors.blueGrey,
    this.chosenEventMarkerBackground = Colors.brown,
    this.dayEventMarkerBackground = Colors.blueAccent,
    this.events
  }) : super(key: key);

  @override
  _EventCalendarState createState() => _EventCalendarState();
}

class _EventCalendarState extends State<EventCalendar>
    with TickerProviderStateMixin {
  final _calendarController = CalendarController();
  final _tableCalendarKey = GlobalKey();
  BoxShadow _eventShadow;
  Shadow _headerShadow;
  Shadow _elementsShadow;
  Shadow _weekdayBorderShadow;
  Shadow _weekendBorderShadow;
  TextStyle _weekdayTextStyle;
  TextStyle _weekendTextStyle;
  TextStyle _titleTextStyle;
  TextStyle _flatNumberTextStyle;
  TextStyle _eventsNumberTextStyle;
  AnimationController _animationController;

  List _selectedEvents;

  @override
  void initState() {
    super.initState();

    _eventShadow = BoxShadow(
        color: widget.shadeColor,
        blurRadius: 2.0,
        spreadRadius: 0.0,
        offset: Offset(2.0, 2.0));

    _headerShadow = Shadow(
      offset: Offset(2.0, 3.0),
      blurRadius: 7.0,
      color: widget.shadeColor,
    );
    _elementsShadow = Shadow(
      offset: Offset(3.0, 3.0),
      blurRadius: 5.0,
      color: widget.shadeColor,
    );
    _weekdayBorderShadow = Shadow(
      offset: Offset(-1.0, -1.0),
      blurRadius: 1.0,
      color: widget.shadeBorderColor,
    );
    _weekendBorderShadow = Shadow(
      offset: Offset(-0.5, -0.5),
      blurRadius: 1.0,
      color: Colors.black54,
    );
    _titleTextStyle = TextStyle(
        shadows: <Shadow>[_headerShadow],
        fontSize: 18.0,
        fontWeight: FontWeight.w600,
        fontFamily: "Montserrat");

    _weekdayTextStyle = TextStyle(shadows: <Shadow>[
      _elementsShadow,
      _weekdayBorderShadow,
    ], fontSize: 16.0, fontWeight: FontWeight.w500, fontFamily: "Montserrat");

    _weekendTextStyle = TextStyle(
        shadows: <Shadow>[
          _elementsShadow,
          _weekendBorderShadow,
        ],
        color: widget.weekendColor,
        fontSize: 16.0,
        fontWeight: FontWeight.w500,
        fontFamily: "Montserrat");

    _flatNumberTextStyle = TextStyle(
        color: widget.mainCalendarColor,
        fontSize: 16.0,
        fontWeight: FontWeight.w500,
        fontFamily: "Montserrat");

    _eventsNumberTextStyle = TextStyle(
        color: widget.mainCalendarColor,
        fontSize: 12.0,
        fontWeight: FontWeight.w500,
        fontFamily: "Montserrat");

    final _selectedDay = DateTime.now();

    _selectedEvents = null;//widget.events[_selectedDay] ?? [];

    _animationController = AnimationController(
      vsync: this,
      duration: const Duration(milliseconds: 400),
    );

    _animationController.forward();
  }

  @override
  void dispose() {
    super.dispose();
    _calendarController.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Stack(children: <Widget>[
      Positioned(
        left: 23.0,
        top: 20.0,
        child: IconShadowWidget(
          Icon(FontAwesomeIcons.angleLeft, color: Colors.transparent),
          shadowColor: Colors.black26,
        ),
      ),
      Positioned(
        right: 17.0,
        top: 20.0,
        child: IconShadowWidget(
          Icon(FontAwesomeIcons.angleRight, color: Colors.transparent),
          shadowColor: Colors.black26,
        ),
      ),
      TableCalendar(
        key: _tableCalendarKey,
        // locale: 'uk',
        calendarController: _calendarController,
        events: widget.events,
        // holidays: _holidays,
        //startDay: , joinedDay
        rowHeight: 40.0,
        initialCalendarFormat: CalendarFormat
            .month, // should be saved in the preferences or at HomeScreen;
        formatAnimation: FormatAnimation.slide,
        startingDayOfWeek: StartingDayOfWeek.monday,
        availableGestures: AvailableGestures.all,
        availableCalendarFormats: const {
          CalendarFormat.month: '',
          CalendarFormat.week: '',
        },
        // Calendar header
        headerStyle: HeaderStyle(
          centerHeaderTitle: true,
          formatButtonShowsNext: false,
          formatButtonVisible: false,
          leftChevronIcon: Icon(FontAwesomeIcons.angleLeft),
          rightChevronIcon: Icon(FontAwesomeIcons.angleRight),
          titleTextStyle: _titleTextStyle,
        ),
        // Days names
        daysOfWeekStyle: DaysOfWeekStyle(
            // Weekdays' names
            weekdayStyle: _weekdayTextStyle,
            // Weekends' names
            weekendStyle: _weekendTextStyle),
        calendarStyle: CalendarStyle(
          renderSelectedFirst: true,
          outsideDaysVisible: false,
          holidayStyle: _weekendTextStyle,
        ),
        builders: CalendarBuilders(
          weekendDayBuilder: (context, date, _) {
            return FadeTransition(
              opacity:
              Tween(begin: 0.0, end: 1.0).animate(_animationController),
              child: Container(
                decoration: BoxDecoration(
                  shape: BoxShape.rectangle,
                  color: Colors.transparent,
                ),
                alignment: Alignment.center,
                child: Text(
                  '${date.day}',
                  style: _weekendTextStyle,
                ),
              ),
            );
          },
          dayBuilder: (context, date, _) {
            return FadeTransition(
              opacity:
              Tween(begin: 0.0, end: 1.0).animate(_animationController),
              child: Container(
                decoration: BoxDecoration(
                  shape: BoxShape.rectangle,
                  color: Colors.transparent,
                ),
                alignment: Alignment.center,
                child: Text(
                  '${date.day}',
                  style: _weekdayTextStyle,
                ),
              ),
            );
          },
          selectedDayBuilder: (context, date, _) {
            return FadeTransition(
              opacity:
                  Tween(begin: 0.0, end: 1.0).animate(_animationController),
              child: Container(
                decoration: BoxDecoration(
                  shape: BoxShape.circle,
                  color: widget.chosenDecorationColor,
                ),
                margin: const EdgeInsets.all(1.0),
                alignment: Alignment.center,
                child: Text(
                  '${date.day}',
                  style: _flatNumberTextStyle,
                ),
              ),
            );
          },
          todayDayBuilder: (context, date, _) {
            return Container(
              decoration: BoxDecoration(
                shape: BoxShape.circle,
                color: widget.todayDecorationColor,
              ),
              margin: const EdgeInsets.all(1.0),
              alignment: Alignment.center,
              child: Text(
                '${date.day}',
                style: _flatNumberTextStyle,
              ),
            );
          },
          markersBuilder: (context, date, events, holidays) {
            final children = <Widget>[];
            if (events.isNotEmpty) {
              children.add(
                Center(
                    child: Padding(
                      padding: EdgeInsets.only(top: 26.0, left: 26.0),
                        child: _buildEventsMarker(date, events)
                    ),
                ),
              );
            }
            return children;
          },
        ),
        onDaySelected: (date, events) {
          _onDaySelected(date, events);
          _animationController.forward(from: 0.0);
        },
        onVisibleDaysChanged: _onVisibleDaysChanged,
      ),
    ]);
  }

  Widget _buildEventsMarker(DateTime date, List events) {
    return AnimatedContainer(
      duration: const Duration(milliseconds: 300),
      decoration: BoxDecoration(
        boxShadow: _calendarController.isSelected(date) ||
                _calendarController.isToday(date)
            ? null
            : <BoxShadow>[_eventShadow],
        shape: BoxShape.circle,
        color: _calendarController.isSelected(date)
            ? widget.chosenEventMarkerBackground
            : _calendarController.isToday(date)
                ? widget.todayEventMarkerBackground
                : widget.dayEventMarkerBackground,
      ),
      child: Center(
        child: Text(events.length > 9 ? '9+' : '${events.length}',
            style: _eventsNumberTextStyle),
      ),
    );
  }

  void _onDaySelected(DateTime day, List events) {
    print('CALLBACK: _onDaySelected');
    setState(() {
      _selectedEvents = events;
    });
  }

  void _onVisibleDaysChanged(
      DateTime first, DateTime last, CalendarFormat format) {
    print('CALLBACK: _onVisibleDaysChanged');
  }
}
