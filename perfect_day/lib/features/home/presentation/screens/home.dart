import 'package:autocomplete_textfield/autocomplete_textfield.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:perfect_day/core/data/repositories/task_repository/task_repository.dart';
import 'package:perfect_day/core/presentation/widgets/statefull/task_list_item.dart';

import '../../../../core/domain/models/models.dart';
import '../../../../core/helpers/helpers.dart';
import '../../../../core/presentation/blocs/blocs.dart';
import '../../../../core/presentation/widgets/widgets.dart';
import '../../domain/models/models.dart';
import '../widgets/widgets.dart';

class HomeScreen extends StatefulWidget {
  final StringCallback onCardTap;
  HomeScreen({this.onCardTap});

  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  final appBarHeight = 76.0;
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey();
  final _scrollController = ScrollController();
  var _bottomNavigationBarIndex = 0;
  double _singleChildScrollViewOffset;
  List<Widget> _fragments;
  CardStack _cardStack;
  Padding _upcomingRow;
  CardList _cardListOne;
  Padding _ideasRow;
  CardList _cardListTwo;
  SizedBox _bottomMargin;
  AdditionalThemeData _additionalThemeData;
  Map<DateTime, List> _calendarTasks;
  List<Task> _pickedDateTasks;

  @override
  void initState() {
    super.initState();
    _scrollController
      ..addListener(() {
        setState(() {
          _singleChildScrollViewOffset = _scrollController.offset;
        });
      });

    //_calendarTasks =

    _fragments = <Widget>[_createHomeFragment(), _createCalendarFragment()];

    _cardStack = CardStack(
      filter: VisibilityFilter.all,
      onCardTap: widget.onCardTap,
      onVerticalCardDrugStart: _onVerticalCardDrugStart,
    );

    _upcomingRow = Padding(
        padding: EdgeInsets.only(left: 16.0),
        child: Row(
          children: <Widget>[
            Text("Upcoming",
                style: TextStyle(
                    color: Colors.white,
                    fontSize: 20.0,
                    fontWeight: FontWeight.w700,
                    fontFamily: "Montserrat")),
          ],
        ));

    _cardListOne = CardList(
      filter: VisibilityFilter.past,
      onCardTap: widget.onCardTap,
    );

    _ideasRow = Padding(
        padding: EdgeInsets.only(left: 16.0, top: 16),
        child: Row(
          children: <Widget>[
            Text("Ideas",
                style: TextStyle(
                    color: Colors.white,
                    fontSize: 20.0,
                    fontWeight: FontWeight.w700,
                    fontFamily: "Montserrat")),
          ],
        ));

    _cardListTwo = CardList(
      filter: VisibilityFilter.upcoming,
      onCardTap: widget.onCardTap,
    );

    _bottomMargin = SizedBox(height: 40.0);
  }

  @override
  void dispose() {
    _scrollController.dispose();
    super.dispose();
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    _additionalThemeData = CustomTheme.instanceOf(context).additionalThemeData;
  }

  @override
  Widget build(BuildContext context) {
    return AnnotatedRegion<SystemUiOverlayStyle>(
      value: SystemUiOverlayStyle(
        statusBarColor: Colors.transparent,
      ),
      child: AppBackground(
        child: Scaffold(
          key: _scaffoldKey,
          backgroundColor: Colors.transparent,
          drawer: AppDrawerProvider.of(context),
          body: Stack(children: <Widget>[
            ScrollConfiguration(
              behavior: WithoutGlowingBehavior(),
              child: ExtendedGlowingOverscrollIndicator(
                  startOpacity: 0.2,
                  maxOpacity: 0.35,
                  leadingOffset:
                      _extendedGlowingOverscrollIndicatorLeadingOffset,
                  axisDirection: AxisDirection.down,
                  child: _fragments[_bottomNavigationBarIndex]),
            ),
            ScrollableAppBar(
                backgroundGradient: (_additionalThemeData == null ||
                        _additionalThemeData.startAppBarGradientColor == null ||
                        _additionalThemeData.endAppBarGradientColor == null)
                    ? null
                    : LinearGradient(
                        begin: Alignment.topCenter,
                        end: Alignment.bottomCenter,
                        colors: [
                          _additionalThemeData.startAppBarGradientColor,
                          _additionalThemeData.endAppBarGradientColor,
                        ],
                      ),
                opacity: 0.7,
                verticalOffset: _singleChildScrollViewOffset,
                title: "Perfect Day",
                onLeftIconPressed: _onLeftAppBarIconPressed,
                onRightIconPressed: _onRightAppBarIconPressed)
          ]),
          bottomNavigationBar: _indexBottom(),
          floatingActionButton: Container(
            width: 65.0,
            height: 65.0,
            decoration: BoxDecoration(
                color: _additionalThemeData.floatingActionButtonColor,
                shape: BoxShape.circle,
                boxShadow: [
                  BoxShadow(
                      color: _additionalThemeData.elevationColor,
                      offset: Offset(0.0, 5.0),
                      blurRadius: 10.0)
                ]),
            child: RawMaterialButton(
              shape: CircleBorder(),
              child: Icon(
                Icons.add,
                size: 35.0,
                //color: Colors.white,
              ),
              onPressed: () {
//              Navigator.push(context,
//                  MaterialPageRoute(builder: (context) => TaskDetails()));
              },
            ),
          ),
          floatingActionButtonLocation:
              FloatingActionButtonLocation.centerDocked,
        ),
      ),
    );
  }

  Widget _indexBottom() => BottomNavigationBar(
        items: [
          BottomNavigationBarItem(
            icon: Column(children: <Widget>[
              SizedBox(height: 5.0),
              Icon(
                Icons.home,
                size: 30.0,
                color: _additionalThemeData == null
                    ? null
                    : _additionalThemeData.bottomNavigationBarDisableIconColor,
              ),
            ]),
            title: Container(),
            activeIcon: Icon(
              Icons.home,
              size: 40,
              color: _additionalThemeData == null
                  ? null
                  : _additionalThemeData.bottomNavigationBarActiveIconColor,
            ),
          ),
          BottomNavigationBarItem(
            icon: Icon(
              FontAwesomeIcons.calendar,
              size: 24,
              color: _additionalThemeData == null
                  ? null
                  : _additionalThemeData.bottomNavigationBarDisableIconColor,
            ),
            title: Container(),
            activeIcon: Column(children: <Widget>[
              Icon(
                FontAwesomeIcons.calendar,
                size: 30,
                color: _additionalThemeData == null
                    ? null
                    : _additionalThemeData.bottomNavigationBarActiveIconColor,
              ),
              SizedBox(
                height: 5.0,
              )
            ]),
          ),
        ],
        type: BottomNavigationBarType.fixed,
        currentIndex: _bottomNavigationBarIndex,
        onTap: (index) {
          setState(() {
            _bottomNavigationBarIndex = index;
            switch (_bottomNavigationBarIndex) {
              case 0:
                //contents = "Home";
                break;
              case 1:
                //contents = "Articles";
                break;
              case 2:
                //contents = "User";
                break;
            }
          });
        },
      );

  double get _extendedGlowingOverscrollIndicatorLeadingOffset {
    return (_singleChildScrollViewOffset != null)
        ? -_singleChildScrollViewOffset + appBarHeight
        : appBarHeight;
  }

  Widget _createHomeFragment() =>
      BlocBuilder<TasksBloc, TasksState>(builder: (context, state) {
        if (state is TasksLoading) {
          return Center(child: LoadingIndicator.color(0));
        } else if (state is TasksLoaded) {
          return ListView(
              addAutomaticKeepAlives: true,
              padding: EdgeInsets.only(top: appBarHeight),
              controller: _scrollController,
              children: <Widget>[
                _cardStack,
                _upcomingRow,
                _cardListOne,
                _ideasRow,
                _cardListTwo,
                _bottomMargin
              ]);
        } else
          return Container(
            height: 0.0,
          );
      });

  Widget _createCalendarFragment() => ListView(
      addAutomaticKeepAlives: true,
      padding: EdgeInsets.only(top: appBarHeight),
      controller: _scrollController,
      children: <Widget>[
        EventCalendar(),
//        ListView.builder(
//          itemCount: _pickedDateTasks?.length,
//            itemBuilder: (context, index){
//              return TaskListItem(task: _pickedDateTasks[index], );
//            })
      ]);

  _onVerticalCardDrugStart(String cardId, DragStartDetails dragStartDetails) {}

  _onRightAppBarIconPressed() {}

  _onLeftAppBarIconPressed() {
    _scaffoldKey.currentState.openDrawer();
  }
}
