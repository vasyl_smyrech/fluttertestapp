import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

import '../../../../../core/data/repositories/task_repository/task_repository.dart';
import '../../../domain/models/models.dart';

@immutable
abstract class FilteredTasksEvent extends Equatable {
  FilteredTasksEvent([List props = const []]) : super(props);
}

class UpdateFilter extends FilteredTasksEvent {
  final VisibilityFilter filter;

  UpdateFilter(this.filter) : super([filter]);

  @override
  String toString() => 'UpdateFilter { filter: $filter }';
}

class UpdateTasks extends FilteredTasksEvent {
  final List<Task> tasks;

  UpdateTasks(this.tasks) : super([tasks]);

  @override
  String toString() => 'UpdateTasks { tasks: $tasks }';
}
