import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

import '../../../../../core/data/repositories/task_repository/task_repository.dart';
import '../../../domain/models/models.dart';

@immutable
abstract class FilteredTasksState extends Equatable {
  FilteredTasksState([List props = const []]) : super(props);
}

class FilteredTasksLoading extends FilteredTasksState {
  @override
  String toString() => 'FilteredTodosLoading';
}

class FilteredTasksLoaded extends FilteredTasksState {
  final List<Task> filteredTasks;
  final VisibilityFilter activeFilter;

  FilteredTasksLoaded(this.filteredTasks, this.activeFilter)
      : super([filteredTasks, activeFilter]);

  @override
  String toString() {
    return 'FilteredTasksLoaded { filteredTasks: $filteredTasks, activeFilter: $activeFilter }';
  }
}
