import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';

import '../../../../../core/data/repositories/task_repository/task_repository.dart';
import '../../../../../core/presentation/blocs/blocs.dart';
import '../../../domain/models/models.dart';
import '../blocs.dart';

class FilteredTasksBloc extends Bloc<FilteredTasksEvent, FilteredTasksState> {
  final TasksBloc _tasksBloc;
  StreamSubscription _tasksSubscription;

  FilteredTasksBloc({@required TasksBloc tasksBloc})
      : assert(tasksBloc != null),
        _tasksBloc = tasksBloc {
    _tasksSubscription = tasksBloc.state.listen((state) {
      if (state is TasksLoaded) {
        dispatch(UpdateTasks((tasksBloc.currentState as TasksLoaded).tasks));
      }
    });
  }

  @override
  FilteredTasksState get initialState {
    return FilteredTasksLoading();
  }

  @override
  Stream<FilteredTasksState> mapEventToState(FilteredTasksEvent event) async* {
    if (event is UpdateFilter) {
      yield* _mapUpdateFilterToState(event);
    } else if (event is UpdateTasks) {
      yield* _mapTasksUpdatedToState(event);
    }
  }

  Stream<FilteredTasksState> _mapUpdateFilterToState(
    UpdateFilter event,
  ) async* {
    final state = _tasksBloc.currentState;
    if (state is TasksLoaded) {
      yield FilteredTasksLoaded(
        _mapTasksToFilteredTasks(state.tasks, event.filter),
        event.filter,
      );
    }
  }

  Stream<FilteredTasksState> _mapTasksUpdatedToState(
    UpdateTasks event,
  ) async* {
    final visibilityFilter = currentState is FilteredTasksLoaded
        ? (currentState as FilteredTasksLoaded).activeFilter
        : VisibilityFilter.all;
    yield FilteredTasksLoaded(
      _mapTasksToFilteredTasks(
        (_tasksBloc.currentState as TasksLoaded).tasks,
        visibilityFilter,
      ),
      visibilityFilter,
    );
  }

  List<Task> _mapTasksToFilteredTasks(
      List<Task> tasks, VisibilityFilter filter) {
    return tasks.where((task) {
      switch (filter) {
        case VisibilityFilter.all:
          return true;
          break;
        case VisibilityFilter.active:
          return !task.completed;
          break;
        case VisibilityFilter.completed:
          return task.completed;
          break;
        case VisibilityFilter.today:
          return task.dueDateTime.difference(DateTime.now()).inDays == 0;
          break;
        case VisibilityFilter.undated:
          return task.dueDateTime == null;
          break;
        case VisibilityFilter.upcoming:
          return task.dueDateTime.difference(DateTime.now()).inDays > 0;
          break;
        case VisibilityFilter.past:
          return task.dueDateTime.difference(DateTime.now()).inDays < 0;
        default:
          return false;
      }
    }).toList();
  }

  @override
  void dispose() {
    _tasksSubscription?.cancel();
    super.dispose();
  }
}

class AllTasksBloc extends FilteredTasksBloc {}

class PastTasksBloc extends FilteredTasksBloc {
  final TasksBloc _tasksBloc;

  PastTasksBloc({@required TasksBloc tasksBloc})
      : assert(tasksBloc != null),
        _tasksBloc = tasksBloc,
        super(tasksBloc: tasksBloc) {
    _tasksSubscription = tasksBloc.state.listen((state) {
      if (state is TasksLoaded) {
        dispatch(UpdateTasks((tasksBloc.currentState as TasksLoaded).tasks));
      }
    });
  }
}

class UpcomingTasksBloc extends FilteredTasksBloc {
  final TasksBloc _tasksBloc;

  UpcomingTasksBloc({@required TasksBloc tasksBloc})
      : assert(tasksBloc != null),
        _tasksBloc = tasksBloc,
        super(tasksBloc: tasksBloc) {
    _tasksSubscription = tasksBloc.state.listen((state) {
      if (state is TasksLoaded) {
        dispatch(UpdateTasks((tasksBloc.currentState as TasksLoaded).tasks));
      }
    });
  }
}

class ActiveTasksBloc extends FilteredTasksBloc {
  final TasksBloc _tasksBloc;

  ActiveTasksBloc({@required TasksBloc tasksBloc})
      : assert(tasksBloc != null),
        _tasksBloc = tasksBloc,
        super(tasksBloc: tasksBloc) {
    _tasksSubscription = tasksBloc.state.listen((state) {
      if (state is TasksLoaded) {
        dispatch(UpdateTasks((tasksBloc.currentState as TasksLoaded).tasks));
      }
    });
  }
}

class CompletedTasksBloc extends FilteredTasksBloc {
  final TasksBloc _tasksBloc;

  CompletedTasksBloc({@required TasksBloc tasksBloc})
      : assert(tasksBloc != null),
        _tasksBloc = tasksBloc,
        super(tasksBloc: tasksBloc) {
    _tasksSubscription = tasksBloc.state.listen((state) {
      if (state is TasksLoaded) {
        dispatch(UpdateTasks((tasksBloc.currentState as TasksLoaded).tasks));
      }
    });
  }
}

class UndatedTasksBloc extends FilteredTasksBloc {
  final TasksBloc _tasksBloc;

  UndatedTasksBloc({@required TasksBloc tasksBloc})
      : assert(tasksBloc != null),
        _tasksBloc = tasksBloc,
        super(tasksBloc: tasksBloc) {
    _tasksSubscription = tasksBloc.state.listen((state) {
      if (state is TasksLoaded) {
        dispatch(UpdateTasks((tasksBloc.currentState as TasksLoaded).tasks));
      }
    });
  }
}

class TodayTasksBloc extends FilteredTasksBloc {
  final TasksBloc _tasksBloc;

  TodayTasksBloc({@required TasksBloc tasksBloc})
      : assert(tasksBloc != null),
        _tasksBloc = tasksBloc,
        super(tasksBloc: tasksBloc) {
    _tasksSubscription = tasksBloc.state.listen((state) {
      if (state is TasksLoaded) {
        dispatch(UpdateTasks((tasksBloc.currentState as TasksLoaded).tasks));
      }
    });
  }
}
