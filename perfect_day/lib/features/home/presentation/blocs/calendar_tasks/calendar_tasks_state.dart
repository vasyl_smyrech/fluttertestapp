import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

@immutable
abstract class CalendarTasksState extends Equatable {
  CalendarTasksState([List props = const []]) : super(props);
}

class CalendarTasksLoading extends CalendarTasksState {
  @override
  String toString() => 'CalendarTasksLoading';
}

class CalendarTasksLoaded extends CalendarTasksState {
  final Map<DateTime, List> calendarTasks;

  CalendarTasksLoaded([this.calendarTasks]) : super([calendarTasks]);

  @override
  String toString() => 'CalendarTasksLoaded { calendarTasks: $calendarTasks }';
}

class CalendarTasksNotLoaded extends CalendarTasksState {
  @override
  String toString() => 'CalendarTasksNotLoaded';
}
