import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

import '../../../../../core/data/repositories/task_repository/models/models.dart';

@immutable
abstract class CalendarTasksEvent extends Equatable {
  CalendarTasksEvent([List props = const []]) : super(props);
}

class LoadCalendarTasks extends CalendarTasksEvent {
  @override
  String toString() => 'LoadCalendarTasks';
}

class UpdateCalendarTasks extends CalendarTasksEvent {
  final List<Task> tasks;

  UpdateCalendarTasks(this.tasks) : super([tasks]);

  @override
  String toString() => 'UpdateCalendarTasks { tasks: $tasks }';
}
