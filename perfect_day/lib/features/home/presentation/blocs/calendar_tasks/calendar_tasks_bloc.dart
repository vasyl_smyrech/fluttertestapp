import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/cupertino.dart';
import 'package:perfect_day/core/presentation/blocs/blocs.dart';

import '../../../../../core/data/repositories/task_repository/task_repository.dart';
import '../../../../../core/domain/services/services.dart';
import '../blocs.dart';

class CalendarTasksBloc extends Bloc<CalendarTasksEvent, CalendarTasksState> {
  StreamSubscription _tasksSubscription;
  var _tasks;

  CalendarTasksBloc({@required TasksBloc tasksBloc})
      : assert(tasksBloc != null) {
    _tasksSubscription = tasksBloc.state.listen((state) {
      if (state is TasksLoaded) {
        _tasks = state.tasks;
        dispatch(UpdateCalendarTasks((tasksBloc.currentState as TasksLoaded).tasks));
      }
    });
  }

  @override
  CalendarTasksState get initialState {
    return CalendarTasksLoading();
  }

  @override
  Stream<CalendarTasksState> mapEventToState(CalendarTasksEvent event) async* {
    if (event is LoadCalendarTasks) yield* _mapLoadCalendarTasksToState(event);

    if (event is UpdateCalendarTasks) yield* _mapUpdateCalendarTasksToState(event);
  }

  Stream<CalendarTasksState> _mapUpdateCalendarTasksToState(
      UpdateCalendarTasks event,
      ) async* {
    try {
      var calendarTasks = Map<DateTime, List<Task>>();

      for (Task task in event.tasks){
       var dateTasks = calendarTasks[task.dueDateTime];
       if (dateTasks == null){
         calendarTasks[task.dueDateTime] = <Task>[task];
       } else {
         dateTasks.add(task);
         calendarTasks[task.dueDateTime] = dateTasks;
       }
      }

      yield CalendarTasksLoaded(calendarTasks);
    } catch (_) {
      yield CalendarTasksNotLoaded();
    }
  }

  Stream<CalendarTasksState> _mapLoadCalendarTasksToState(
      LoadCalendarTasks event,
      ) async* {
   UpdateCalendarTasks(_tasks);
  }

  @override
  void dispose() {
    _tasksSubscription?.cancel();
    super.dispose();
  }
}
