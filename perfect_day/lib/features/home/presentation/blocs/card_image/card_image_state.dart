import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

@immutable
abstract class CardImageState extends Equatable {
  CardImageState([List props = const []]) : super(props);
}

class CardImageLoading extends CardImageState {
  @override
  String toString() => 'ImageLoading';
}

class CardImageLoaded extends CardImageState {
  final String imagePath;

  CardImageLoaded([this.imagePath]) : super([imagePath]);

  @override
  String toString() => 'CardImageLoaded { imagePath: $imagePath }';
}

class CardImageNotLoaded extends CardImageState {
  @override
  String toString() => 'ImageNotLoaded';
}
