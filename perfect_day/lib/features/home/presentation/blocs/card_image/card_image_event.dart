import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

@immutable
abstract class CardImageEvent extends Equatable {
  CardImageEvent([List props = const []]) : super(props);
}

class LoadImage extends CardImageEvent {
  final String imageUrl;
  final String taskId;

  LoadImage(this.imageUrl, this.taskId) : super([imageUrl, taskId]);

  @override
  String toString() => 'LoadImage';
}

class UpdateImage extends CardImageEvent {
  final String imageUrl;

  UpdateImage(this.imageUrl) : super([imageUrl]);

  @override
  String toString() => 'UpdateImage';
}
