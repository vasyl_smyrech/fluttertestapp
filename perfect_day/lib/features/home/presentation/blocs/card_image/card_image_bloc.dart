import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

import '../../../../../core/data/repositories/task_repository/task_repository.dart';
import '../../../../../core/domain/services/services.dart';
import '../blocs.dart';

class CardImageBloc extends Bloc<CardImageEvent, CardImageState> {
  final fileService = FileService();
  StreamSubscription _documentSubscription;
  final _tasksRepository = FirebaseTaskRepository();

  @override
  CardImageState get initialState {
    return CardImageLoading();
  }

  @override
  Stream<CardImageState> mapEventToState(CardImageEvent event) async* {
    if (event is LoadImage) yield* _mapLoadImageToState(event);

    if (event is UpdateImage) yield* _mapUpdateImageToState(event);
  }

  Stream<CardImageState> _mapUpdateImageToState(
    UpdateImage event,
  ) async* {
    try {
      final imageFilePath = await fileService.fetchFile(event.imageUrl);
      yield CardImageLoaded(imageFilePath);
    } catch (_) {
      yield CardImageNotLoaded();
    }
  }

  Stream<CardImageState> _mapLoadImageToState(
    LoadImage event,
  ) async* {
    _documentSubscription?.cancel();
    _documentSubscription =
        _tasksRepository.snapshots().listen((querySnapshot) {
      querySnapshot.documentChanges.forEach((change) {
        if (change.type == DocumentChangeType.modified &&
            change.document.documentID == event.taskId) {
          dispatch(UpdateImage(change.document.data['imageUrl']));
        } else {
          if (currentState == CardImageLoading()) {
            dispatch(UpdateImage(event.imageUrl));
          }
        }
      });
    });
  }

  @override
  void dispose() {
    _documentSubscription?.cancel();
    super.dispose();
  }
}
