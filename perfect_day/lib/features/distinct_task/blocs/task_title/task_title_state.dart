import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

@immutable
abstract class TaskDetailsTitleState extends Equatable {
  TaskDetailsTitleState([List props = const []]) : super(props);
}

class TitleChanged extends TaskDetailsTitleState {
  final String title;

  TitleChanged(this.title) : super([title]);

  @override
  String toString() => 'TitleChanged { title: $title }';
}

class TitleNotChanged extends TaskDetailsTitleState {
  @override
  String toString() => 'TitleNotChanged';
}

class TitleChanging extends TaskDetailsTitleState {
  @override
  String toString() => 'TitleChanging';
}

class TitleChangeInitial extends TaskDetailsTitleState {
  @override
  String toString() => 'TitleChangeInitial';
}

class TitleKeyboardShowing extends TaskDetailsTitleState {
  final String title;

  TitleKeyboardShowing(this.title) : super([title]);

  @override
  String toString() => 'TitleKeyboardShowing { title: $title }';
}
