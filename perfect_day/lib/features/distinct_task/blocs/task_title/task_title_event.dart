import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

@immutable
abstract class TaskDetailsTitleEvent extends Equatable {
  TaskDetailsTitleEvent([List props = const []]) : super(props);
}

class TitleChange extends TaskDetailsTitleEvent {
  final String title;

  TitleChange(this.title) : super([title]);

  @override
  String toString() => 'TitleChange { date :$title }';
}

class TitleKeyboardShow extends TaskDetailsTitleEvent {
  final String title;

  TitleKeyboardShow(this.title) : super([title]);

  @override
  String toString() => 'TitleKeyboardShow { date :$title }';
}
