import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';

import '../../../../core/data/repositories/task_repository/task_repository.dart';
import '../blocs.dart';

class TaskDetailsTitleBloc
    extends Bloc<TaskDetailsTitleEvent, TaskDetailsTitleState> {
  final TaskRepository _taskRepository;
  final String _taskId;

  TaskDetailsTitleBloc(
      {@required TaskRepository taskRepository, @required String taskId})
      : assert(taskRepository != null, taskId != null),
        _taskRepository = taskRepository,
        _taskId = taskId;

  @override
  TaskDetailsTitleState get initialState => TitleChangeInitial();

  @override
  Stream<TaskDetailsTitleState> mapEventToState(
      TaskDetailsTitleEvent event) async* {
    if (event is TitleKeyboardShow) yield* _mapShowTitleKeyboardState(event);

    if (event is TitleChange) yield* _mapChangeTitleState(event);
  }

  Stream<TaskDetailsTitleState> _mapChangeTitleState(
    TitleChange event,
  ) async* {
    yield TitleChanging();

    var oldTask = await _taskRepository.getTask(_taskId);

    if (event.title != null && oldTask.title != event.title) {
      var updatedTask =
          await _taskRepository.updateTaskTitle(_taskId, event.title);

      yield TitleChanged(updatedTask.title);
    } else {
      yield TitleNotChanged();
    }
  }

  Stream<TaskDetailsTitleState> _mapShowTitleKeyboardState(
    TitleKeyboardShow event,
  ) async* {
    yield TitleKeyboardShowing(event.title);
  }
}
