import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

@immutable
abstract class TaskDetailsDateEvent extends Equatable {
  TaskDetailsDateEvent([List props = const []]) : super(props);
}

class DueDateChange extends TaskDetailsDateEvent {
  final DateTime date;

  DueDateChange(this.date) : super([date]);

  @override
  String toString() => 'DueDateChange { date: $date }';
}

class DueDateDialogShow extends TaskDetailsDateEvent {
  final DateTime date;

  DueDateDialogShow(this.date) : super([date]);

  @override
  String toString() => 'DueDateDialogShow { date: $date }';
}
