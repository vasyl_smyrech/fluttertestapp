import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

@immutable
abstract class TaskDetailsDateState extends Equatable {
  TaskDetailsDateState([List props = const []]) : super(props);
}

class DueDateChanged extends TaskDetailsDateState {
  final DateTime date;

  DueDateChanged(this.date) : super([date]);

  @override
  String toString() => 'DueDateChanged { date: $date }';
}

class DueDateNotChanged extends TaskDetailsDateState {
  @override
  String toString() => 'DueDateNotChanged';
}

class DueDateChanging extends TaskDetailsDateState {
  @override
  String toString() => 'DueDateChanging';
}

class DueDateChangeInitial extends TaskDetailsDateState {
  @override
  String toString() => 'DueDateChangeInitial';
}

class DueDateDialogShowing extends TaskDetailsDateState {
  final DateTime date;

  DueDateDialogShowing(this.date) : super([date]);

  @override
  String toString() => 'DueDateDialogShowing { date: $date }';
}
