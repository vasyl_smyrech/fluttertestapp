import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';

import '../../../../core/data/repositories/task_repository/task_repository.dart';
import '../blocs.dart';

class TaskDetailsDateBloc
    extends Bloc<TaskDetailsDateEvent, TaskDetailsDateState> {
  final TaskRepository _taskRepository;
  final String _taskId;

  TaskDetailsDateBloc(
      {@required TaskRepository taskRepository, @required String taskId})
      : assert(taskRepository != null, taskId != null),
        _taskRepository = taskRepository,
        _taskId = taskId;

  @override
  TaskDetailsDateState get initialState => DueDateChangeInitial();

  @override
  Stream<TaskDetailsDateState> mapEventToState(
      TaskDetailsDateEvent event) async* {
    if (event is DueDateDialogShow) {
      yield* _mapShowDueDateTaskDialogState(event);
    }
    if (event is DueDateChange) {
      yield* _mapChangeDueDateTaskState(event);
    }
  }

  Stream<TaskDetailsDateState> _mapChangeDueDateTaskState(
    DueDateChange event,
  ) async* {
    yield DueDateChanging();

    if (event.date != null) {
      var updatedTask =
          await _taskRepository.updateTaskDueDate(_taskId, event.date);

      yield DueDateChanged(updatedTask.dueDateTime);
    } else {
      yield DueDateNotChanged();
    }
  }

  Stream<TaskDetailsDateState> _mapShowDueDateTaskDialogState(
    DueDateDialogShow event,
  ) async* {
    yield DueDateDialogShowing(event.date);
  }
}
