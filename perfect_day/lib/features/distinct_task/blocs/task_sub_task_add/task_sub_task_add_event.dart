import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

@immutable
abstract class TaskDetailsSubTaskAddEvent extends Equatable {
  TaskDetailsSubTaskAddEvent([List props = const []]) : super(props);
}

class SubTaskAdd extends TaskDetailsSubTaskAddEvent {
  final String content;

  SubTaskAdd(this.content) : super([content]);

  @override
  String toString() => 'SubTaskAdd { content: $content }';
}

class SubTaskAddHintClear extends TaskDetailsSubTaskAddEvent {
  SubTaskAddHintClear() : super([]);

  @override
  String toString() => 'SubTaskAddHintClear';
}
