import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

import '../../../../core/data/repositories/task_repository/task_repository.dart';

@immutable
abstract class TaskDetailsSubTaskAddState extends Equatable {
  TaskDetailsSubTaskAddState([List props = const []]) : super(props);
}

class SubTaskAddInitial extends TaskDetailsSubTaskAddState {
  @override
  String toString() => 'SubTaskAddInitial';
}

class SubTaskAdded extends TaskDetailsSubTaskAddState {
  final SubTask subTask;

  SubTaskAdded(this.subTask) : super([subTask]);

  @override
  String toString() => 'SubTaskAdded { subTask: $subTask }';
}

class SubTaskNotAdded extends TaskDetailsSubTaskAddState {
  @override
  String toString() => 'SubTaskNotAdded';
}

class SubTaskAdding extends TaskDetailsSubTaskAddState {
  @override
  String toString() => 'SubTaskAdding';
}

class SubTaskHintCleared extends TaskDetailsSubTaskAddState {
  @override
  String toString() => 'SubTaskHintCleared';
}
