import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';

import '../../../../core/data/repositories/task_repository/task_repository.dart';
import '../blocs.dart';

class TaskDetailsSubTaskAddBloc
    extends Bloc<TaskDetailsSubTaskAddEvent, TaskDetailsSubTaskAddState> {
  final TaskRepository _taskRepository;
  final String _taskId;

  TaskDetailsSubTaskAddBloc(
      {@required TaskRepository taskRepository, @required String taskId})
      : assert(taskRepository != null),
        assert(taskId != null),
        _taskRepository = taskRepository,
        _taskId = taskId;

  @override
  TaskDetailsSubTaskAddState get initialState => SubTaskAddInitial();

  @override
  Stream<TaskDetailsSubTaskAddState> mapEventToState(
      TaskDetailsSubTaskAddEvent event) async* {
    if (event is SubTaskAddHintClear) yield* _mapSubTaskHintClearState(event);

    if (event is SubTaskAdd) yield* _mapSubTaskAddState(event);
  }

  Stream<TaskDetailsSubTaskAddState> _mapSubTaskAddState(
    SubTaskAdd event,
  ) async* {
    yield SubTaskAdding();

    var subTask = SubTask(
        content: event.content, isCompleted: false, createdAt: DateTime.now());

    try {
      var addedSubTask = await _taskRepository.addSubTask(subTask, _taskId);

      if (addedSubTask != null) {
        yield SubTaskAdded(addedSubTask);
      } else {
        yield SubTaskNotAdded();
      }
    } catch (e) {
      yield SubTaskNotAdded();
      print(e.toString());
    }
  }

  Stream<TaskDetailsSubTaskAddState> _mapSubTaskHintClearState(
    SubTaskAddHintClear event,
  ) async* {
    yield SubTaskHintCleared();
  }
}
