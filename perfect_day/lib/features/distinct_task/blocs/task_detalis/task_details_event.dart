import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

import '../../../../core/data/repositories/task_repository/task_repository.dart';

@immutable
abstract class TaskDetailsEvent extends Equatable {
  TaskDetailsEvent([List props = const []]) : super(props);
}

class TaskLoad extends TaskDetailsEvent {
  final String taskId;

  TaskLoad(this.taskId) : super([taskId]);

  @override
  String toString() => 'TaskLoad { title :$taskId }';
}

//class TaskUpdate extends TaskDetailsEvent {
////  final Task task;
////
////  TaskUpdate(this.task) : super([task]);
//
//  @override
//  String toString() => 'TaskUpdate';
//}

//class TitleChange extends TaskDetailsEvent {
//  final String title;
//
//  TitleChange({@required this.title}) : super([title]);
//
//  @override
//  String toString() => 'TitleChange { title :$title }';
//}

//class SubTaskChange extends TaskDetailsEvent {
//  final String subTaskText;
//
//  SubTaskChange({@required this.subTaskText}) : super([subTaskText]);
//
//  @override
//  String toString() => 'SubTaskChange { subTaskText :$subTaskText }';
//}
//
//class SubTaskAdd extends TaskDetailsEvent {
//  final String subTaskText;
//
//  SubTaskAdd({@required this.subTaskText}) : super([subTaskText]);
//
//  @override
//  String toString() => 'SubTaskAdd { subTaskText :$subTaskText }';
//}

//class DurationChange extends TaskDetailsEvent {
//  final int duration;
//
//  DurationChange({@required this.duration}) : super([duration]);
//
//  @override
//  String toString() => 'DurationChange { duration :$duration }';
//}

//class DueDateChange extends TaskDetailsEvent {
//  final DateTime date;
//
//  DueDateChange(this.date) : super([date]);
//
//  @override
//  String toString() => 'DueDateChange { duration :$date }';
//}

class TaskUpdate extends TaskDetailsEvent {
  final Task task;

  TaskUpdate(this.task) : super([task]);

  @override
  String toString() => 'TaskUpdated { duration :$task }';
}
