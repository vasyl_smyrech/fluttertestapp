import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

import '../../../../core/data/repositories/task_repository/task_repository.dart';

@immutable
abstract class TaskDetailsState extends Equatable {
  TaskDetailsState([List props = const []]) : super(props);
}

class TaskLoading extends TaskDetailsState {
  @override
  String toString() => 'TaskLoading';
}

class TaskLoaded extends TaskDetailsState {
  final Task task;

  TaskLoaded(this.task) : super([task]);

  @override
  String toString() => 'TaskLoaded { task: $task }';
}

class TaskNotLoaded extends TaskDetailsState {
  @override
  String toString() => 'TaskNotLoaded';
}

//class TitleChanged extends TaskDetailsState {
//  final String title;
//
//  TitleChanged(this.title) : super([title]);
//
//  @override
//  String toString() => 'TitleChanged { title: $title }';
//}
//
//class SubTaskChanged extends TaskDetailsState {
//  final String subTaskText;
//
//  SubTaskChanged(this.subTaskText) : super([subTaskText]);
//
//  @override
//  String toString() => 'SubTaskChanged { title: $subTaskText }';
//}
//
//class SubTaskAdded extends TaskDetailsState {
//  final String subTaskText;
//
//  SubTaskAdded(this.subTaskText) : super([subTaskText]);
//
//  @override
//  String toString() => 'SubTaskAdded { title: $subTaskText }';
//}
//
//class DurationChanged extends TaskDetailsState {
//  final int duration;
//
//  DurationChanged(this.duration) : super([duration]);
//
//  @override
//  String toString() => 'DurationChange { duration :$duration }';
//}

//class DueDateChanged extends TaskDetailsState {
//  final DateTime date;
//
//  DueDateChanged(this.date) : super([date]);
//
//  @override
//  String toString() => 'DueDateChange { duration :$date }';
//}

//class DueDateChanging extends TaskDetailsState {
//  final DateTime date;
//
//  DueDateChanging(this.date) : super([date]);
//
//  @override
//  String toString() => 'DueDateChanging { duration :$date }';
//}

class TaskUpdating extends TaskDetailsState {
  final Task task;

  TaskUpdating(this.task) : super([task]);

  @override
  String toString() => 'TaskUpdating { title :$task }';
}

class TaskUpdated extends TaskDetailsState {
//  final Task task;
//
//  TaskUpdated(this.task) : super([task]);

  @override
  String toString() => 'TaskUpdated';
}

class TaskNotUpdated extends TaskDetailsState {
  final Task task;

  TaskNotUpdated(this.task) : super([task]);

  @override
  String toString() => 'TaskNotUpdated { title :$task }';
}
