import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';

import '../../../../core/data/repositories/task_repository/task_repository.dart';
import '../blocs.dart';

class TaskDetailsBloc extends Bloc<TaskDetailsEvent, TaskDetailsState> {
  final TaskRepository _taskRepository;
  StreamSubscription _taskSubscription;
  final String _taskId;
  Task _task;

  TaskDetailsBloc(
      {@required TaskRepository taskRepository, @required String taskId})
      : assert(taskRepository != null, taskId != null),
        _taskRepository = taskRepository,
        _taskId = taskId;

  @override
  TaskDetailsState get initialState => TaskLoading();

//  @override
//  Stream<TaskDetailsState> mapEventToState(TaskDetailsEvent event) async* {
//    if (event is TaskLoad) {
//      yield TaskLoading();
//      _task = await _taskRepository.getTask(_taskId);
//
//      if (_task != null) {
//        yield TaskLoaded(_task);
//      } else {
//        yield TaskNotLoaded();
//      }
//    } else if (event is TaskUpdate) {
//      yield TaskUpdating(_task);
//      await _taskRepository.updateTask(event.task);
//      _task = event.task;
//      yield TaskUpdated(_task);
//    } else if (event is TitleChange) {
//      yield TaskUpdating(_task);
//      var updatedTask =
//          await _taskRepository.updateTaskTitle(_taskId, event.title);
//      yield TitleChanged(updatedTask.title);
//      _task = updatedTask;
//      yield TaskUpdated(_task);
//    } else if (event is DueDateChange) {
//      yield DueDateChanging(event.date);
//      var updatedTask =
//          await _taskRepository.updateTaskDueDate(_taskId, event.date);
//      yield DueDateChanged(event.date);
//      _task = updatedTask;
//      yield TaskUpdated(_task);
//    }else if(event is DurationChange) {
//      yield TaskUpdating(_task);
//      var updatedTask =
//      await _taskRepository.updateTaskDuration(_taskId, event.duration);
//      yield DurationChanged(event.duration);
//      _task = updatedTask;
//      yield TaskUpdated(_task);
//    }
//  }

  @override
  Stream<TaskDetailsState> mapEventToState(TaskDetailsEvent event) async* {
    if (event is TaskLoad) yield* _mapLoadTaskState(event);
    if (event is TaskUpdate) yield* _mapUpdateTaskState(event);
//    } else if (event is DueDateChange) {
//      yield* _mapChangeDueDateTaskState(event);
//    }
  }

  Stream<TaskDetailsState> _mapLoadTaskState(
    TaskLoad event,
  ) async* {
//    _taskSubscription?.cancel();
//    _taskSubscription = _taskRepository.tasks().listen(
//      (tasks) {
//        dispatch(
//          TaskUpdate(tasks.firstWhere((item) => item.id == _taskId)),
//        );
//      },
//    );

    try {
      _task = await _taskRepository.getTask(_taskId);
      yield TaskLoaded(_task);
    } catch (_) {
      yield TaskNotLoaded();
    }
  }

  Stream<TaskDetailsState> _mapUpdateTaskState(
    TaskUpdate event,
  ) async* {
    try {
      _task = await _taskRepository.getTask(_taskId);
      yield TaskLoaded(_task);
    } catch (_) {
      yield TaskNotLoaded();
    }
  }

//  Stream<TaskDetailsState> _mapChangeDueDateTaskState(
//    DueDateChange event,
//  ) async* {
//    if (currentState is TaskLoaded) {
////      yield DueDateChanging(event.date);
//        yield DueDateChanged(event.date);
//
//      var updatedTask =
//          await _taskRepository.updateTaskDueDate(_taskId, event.date);
//      yield TaskLoaded(updatedTask);
//    }
//  }

  @override
  void dispose() {
    _taskSubscription?.cancel();
    super.dispose();
  }
}
