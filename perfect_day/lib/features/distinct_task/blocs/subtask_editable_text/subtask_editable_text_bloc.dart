import 'package:bloc/bloc.dart';

import '../blocs.dart';

class SubTaskEditableBloc
    extends Bloc<SubTaskEditableEvent, SubTaskEditableState> {
  @override
  SubTaskEditableState get initialState => SubTaskEditableRebuildInitial();

  @override
  Stream<SubTaskEditableState> mapEventToState(
      SubTaskEditableEvent event) async* {
    if (event is SubTaskEditableRebuild) yield* _mapRebuildEditableState(event);
  }

  Stream<SubTaskEditableState> _mapRebuildEditableState(
      SubTaskEditableRebuild event) async* {
    yield SubTaskEditableWasRebuild(event.isEditable);
  }
}
