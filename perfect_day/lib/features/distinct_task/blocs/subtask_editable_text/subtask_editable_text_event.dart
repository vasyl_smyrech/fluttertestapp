import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

@immutable
abstract class SubTaskEditableEvent extends Equatable {
  SubTaskEditableEvent([List props = const []]) : super(props);
}

class SubTaskEditableRebuild extends SubTaskEditableEvent {
  final bool isEditable;

  SubTaskEditableRebuild(this.isEditable) : super([isEditable]);

  @override
  String toString() => 'SubTaskEditableRebuild { isEditable: $isEditable }';
}
