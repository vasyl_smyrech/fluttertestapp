import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

@immutable
abstract class SubTaskEditableState extends Equatable {
  SubTaskEditableState([List props = const []]) : super(props);
}

class SubTaskEditableWasRebuild extends SubTaskEditableState {
  final bool isEditable;

  SubTaskEditableWasRebuild(this.isEditable) : super([isEditable]);

  @override
  String toString() => 'SubTaskEditableWasRebuild { isEditable: $isEditable }';
}

class SubTaskEditableRebuildInitial extends SubTaskEditableState {
  @override
  String toString() => 'SubTaskEditableRebuildInitial';
}
