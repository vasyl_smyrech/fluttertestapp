import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

@immutable
abstract class TaskDetailsImageEvent extends Equatable {
  TaskDetailsImageEvent([List props = const []]) : super(props);
}

class TitleImageLoad extends TaskDetailsImageEvent {
  final String imageUrl;

  TitleImageLoad(this.imageUrl) : super([imageUrl]);

  @override
  String toString() => 'DurationSliderRebuild { imageUrl: $imageUrl }';
}

class TitleImagePickerShow extends TaskDetailsImageEvent {
  @override
  String toString() => 'TitleImagePickerShow';
}

class TitleImageChange extends TaskDetailsImageEvent {
  final String filePath;

  TitleImageChange(this.filePath) : super([filePath]);

  @override
  String toString() => 'TitleImageChange { imageUrl: $filePath }';
}
