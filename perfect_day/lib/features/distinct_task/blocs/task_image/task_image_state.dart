import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

@immutable
abstract class TaskDetailsImageState extends Equatable {
  TaskDetailsImageState([List props = const []]) : super(props);
}

class TitleImageLoading extends TaskDetailsImageState {
  @override
  String toString() => 'TitleImageLoading';
}

class TitleImageNotLoaded extends TaskDetailsImageState {
  @override
  String toString() => 'TitleImageNotLoaded';
}

class TitleImageLoaded extends TaskDetailsImageState {
  final String imagePath;

  TitleImageLoaded(this.imagePath) : super([imagePath]);

  @override
  String toString() => 'TitleImageLoaded { imagePath: $imagePath }';
}

class TitleImagePickerShowing extends TaskDetailsImageState {
  @override
  String toString() => 'TitleImagePickerShowing';
}

class TitleImageChanging extends TaskDetailsImageState {
  @override
  String toString() => 'TitleImageChanging';
}

class TitleImageChanged extends TaskDetailsImageState {
  final String imagePath;

  TitleImageChanged(this.imagePath) : super([imagePath]);

  @override
  String toString() => 'TitleImageChanged { imagePath: $imagePath }';
}

class TitleImageNotChanged extends TaskDetailsImageState {
  @override
  String toString() => 'TitleImageChanging';
}
