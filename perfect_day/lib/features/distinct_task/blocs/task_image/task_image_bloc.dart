import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';

import '../../../../core/data/repositories/task_repository/task_repository.dart';
import '../../../../core/domain/services/file_service.dart';
import '../blocs.dart';

class TaskDetailsImageBloc
    extends Bloc<TaskDetailsImageEvent, TaskDetailsImageState> {
  final TaskRepository _tasksRepository;
  final String _taskId;

  TaskDetailsImageBloc(
      {@required TaskRepository tasksRepository, @required String taskId})
      : assert(tasksRepository != null, taskId != null),
        _tasksRepository = tasksRepository,
        _taskId = taskId;

  final _fileService = FileService();

  @override
  TaskDetailsImageState get initialState => TitleImageLoading();

  @override
  Stream<TaskDetailsImageState> mapEventToState(
      TaskDetailsImageEvent event) async* {
    if (event is TitleImageLoad) yield* _mapTitleImageLoadState(event);

    if (event is TitleImagePickerShow) yield* _mapTitleImagePickerState(event);

    if (event is TitleImageChange) yield* _mapTitleImageChangeState(event);
  }

  Stream<TaskDetailsImageState> _mapTitleImageLoadState(
    TitleImageLoad event,
  ) async* {
    var imageFilePath;
    try {
      imageFilePath = await _fileService.fetchFile(event.imageUrl);
    } catch (_) {
      yield TitleImageNotLoaded();
    }
    if (imageFilePath != null) yield TitleImageLoaded(imageFilePath);
  }

  Stream<TaskDetailsImageState> _mapTitleImagePickerState(
    TitleImagePickerShow event,
  ) async* {
    yield TitleImagePickerShowing();
  }

  Stream<TaskDetailsImageState> _mapTitleImageChangeState(
    TitleImageChange event,
  ) async* {
    if (event.filePath != null) {
      TitleImageLoading();

      try {
        _fileService
            .saveFile(event.filePath, storageFolderPath: "images")
            .then((result) {
          _tasksRepository.updateTaskImage(
              taskId: _taskId, fireBaseFileUrl: result);
          dispatch(TitleImageLoad(result));
        });
      } catch (_) {
        yield TitleImageNotLoaded();
      }
    } else
      yield TitleImageNotLoaded();
  }
}
