import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

import '../../../../core/data/repositories/task_repository/task_repository.dart';

@immutable
abstract class SubTasksEvent extends Equatable {
  SubTasksEvent([List props = const []]) : super(props);
}

class SubTasksLoad extends SubTasksEvent {
  @override
  String toString() => 'SubTasksLoad';
}

class SubTasksUpdate extends SubTasksEvent {
  final List<SubTask> subTasks;

  SubTasksUpdate(this.subTasks);

  @override
  String toString() => 'SubTasksUpdate { subTasks: $subTasks }';
}
