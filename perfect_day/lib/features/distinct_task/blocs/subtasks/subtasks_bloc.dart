import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';

import '../../../../core/data/repositories/task_repository/task_repository.dart';
import '../blocs.dart';

class SubTasksBloc extends Bloc<SubTasksEvent, SubTasksState> {
  final TaskRepository _tasksRepository;
  final String _taskId;
  StreamSubscription _tasksSubscription;

  SubTasksBloc(
      {@required TaskRepository tasksRepository, @required String taskId})
      : assert(tasksRepository != null, taskId != null),
        _tasksRepository = tasksRepository,
        _taskId = taskId;

  @override
  SubTasksState get initialState => SubTasksLoading();

  @override
  Stream<SubTasksState> mapEventToState(SubTasksEvent event) async* {
    if (event is SubTasksLoad) {
      yield* _mapSubTasksLoadToState();
    } else if (event is SubTasksUpdate) {
      yield* _mapSubTasksUpdateToState(event);
    }
  }

  Stream<SubTasksState> _mapSubTasksLoadToState() async* {
    _tasksSubscription?.cancel();
    _tasksSubscription = _tasksRepository.subTasksForTask(_taskId).listen(
      (subTasks) {
        dispatch(
          SubTasksUpdate(subTasks),
        );
      },
    );
  }

  Stream<SubTasksState> _mapSubTasksUpdateToState(SubTasksUpdate event) async* {
    yield SubTasksLoaded(event.subTasks);
  }

  @override
  void dispose() {
    _tasksSubscription?.cancel();
    super.dispose();
  }
}
