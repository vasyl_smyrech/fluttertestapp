import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

import '../../../../core/data/repositories/task_repository/task_repository.dart';

@immutable
abstract class SubTasksState extends Equatable {
  SubTasksState([List props = const []]) : super(props);
}

class SubTasksLoading extends SubTasksState {
  @override
  String toString() => 'SubTasksLoading';
}

class SubTasksLoaded extends SubTasksState {
  final List<SubTask> subTasks;

  SubTasksLoaded([this.subTasks = const []]) : super([subTasks]);

  @override
  String toString() => 'SubTasksLoaded { tasks: $subTasks }';
}

class SubTasksNotLoaded extends SubTasksState {
  @override
  String toString() => 'SubTasksNotLoaded';
}
