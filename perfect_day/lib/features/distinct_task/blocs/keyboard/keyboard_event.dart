import 'package:meta/meta.dart';
import 'package:equatable/equatable.dart';

@immutable
abstract class KeyboardEvent extends Equatable {
  KeyboardEvent([List props = const []]) : super(props);
}

class KeyboardClose extends KeyboardEvent {
  @override
  String toString() => 'KeyboardClose';
}
