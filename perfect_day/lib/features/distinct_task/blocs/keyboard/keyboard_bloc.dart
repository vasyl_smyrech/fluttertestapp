import 'package:bloc/bloc.dart';

import '../blocs.dart';

class KeyboardBloc extends Bloc<KeyboardEvent, KeyboardState> {
  @override
  KeyboardState get initialState => KeyboardInitial();

  @override
  Stream<KeyboardState> mapEventToState(KeyboardEvent event) async* {
    if (event is KeyboardClose) yield* _mapCloseKeyboardState();
  }

  Stream<KeyboardState> _mapCloseKeyboardState() async* {
    yield KeyboardClosed();
    print('privet');
  }
}
