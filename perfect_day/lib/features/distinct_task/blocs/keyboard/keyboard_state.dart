import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

@immutable
abstract class KeyboardState extends Equatable {
  KeyboardState([List props = const []]) : super(props);
}

class KeyboardClosed extends KeyboardState {
  @override
  String toString() => 'KeyboardClosed';
}

class KeyboardInitial extends KeyboardState {
  @override
  String toString() => 'KeyboardInitial';
}
