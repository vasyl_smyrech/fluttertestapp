import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';

import '../../../../core/data/repositories/task_repository/task_repository.dart';
import '../blocs.dart';

class TaskDetailsDurationBloc
    extends Bloc<TaskDetailsDurationEvent, TaskDetailsDurationState> {
  final TaskRepository _taskRepository;
  final String _taskId;

  TaskDetailsDurationBloc(
      {@required TaskRepository taskRepository, @required String taskId})
      : assert(taskRepository != null, taskId != null),
        _taskRepository = taskRepository,
        _taskId = taskId;

  @override
  TaskDetailsDurationState get initialState => DurationChangeInitial();

  @override
  Stream<TaskDetailsDurationState> mapEventToState(
      TaskDetailsDurationEvent event) async* {
    if (event is DurationDialogShow) {
      yield* _mapDurationDialogShowState(event);
    }
//    if (event is DurationDialogRebuild) {
//      yield* _mapDurationDialogRebuildState(event);
//    }
    if (event is DurationChange) yield* _mapChangeTaskDurationState(event);
  }

  Stream<TaskDetailsDurationState> _mapChangeTaskDurationState(
    DurationChange event,
  ) async* {
    yield DurationChanging();

    if (event.newDuration != null &&
        event.newDuration != event.oldDuration &&
        event.isOk == true) {
      var updatedTask =
          await _taskRepository.updateTaskDuration(_taskId, event.newDuration);

      yield DurationChanged(updatedTask.duration);
    } else {
      yield DurationNotChanged();
    }
  }

  Stream<TaskDetailsDurationState> _mapDurationDialogShowState(
    DurationDialogShow event,
  ) async* {
    yield DurationDialogShowing(event.duration);
  }

//  Stream<TaskDetailsDurationState> _mapDurationDialogRebuildState(
//      DurationDialogRebuild event,
//      ) async* {
//    yield DurationDialogRebuilt(oldDuration: event.oldDuration, newDuration: event.newDuration);
//  }

//  Stream<TaskDetailsDurationState> _mapDurationDialogHideState(
//      DurationDialogHide event,
//      ) async* {
//    yield DurationDialogHided(event.minutes);
//  }
}
