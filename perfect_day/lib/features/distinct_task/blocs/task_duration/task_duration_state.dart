import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

@immutable
abstract class TaskDetailsDurationState extends Equatable {
  TaskDetailsDurationState([List props = const []]) : super(props);
}

class DurationChanged extends TaskDetailsDurationState {
  final int duration;

  DurationChanged(this.duration) : super([duration]);

  @override
  String toString() => 'DurationChanged { duration: $duration }';
}

//class DurationDialogRebuilt extends TaskDetailsDurationState
//{
//  final int oldDuration;
//  final int newDuration;
//
//  DurationDialogRebuilt({this.oldDuration, this.newDuration}) : super([oldDuration, newDuration]);
//
//  @override
//  String toString() => 'DurationDialogRebuilt { oldDuration: $oldDuration, newDuration: $newDuration }';
//}

class DurationNotChanged extends TaskDetailsDurationState {
  @override
  String toString() => 'DurationNotChanged';
}

class DurationChanging extends TaskDetailsDurationState {
  @override
  String toString() => 'DurationChanging';
}

class DurationChangeInitial extends TaskDetailsDurationState {
  @override
  String toString() => 'DurationChangeInitial';
}

class DurationDialogShowing extends TaskDetailsDurationState {
  final int duration;

  DurationDialogShowing(this.duration) : super([duration]);

  @override
  String toString() => 'DurationDialogShowed { duration: $duration }';
}

class DurationDialogHided extends TaskDetailsDurationState {
  final int duration;

  DurationDialogHided(this.duration) : super([duration]);

  @override
  String toString() => 'DurationDialogHided { duration: $duration }';
}
