import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

@immutable
abstract class TaskDetailsDurationEvent extends Equatable {
  TaskDetailsDurationEvent([List props = const []]) : super(props);
}

class DurationChange extends TaskDetailsDurationEvent {
  final int oldDuration;
  final int newDuration;
  final bool isOk;

  DurationChange({this.oldDuration, this.newDuration, this.isOk})
      : super([oldDuration, newDuration, isOk]);

  @override
  String toString() =>
      'DurationChange { from $oldDuration to $newDuration, isOk: $isOk }';
}

class DurationDialogShow extends TaskDetailsDurationEvent {
  final int duration;

  DurationDialogShow(this.duration) : super([duration]);

  @override
  String toString() => 'DurationDialogShow { duration: $duration }';
}

//class DurationDialogRebuild extends TaskDetailsDurationEvent
//{
//  final int oldDuration;
//  final int newDuration;
//
//  DurationDialogRebuild({this.oldDuration, this.newDuration}) : super([oldDuration, newDuration]);
//
//  @override
//  String toString() => 'DurationDialogRebuild { from: $oldDuration to $newDuration }';
//}

//class DurationDialogHide extends TaskDetailsDurationEvent
//{
//  final int minutes;
//
//  DurationDialogHide(this.minutes) : super([minutes]);
//
//  @override
//  String toString() => 'DurationDialogHide { minutes: $minutes }';
//}
