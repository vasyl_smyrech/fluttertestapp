import 'dart:async';

import 'package:bloc/bloc.dart';

import '../blocs.dart';

class TaskDetailsDurationSliderBloc extends Bloc<TaskDetailsDurationSliderEvent,
    TaskDetailsDurationSliderState> {
  TaskDetailsDurationSliderBloc();

  @override
  TaskDetailsDurationSliderState get initialState => DurationSliderInitial();

  @override
  Stream<TaskDetailsDurationSliderState> mapEventToState(
      TaskDetailsDurationSliderEvent event) async* {
    if (event is DurationSliderRebuild) {
      yield* _mapDurationSliderRebuildState(event);
    }
  }

  Stream<TaskDetailsDurationSliderState> _mapDurationSliderRebuildState(
    DurationSliderRebuild event,
  ) async* {
    yield DurationSliderRebuilt(
        oldDuration: event.oldDuration, newDuration: event.newDuration);
  }
}
