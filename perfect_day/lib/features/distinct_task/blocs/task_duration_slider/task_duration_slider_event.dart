import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

@immutable
abstract class TaskDetailsDurationSliderEvent extends Equatable {
  TaskDetailsDurationSliderEvent([List props = const []]) : super(props);
}

class DurationSliderRebuild extends TaskDetailsDurationSliderEvent {
  final int oldDuration;
  final int newDuration;

  DurationSliderRebuild({this.oldDuration, this.newDuration})
      : super([oldDuration, newDuration]);

  @override
  String toString() =>
      'DurationSliderRebuild { from: $oldDuration to $newDuration }';
}
