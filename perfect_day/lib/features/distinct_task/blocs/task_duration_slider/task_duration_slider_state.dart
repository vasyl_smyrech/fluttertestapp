import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

@immutable
abstract class TaskDetailsDurationSliderState extends Equatable {
  TaskDetailsDurationSliderState([List props = const []]) : super(props);
}

class DurationSliderInitial extends TaskDetailsDurationSliderState {
  @override
  String toString() => 'DurationSliderInitial';
}

class DurationSliderRebuilt extends TaskDetailsDurationSliderState {
  final int oldDuration;
  final int newDuration;

  DurationSliderRebuilt({this.oldDuration, this.newDuration})
      : super([oldDuration, newDuration]);

  @override
  String toString() =>
      'DurationSliderRebuilt { oldDuration: $oldDuration, newDuration: $newDuration }';
}
