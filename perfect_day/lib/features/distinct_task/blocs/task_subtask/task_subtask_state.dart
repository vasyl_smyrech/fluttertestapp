import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

import '../../../../core/data/repositories/task_repository/task_repository.dart';

@immutable
abstract class TaskDetailsSubTaskState extends Equatable {
  TaskDetailsSubTaskState([List props = const []]) : super(props);
}

class SubTaskChanged extends TaskDetailsSubTaskState {
  final SubTask subTask;

  SubTaskChanged(this.subTask) : super([subTask]);

  @override
  String toString() => 'SubTaskChanged { subTask: $subTask }';
}

class SubTaskNotChanged extends TaskDetailsSubTaskState {
  @override
  String toString() => 'SubTaskNotChanged';
}

class SubTaskChanging extends TaskDetailsSubTaskState {
  @override
  String toString() => 'SubTaskChanging';
}

class SubTaskInitial extends TaskDetailsSubTaskState {
  @override
  String toString() => 'SubTaskInitial';
}

class SubTaskKeyboardShowing extends TaskDetailsSubTaskState {
  final SubTask subTask;

  SubTaskKeyboardShowing(this.subTask) : super([subTask]);

  @override
  String toString() => 'SubTaskKeyboardShowing { subTask: $subTask }';
}

class SubTaskDeleting extends TaskDetailsSubTaskState {
  @override
  String toString() => 'SubTaskDeleting';
}

class SubTaskDeleted extends TaskDetailsSubTaskState {
  @override
  String toString() => 'SubTaskDeleted';
}

class SubTaskNotDeleted extends TaskDetailsSubTaskState {
  @override
  String toString() => 'SubTaskNotDeleted';
}

class SubTaskDeletePopUpShowed extends TaskDetailsSubTaskState {
  @override
  String toString() => 'SubTaskDeletePopUpShowed';
}

class SubTaskDeletePopUpHided extends TaskDetailsSubTaskState {
  @override
  String toString() => 'SubTaskDeletePopUpHided';
}

class SubTaskEditing extends TaskDetailsSubTaskState {
  @override
  String toString() => 'SubTaskEditing';
}

class SubTaskEditCanceled extends TaskDetailsSubTaskState {
  @override
  String toString() => 'SubTaskEditCanceled';
}

class SubTaskTextFieldFocusGot extends TaskDetailsSubTaskState {
  @override
  String toString() => 'SubTaskTextFieldFocusGot';
}
