import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';

import '../../../../core/data/repositories/task_repository/task_repository.dart';
import '../blocs.dart';

class TaskDetailsSubTaskBloc
    extends Bloc<TaskDetailsSubTaskEvent, TaskDetailsSubTaskState> {
  final TaskRepository _taskRepository;
  final String _taskId;
  final String _subTaskId;

  TaskDetailsSubTaskBloc(
      {@required TaskRepository taskRepository,
      @required String taskId,
      @required String subTaskId})
      : assert(taskRepository != null, subTaskId != null),
        _taskRepository = taskRepository,
        _taskId = taskId,
        _subTaskId = subTaskId;

  @override
  TaskDetailsSubTaskState get initialState => SubTaskInitial();

  @override
  Stream<TaskDetailsSubTaskState> mapEventToState(
      TaskDetailsSubTaskEvent event) async* {
    if (event is SubTaskDeletePopUpShow) {
      yield* _mapDeleteSubTaskPopUpState(event);
    }
    if (event is SubTaskDeletePopUpHide) {
      yield* _mapDeleteSubTaskPopUpHideState(event);
    }
    if (event is SubTaskContentChange) {
      yield* _mapChangeSubTaskState(event);
    }
    if (event is SubTaskCompletionChange) {
      yield* _mapChangeSubTaskCompletionState(event);
    }
    if (event is SubTaskDelete) {
      yield* _mapDeleteSubTaskState(event);
    }
    if (event is SubTaskEdit) {
      yield* _mapSubTaskEditState();
    }
    if (event is SubTaskCancelEdit) {
      yield* _mapSubTaskEditCanceledState();
    }
    if (event is SubTaskTextFieldFocusGet) {
      yield* _mapSubTaskFocusGetState();
    }
  }

  Stream<TaskDetailsSubTaskState> _mapChangeSubTaskState(
    SubTaskContentChange event,
  ) async* {
    yield SubTaskChanging();

    var oldSubTask = await _taskRepository.getSubTask(
        taskId: _taskId, subTaskId: _subTaskId);

    if (oldSubTask.content != event.subTaskContent) {
      var updatedTask = await _taskRepository.updateSubTaskContent(
          content: event.subTaskContent,
          taskId: _taskId,
          subTaskId: _subTaskId);

      yield SubTaskChanged(updatedTask);
    } else {
      yield SubTaskNotChanged();
    }
  }

  Stream<TaskDetailsSubTaskState> _mapChangeSubTaskCompletionState(
    SubTaskCompletionChange event,
  ) async* {
    yield SubTaskChanging();

    var oldSubTask = await _taskRepository.getSubTask(
        taskId: _taskId, subTaskId: _subTaskId);

    if (oldSubTask.isCompleted != event.isCompleted) {
      var updatedTask = await _taskRepository.updateSubTaskCompletion(
          isCompleted: event.isCompleted,
          taskId: _taskId,
          subTaskId: _subTaskId);

      yield SubTaskChanged(updatedTask);
    } else {
      yield SubTaskNotChanged();
    }
  }

  Stream<TaskDetailsSubTaskState> _mapDeleteSubTaskState(
    SubTaskDelete event,
  ) async* {
    yield SubTaskDeleting();
    try {
      await _taskRepository.deleteSubTask(event.subTask, _taskId);
    } catch (_) {
      yield SubTaskNotDeleted();
    }
    yield SubTaskDeleted();
  }

  Stream<TaskDetailsSubTaskState> _mapDeleteSubTaskPopUpState(
    SubTaskDeletePopUpShow event,
  ) async* {
    yield SubTaskDeletePopUpShowed();
  }

  Stream<TaskDetailsSubTaskState> _mapDeleteSubTaskPopUpHideState(
    SubTaskDeletePopUpHide event,
  ) async* {
    yield SubTaskDeletePopUpHided();
  }

  Stream<TaskDetailsSubTaskState> _mapSubTaskEditState() async* {
    yield SubTaskEditing();
  }

  Stream<TaskDetailsSubTaskState> _mapSubTaskEditCanceledState() async* {
    yield SubTaskEditCanceled();
  }

  Stream<TaskDetailsSubTaskState> _mapSubTaskFocusGetState() async* {
    yield SubTaskTextFieldFocusGot();
  }
}
