import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

import '../../../../core/data/repositories/task_repository/task_repository.dart';

@immutable
abstract class TaskDetailsSubTaskEvent extends Equatable {
  TaskDetailsSubTaskEvent([List props = const []]) : super(props);
}

class SubTaskContentChange extends TaskDetailsSubTaskEvent {
  final String subTaskContent;

  SubTaskContentChange(this.subTaskContent) : super([subTaskContent]);

  @override
  String toString() =>
      'SubTaskContentChange { subTaskContent :$subTaskContent }';
}

class SubTaskKeyboardShow extends TaskDetailsSubTaskEvent {
  final SubTask subTask;

  SubTaskKeyboardShow(this.subTask) : super([subTask]);

  @override
  String toString() => 'SubTaskKeyboardShow { subTask: $subTask }';
}

class SubTaskEdit extends TaskDetailsSubTaskEvent {
  final SubTask subTask;

  SubTaskEdit(this.subTask) : super([subTask]);

  @override
  String toString() => 'SubTaskEdit { subTask: $subTask }';
}

class SubTaskCancelEdit extends TaskDetailsSubTaskEvent {
  @override
  String toString() => 'SubTaskCancelEdit';
}

class SubTaskDelete extends TaskDetailsSubTaskEvent {
  final SubTask subTask;

  SubTaskDelete(this.subTask) : super([subTask]);

  @override
  String toString() => 'SubTaskDelete { subTask: $subTask }';
}

class SubTaskDeletePopUpShow extends TaskDetailsSubTaskEvent {
  @override
  String toString() => 'SubTaskDeletePopUpShow';
}

class SubTaskDeletePopUpHide extends TaskDetailsSubTaskEvent {
  @override
  String toString() => 'SubTaskDeletePopUpHide';
}

class SubTaskCompletionChange extends TaskDetailsSubTaskEvent {
  final bool isCompleted;

  SubTaskCompletionChange(this.isCompleted) : super([isCompleted]);

  @override
  String toString() => 'SubTaskCompletionChange { isCompleted: $isCompleted }';
}

class SubTaskTextFieldFocusGet extends TaskDetailsSubTaskEvent {
  @override
  String toString() => 'SubTaskTextFieldFocusGet';
}
