import 'dart:io';

import 'package:file_picker/file_picker.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:intl/intl.dart';
import 'package:percent_indicator/percent_indicator.dart';
import 'package:vibration/vibration.dart';

import '../../../../core/data/repositories/task_repository/task_repository.dart';
import '../../../../core/domain/models/models.dart';
import '../../../../core/helpers/helpers.dart';
import '../../../../core/presentation/blocs/blocs.dart';
import '../../../../core/presentation/widgets/widgets.dart';
import '../../blocs/blocs.dart';
import '../widgets/widgets.dart';

class TaskDetailsArguments {
  final String _taskId;

  TaskDetailsArguments(this._taskId);
}

class TaskDetails extends StatelessWidget {
  static const routeName = '/taskDetails';

  @override
  Widget build(BuildContext context) {
    final TaskDetailsArguments _args =
        ModalRoute.of(context).settings.arguments;
    final TaskRepository _tasksRepository =
        RepositoryProvider.of<TaskRepository>(context);

    return MultiBlocProvider(
        providers: [
          BlocProvider<TaskDetailsBloc>(builder: (context) {
            return TaskDetailsBloc(
              taskRepository: _tasksRepository,
              taskId: _args._taskId,
            )..dispatch(TaskLoad(_args._taskId));
          }),
          BlocProvider<TaskDetailsDateBloc>(builder: (context) {
            return TaskDetailsDateBloc(
              taskRepository: _tasksRepository,
              taskId: _args._taskId,
            );
          }),
          BlocProvider<TaskDetailsTitleBloc>(builder: (context) {
            return TaskDetailsTitleBloc(
              taskRepository: _tasksRepository,
              taskId: _args._taskId,
            );
          }),
          BlocProvider<SubTasksBloc>(builder: (context) {
            return SubTasksBloc(
              tasksRepository: _tasksRepository,
              taskId: _args._taskId,
            )..dispatch(SubTasksLoad());
          }),
          BlocProvider<TaskCompletionBloc>(builder: (context) {
            return TaskCompletionBloc(
              taskRepository: _tasksRepository,
              taskId: _args._taskId,
            )..dispatch(TaskCompletionLoad());
          }),
//          BlocProvider<KeyboardBloc>(builder: (context) {
//            return KeyboardBloc();
//          }),
        ],
        child: MultiRepositoryProvider(
          providers: [
            RepositoryProvider<TaskRepository>(
              builder: (context) => _tasksRepository,
            ),
          ],
          child: AppBackground(child:
              BlocBuilder<TaskDetailsBloc, TaskDetailsState>(
                  builder: (context, state) {
            if (state is TaskLoading) return LoadingIndicator.color(0);

            if (state is TaskLoaded) return TaskDetailsScreen(state.task);

            if (state is TaskNotLoaded) return LoadingIndicator.color(0);

            return LoadingIndicator.color(1);
          })),
        ));
  }
}

class ChangeableImageAppBar extends StatelessWidget {
  final String _taskTitle;

  ChangeableImageAppBar(this._taskTitle);

  @override
  Widget build(BuildContext context) {
    final _imageBloc = BlocProvider.of<TaskDetailsImageBloc>(context);

    return SliverAppBar(
      expandedHeight: 300.0,
      backgroundColor: Colors.transparent,
      leading: IconButton(
          icon: Icon(FontAwesomeIcons.angleLeft,
              size: MeasurerSingleton().screenAwareSize(20.0, context),
              color: Colors.white),
          onPressed: () {
            Navigator.pop(context);
          }),
      actions: <Widget>[
        IconButton(
          icon: Icon(
            FontAwesomeIcons.image,
            size: MeasurerSingleton().screenAwareSize(20.0, context),
            color: Colors.white,
          ),
          tooltip: 'Change the title image',
          onPressed: () {
            _imageBloc.dispatch(TitleImagePickerShow());
          },
        ),
      ],
      floating: true,
      flexibleSpace: FlexibleSpaceBar(
        collapseMode: CollapseMode.parallax,
        centerTitle: false,
        background: BlocBuilder<TaskDetailsImageBloc, TaskDetailsImageState>(
          builder: (context, state) {
            if (state is TitleImageLoading) {
              return SingleChildScrollView(
                child: Container(
                  margin: const EdgeInsets.only(
                      left: 16.0, right: 16.0, top: 100.0),
                  child: Stack(
                    alignment: const Alignment(1.0, 1.0),
                    children: <Widget>[
                      TaskTitle(_taskTitle),
                      SizedBox(
                        height: 25,
                        width: 25,
                        child: CircularProgressIndicator(
                          valueColor:
                              AlwaysStoppedAnimation<Color>(Colors.white),
                        ),
                      ),
                    ],
                  ),
                ),
              );
            }
            if (state is TitleImageLoaded) {
              return Container(
                decoration: BoxDecoration(
                  image: DecorationImage(
                    image: FileImage(File(state.imagePath)),
                    fit: BoxFit.cover,
                  ),
                ),
                child: SingleChildScrollView(
                  child: Container(
                    margin: const EdgeInsets.only(
                        left: 16.0, right: 16.0, top: 100.0),
                    child: Column(
                      children: <Widget>[
                        TaskTitle(_taskTitle),
                      ],
                    ),
                  ),
                ),
              );
            }
            if (state is TitleImageNotLoaded) {
              return SingleChildScrollView(
                child: Container(
                  margin: const EdgeInsets.only(
                      left: 16.0, right: 16.0, top: 100.0),
                  child: Column(
                    children: <Widget>[
                      TaskTitle(_taskTitle),
                    ],
                  ),
                ),
              );
            }

            return SingleChildScrollView(
              child: Container(
                margin:
                    const EdgeInsets.only(left: 16.0, right: 16.0, top: 100.0),
                child: Stack(
                  alignment: const Alignment(1.0, 1.0),
                  children: <Widget>[
                    TaskTitle(_taskTitle),
                    SizedBox(
                      height: 25,
                      width: 25,
                      child: CircularProgressIndicator(
                        valueColor: AlwaysStoppedAnimation<Color>(Colors.white),
                      ),
                    ),
                  ],
                ),
              ),
            );
          },
        ),
      ),
    );
  }
}

class TaskDetailsScreen extends StatefulWidget {
  final Task _initialTask;

  TaskDetailsScreen(this._initialTask);

  @override
  _TaskDetailsScreenState createState() => _TaskDetailsScreenState();
}

class _TaskDetailsScreenState extends State<TaskDetailsScreen> {
  AdditionalThemeData _additionalThemeData;

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    _additionalThemeData = CustomTheme.instanceOf(context).additionalThemeData;
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        FocusScope.of(context).unfocus();
      },
      child: Scaffold(
        backgroundColor: Colors.transparent,
        body: MultiBlocListener(
          listeners: [
            BlocListener<TaskDetailsDateBloc, TaskDetailsDateState>(
                listener: (context, state) {
              if (state is DueDateChanged) {
                Scaffold.of(context).showSnackBar(
                  SnackBar(
                    backgroundColor: Colors.green,
                    content: Text('Task due date was updated'),
                  ),
                );
              }
            }),
            BlocListener<TaskDetailsTitleBloc, TaskDetailsTitleState>(
                listener: (context, state) {
              if (state is TitleChanged) {
                Scaffold.of(context).showSnackBar(
                  SnackBar(
                    backgroundColor: Colors.green,
                    content: Text('Task title was updated'),
                  ),
                );
              }
            }),
          ],
          child: CustomScrollView(
            slivers: <Widget>[
              BlocProvider<TaskDetailsImageBloc>(
                builder: (context) {
                  return TaskDetailsImageBloc(
                      tasksRepository:
                          RepositoryProvider.of<TaskRepository>(context),
                      taskId: widget._initialTask.id)
                    ..dispatch(TitleImageLoad(widget._initialTask.imageUrl));
                },
                child:
                    BlocListener<TaskDetailsImageBloc, TaskDetailsImageState>(
                        listener: (context, state) {
                          final imageBloc =
                              BlocProvider.of<TaskDetailsImageBloc>(context);

                          if (state is TitleImagePickerShowing) {
                            FilePicker.getFilePath(type: FileType.IMAGE)
                                .then((filePath) {
                              imageBloc.dispatch(TitleImageChange(filePath));
                            });
                          }
                        },
                        child:
                            ChangeableImageAppBar(widget._initialTask.title)),
              ),
              SliverList(
                delegate: SliverChildListDelegate([
                  Container(
                    margin: const EdgeInsets.only(
                        left: 16.0, right: 16.0, top: 16.0),
                    child: Row(
                      children: <Widget>[
                        Container(
                          child: Column(
                            children: <Widget>[
                              EventDate(widget._initialTask.dueDateTime),
                              EventDuration(widget._initialTask.duration),
                            ],
                          ),
                        ),
                        Expanded(
                          child: Align(
                            alignment: Alignment.centerRight,
                            child: BlocBuilder<TaskCompletionBloc,
                                TaskCompletionState>(builder: (context, state) {
                              if (state is TaskCompletionLoading) {
                                return LoadingIndicator.color(0);
                              }
                              if (state is TaskCompletionLoaded) {
                                return CircularPercentIndicator(
                                  radius: 75.0,
                                  lineWidth: 10.0,
                                  percent: state.completion,
                                  animation: true,
                                  center: new Text(
                                    (state.completion * 100)
                                            .toStringAsFixed(1) +
                                        '%',
                                    style: TextStyle(
                                      fontFamily: 'Montserrat',
                                    ),
                                  ),
                                  progressColor: _additionalThemeData
                                      .circularPercentIndicatorProgressColor,
                                  backgroundColor: _additionalThemeData
                                      .circularPercentIndicatorBackgroundColor,
                                );
                              }

                              return LoadingIndicator.color(0);
                            }),
                          ),
                        ),
                      ],
                    ),
                  ),
                  Container(
                    margin: const EdgeInsets.only(
                        left: 8.0, right: 16.0, top: 16.0),
                    child: Column(
                      children: <Widget>[
                        SectionHeader('Subtasks', 8),
                        SubTaskList(),
                        AddSubTaskWidget('Add new task here'),
                      ],
                    ),
                  ),
                  Container(
                    margin: const EdgeInsets.only(
                        left: 16.0, right: 16.0, top: 16.0, bottom: 32.0),
                    child: Column(
                      children: <Widget>[
//                      SectionHeader('Participants', 0),
//                      Row(
//                        children: <Widget>[
//                          ParticipantWidget.withPicture(
//                              'Marta', 'images/users/female.jpg'),
//                          ParticipantWidget.withPicture(
//                              'Walt', 'images/users/male.jpg'),
//                          ParticipantWidget.empty(),
//                        ],
//                      ),
                      ],
                    ),
                  ),
                ]),
              ),
            ],
          ),
        ),
      ),
    );
  }
}

class TaskTitle extends StatefulWidget {
  final String initialTitle;

  TaskTitle(this.initialTitle);

  @override
  _TaskTitleState createState() => _TaskTitleState();
}

class _TaskTitleState extends State<TaskTitle> {
  final _textEditingController = TextEditingController();
  final FocusNode _node = FocusNode(debugLabel: 'Title');

  @override
  void initState() {
    super.initState();
    _textEditingController.text = widget.initialTitle;
  }

  Widget createTitle(TextEditingController controller,
      TaskDetailsTitleBloc bloc, FocusNode node) {
    return Padding(
      padding: const EdgeInsets.only(bottom: 16.0),
      child: Align(
        alignment: Alignment.centerLeft,
        child: TextField(
          decoration: InputDecoration(
            contentPadding: const EdgeInsets.all(0.0),
            border: InputBorder.none,
            hintText: null,
          ),
          style: TextStyle(
            color: Colors.white,
            fontFamily: 'Montserrat',
            fontStyle: FontStyle.normal,
            fontSize: 36,
            fontWeight: FontWeight.w700,
            shadows: <Shadow>[
              Shadow(
                offset: Offset(0.0, 5.0),
                blurRadius: 20.0,
                color: Color.fromARGB(255, 0, 0, 0),
              ),
            ],
          ),
          maxLines: null,
          autocorrect: true,
          enableInteractiveSelection: true,
          keyboardType: TextInputType.text,
          textInputAction: TextInputAction.done,
          focusNode: node,
          controller: _textEditingController,
          onSubmitted: (newValue) {
            node.unfocus();
            bloc.dispatch(TitleChange(newValue));
          },
          onTap: () {
            if (!node.hasFocus) node.requestFocus();
          },
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<TaskDetailsTitleBloc, TaskDetailsTitleState>(
        builder: (context, state) {
      final detailsTitleBloc = BlocProvider.of<TaskDetailsTitleBloc>(context);

      if (state is TitleChanging) {
        return createTitle(_textEditingController, detailsTitleBloc, _node);
      }

      if (state is TitleChanged) {
        //_textEditingController.text = state.title;
        return createTitle(_textEditingController, detailsTitleBloc, _node);
      }

      if (state is TitleNotChanged) {
        _textEditingController.text = widget.initialTitle;
        return createTitle(_textEditingController, detailsTitleBloc, _node);
      }

      return createTitle(_textEditingController, detailsTitleBloc, _node);
    });
  }

  @override
  void dispose() {
    _node.dispose();
    _textEditingController.dispose();
    super.dispose();
  }
}

class EventDate extends StatefulWidget {
  final DateTime _initialDueDate;

  EventDate(this._initialDueDate);

  @override
  _EventDateState createState() => _EventDateState();
}

class _EventDateState extends State<EventDate> {
  DateTime _dueDateState;
  TaskDetailsDateBloc _taskDetailsDateBloc;

  @override
  void initState() {
    super.initState();
    _dueDateState = widget._initialDueDate;
    _taskDetailsDateBloc = BlocProvider.of<TaskDetailsDateBloc>(context);
  }

  Widget createDueDate(DateTime dueDate) {
    return Padding(
      padding: const EdgeInsets.fromLTRB(0, 4, 0, 4),
      child: Align(
        alignment: Alignment.centerLeft,
        child: Text(
          DateFormat.yMMMMd("en_US").format(dueDate),
          style: TextStyle(
            color: Colors.white,
            fontFamily: 'Montserrat',
            fontStyle: FontStyle.normal,
            fontSize: 14,
            fontWeight: FontWeight.w400,
          ),
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        _taskDetailsDateBloc.dispatch(DueDateDialogShow(_dueDateState));
      },
      child: BlocListener<TaskDetailsDateBloc, TaskDetailsDateState>(
        listener: (context, state) {
          if (state is DueDateDialogShowing) {
            showDatePicker(
                    context: context,
                    initialDate: _dueDateState,
                    firstDate: DateTime(2018),
                    lastDate: DateTime(2020))
                .then((picked) {
              _taskDetailsDateBloc.dispatch(DueDateChange(picked));
            });
          }
        },
        child: BlocBuilder<TaskDetailsDateBloc, TaskDetailsDateState>(
            builder: (context, state) {
          if (state is DueDateChanging) {
            return LoadingIndicator.color(0);
          }

          if (state is DueDateDialogShowing) {
            return createDueDate(_dueDateState);
          }

          if (state is DueDateChanged) {
            _dueDateState = state.date;
            return createDueDate(state.date);
          }

          if (state is DueDateNotChanged) {
            return createDueDate(_dueDateState);
          }
          return createDueDate(_dueDateState);
        }),
      ),
    );
  }
}

class EventDuration extends StatefulWidget {
  final int _initialDuration;

  EventDuration(this._initialDuration);

  @override
  _EventDurationState createState() => _EventDurationState();
}

class _EventDurationState extends State<EventDuration> {
  int _durationState;
  TaskDetailsArguments _args;

  _EventDurationState();

  @override
  void initState() {
    super.initState();
    _durationState = widget._initialDuration;
  }

  @override
  didChangeDependencies() {
    super.didChangeDependencies();
    _args = ModalRoute.of(context).settings.arguments;
  }

  Widget _buildSlider({TaskDetailsDurationSliderBloc bloc, int duration}) {
    int selectedValue = duration;
    return Column(
      children: <Widget>[
        Slider.adaptive(
          onChanged: (newValue) {
            selectedValue = newValue.round();

            bloc.dispatch(DurationSliderRebuild(
                newDuration: selectedValue, oldDuration: duration));
          },
          value: selectedValue.toDouble(),
          max: 600,
          min: 0,
          divisions: 40,
        ),
        Text('Event will take: '
            '$selectedValue minutes'),
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    return BlocProvider<TaskDetailsDurationBloc>(
      builder: (context) {
        return TaskDetailsDurationBloc(
            taskId: _args._taskId,
            taskRepository: RepositoryProvider.of<TaskRepository>(context));
      },
      child: BlocListener<TaskDetailsDurationBloc, TaskDetailsDurationState>(
        listener: (context, state) {
          TaskDetailsDurationBloc bloc =
              BlocProvider.of<TaskDetailsDurationBloc>(context);
          if (state is DurationDialogShowing) {
            int newDuration;
            int oldDuration = _durationState;
            bool isOk;

            showDialog<int>(
              context: context,
              builder: (context) {
                return AlertDialog(
                  title: Text('Set up the task duration'),
                  actions: <Widget>[
                    FlatButton(
                        child: Text('Cancel'),
                        onPressed: () {
                          Navigator.pop(context);
                        }),
                    FlatButton(
                        child: Text('Ok'),
                        onPressed: () {
                          isOk = true;
                          Navigator.pop(context, newDuration);
                        }),
                  ],
                  content: SingleChildScrollView(
                    child: Container(
                      alignment: Alignment.topLeft,
                      child: BlocProvider<TaskDetailsDurationSliderBloc>(
                        builder: (context) {
                          return TaskDetailsDurationSliderBloc();
                        },
                        child: BlocBuilder<TaskDetailsDurationSliderBloc,
                                TaskDetailsDurationSliderState>(
                            builder: (BuildContext context, state) {
                          final bloc =
                              BlocProvider.of<TaskDetailsDurationSliderBloc>(
                                  context);
                          if (state is DurationSliderRebuilt) {
                            oldDuration = state.oldDuration;
                            newDuration = state.newDuration;
                            return _buildSlider(
                                bloc: bloc, duration: state.newDuration);
                          }
                          return _buildSlider(
                              bloc: bloc, duration: _durationState);
                        }),
                      ),
                    ),
                  ),
                );
              },
            ).then((int returnedValue) {
              bloc.dispatch(DurationChange(
                  newDuration: newDuration,
                  oldDuration: oldDuration,
                  isOk: isOk));
            });
          }

          if (state is DurationChanged) {
            Scaffold.of(context).showSnackBar(
              SnackBar(
                backgroundColor: Colors.green,
                content: Text('Task duration changed'),
              ),
            );
          }
        },
        child: BlocBuilder<TaskDetailsDurationBloc, TaskDetailsDurationState>(
          builder: (BuildContext context, state) {
            final bloc = BlocProvider.of<TaskDetailsDurationBloc>(context);
            if (state is DurationChanged) {
              _durationState = state.duration;
              return createDurationWidget(bloc, state.duration);
            }
            if (state is DurationChanging) {
              return LoadingIndicator.color(0);
            }
            if (state is DurationNotChanged) {
              return createDurationWidget(bloc, widget._initialDuration);
            }
            if (state is DurationDialogShowing) {
              return createDurationWidget(bloc, state.duration);
            }
            return createDurationWidget(bloc, widget._initialDuration);
          },
        ),
      ),
    );
  }

  Widget createDurationWidget(TaskDetailsDurationBloc bloc, int duration) {
    return GestureDetector(
      onTap: () => bloc.dispatch(DurationDialogShow(duration)),
      child: Padding(
        padding: const EdgeInsets.fromLTRB(0, 4, 0, 4),
        child: Align(
          alignment: Alignment.centerLeft,
          child: Text(
            duration.toString() + ' minutes',
            style: TextStyle(
              color: Colors.white,
              fontFamily: 'Montserrat',
              fontStyle: FontStyle.normal,
              fontSize: 14,
              fontWeight: FontWeight.w400,
            ),
          ),
        ),
      ),
    );
  }
}

class SubTaskList extends StatefulWidget {
  @override
  _SubTaskListState createState() => _SubTaskListState();
}

class _SubTaskListState extends State<SubTaskList> {
  @override
  Widget build(BuildContext context) {
    return BlocBuilder<SubTasksBloc, SubTasksState>(builder: (context, state) {
      if (state is SubTasksLoading) {
        return LoadingIndicator.color(0);
      }

      if (state is SubTasksLoaded) {
        if (state.subTasks.isEmpty) {
          return Center(
            child: Text('No subtasks'),
          );
        }
        return ListView.builder(
          scrollDirection: Axis.vertical,
          shrinkWrap: true,
          padding: EdgeInsets.zero,
          physics: const NeverScrollableScrollPhysics(),
          itemBuilder: (BuildContext context, int index) {
            return SubTaskItem(state.subTasks[index]);
          },
          itemCount: state.subTasks.length,
        );
      }
      return LoadingIndicator.color(0);
    });
  }
}

class SubTaskItem extends StatefulWidget {
  final SubTask _subTask;

  SubTaskItem(this._subTask);

  @override
  _SubTaskItemState createState() => _SubTaskItemState();
}

class _SubTaskItemState extends State<SubTaskItem> {
  bool _isCompleted;
  var _tapPosition;
  TaskDetailsArguments _args;

  @override
  void initState() {
    super.initState();
  }

  @override
  didChangeDependencies() {
    super.didChangeDependencies();
    _args = ModalRoute.of(context).settings.arguments;
  }

  void _storePosition(TapDownDetails details) {
    _tapPosition = details.globalPosition;
  }

  @override
  Widget build(BuildContext context) {
    _isCompleted = widget._subTask.isCompleted;

    return MultiBlocProvider(
      providers: [
        BlocProvider<TaskDetailsSubTaskBloc>(builder: (context) {
          return TaskDetailsSubTaskBloc(
              taskId: _args._taskId,
              subTaskId: widget._subTask.id,
              taskRepository: RepositoryProvider.of<TaskRepository>(context));
        }),
        BlocProvider<SubTaskEditableBloc>(builder: (context) {
          return SubTaskEditableBloc();
        })
      ],
      child: MultiBlocListener(
        listeners: [
          BlocListener<TaskDetailsSubTaskBloc, TaskDetailsSubTaskState>(
              listener: (context, state) {
            if (state is SubTaskChanged) {
              Scaffold.of(context)
                ..hideCurrentSnackBar()
                ..showSnackBar(
                  SnackBar(
                    backgroundColor: Colors.green,
                    content: Text('Subtask content changed'),
                  ),
                );
            }

            if (state is SubTaskDeletePopUpShowed) {
              Vibration.hasVibrator().then((result) {
                if (result == true) {
                  Vibration.vibrate();
                }
              });

              final RenderBox overlay =
                  Overlay.of(context).context.findRenderObject();

              showMenu(
                position: RelativeRect.fromRect(
                    _tapPosition & Size(40, 40), // smaller rect, the touch area
                    Offset.zero & overlay.size // Bigger rect, the entire screen
                    ),
                context: context,
                items: <PopupMenuEntry<String>>[
                  PopupMenuItem(
                    value: 'DeletePressed',
                    child: Row(
                      children: <Widget>[
                        Icon(Icons.delete),
                        Text("Delete"),
                      ],
                    ),
                  )
                ],
              ).then<void>((String result) {
                var subTaskBloc =
                    BlocProvider.of<TaskDetailsSubTaskBloc>(context);
                if (result != null) {
                  subTaskBloc.dispatch(SubTaskDelete(widget._subTask));
                } else {
                  subTaskBloc.dispatch(SubTaskDeletePopUpHide());
                }
              });
            }

            if (state is SubTaskDeleted) {
              Scaffold.of(context)
                ..hideCurrentSnackBar()
                ..showSnackBar(
                  SnackBar(
                    backgroundColor: Colors.green,
                    content: Text('Subtask deleted'),
                  ),
                );
            }
          }),
        ],
        child: BlocBuilder<TaskDetailsSubTaskBloc, TaskDetailsSubTaskState>(
            builder: (BuildContext context, state) {
          var subTaskBloc = BlocProvider.of<TaskDetailsSubTaskBloc>(context);
          if (state is SubTaskDeleting) return LoadingIndicator.color(0);
          if (state is SubTaskDeleted)
            return Container(
              height: 0.0,
            );

          if (state is SubTaskChanging) return LoadingIndicator.color(0);
          if (state is SubTaskChanged) {
            _isCompleted = state.subTask.isCompleted;
            return _createSubTask(subTaskBloc, false);
          }
          if (state is SubTaskNotChanged) {
            return _createSubTask(subTaskBloc, false);
          }
          if (state is SubTaskDeletePopUpHided) {
            return _createSubTask(subTaskBloc, false);
          }
          return _createSubTask(subTaskBloc, false);
        }),
      ),
    );
  }

  Widget _createSubTask(TaskDetailsSubTaskBloc subTaskBloc, bool stateReset) =>
      Container(
        margin: EdgeInsets.fromLTRB(0, 4, 0, 4),
        child: GestureDetector(
          onHorizontalDragEnd: (details) {
            subTaskBloc.dispatch(SubTaskCompletionChange(!_isCompleted));
          },
          onLongPressStart: (details) {
            subTaskBloc.dispatch(SubTaskDeletePopUpShow());
          },
          onTapDown: _storePosition,
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              Icon(Icons.navigate_next, color: Colors.white),
              Expanded(
                child: Padding(
                  padding: const EdgeInsets.fromLTRB(4, 1, 0, 0),
                  child: Align(
                      alignment: Alignment.centerLeft,
                      child: SubTaskEditable(
                          widget._subTask,
                          stateReset,
                          (_) =>
                              subTaskBloc.dispatch(SubTaskContentChange(_)))),
                ),
              ),
            ],
          ),
        ),
      );
}

class AddSubTaskWidget extends StatefulWidget {
  final String _invitationText;

  AddSubTaskWidget(this._invitationText);

  @override
  _AddSubTaskWidgetState createState() => _AddSubTaskWidgetState();
}

class _AddSubTaskWidgetState extends State<AddSubTaskWidget> {
  final TextEditingController _textEditingController = TextEditingController();
  final FocusNode _node = FocusNode(debugLabel: 'SubTaskInvitation');
  TaskDetailsArguments _args;

  @override
  void initState() {
    super.initState();
    _textEditingController..text = widget._invitationText;
  }

  @override
  didChangeDependencies() {
    super.didChangeDependencies();
    _args = ModalRoute.of(context).settings.arguments;
  }

  Widget _createAddingSubTaskWidget(
      TaskDetailsSubTaskAddBloc subTaskAddBloc,
      FocusNode node,
      TextEditingController controller,
      bool isHintTextCleared) {
    return Container(
      margin: EdgeInsets.fromLTRB(0, 4, 0, 4),
      child: GestureDetector(
        onTap: () {},
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            Icon(Icons.navigate_next,
                color: isHintTextCleared ? Colors.white : Colors.transparent),
            Expanded(
              child: Padding(
                padding: const EdgeInsets.fromLTRB(4, 1, 0, 0),
                child: Align(
                  alignment: Alignment.centerLeft,
                  child: TextField(
                    decoration: InputDecoration(
                      contentPadding: const EdgeInsets.all(0.0),
                      border: InputBorder.none,
                    ),
                    style: TextStyle(
                      color: !isHintTextCleared
                          ? Colors.white.withOpacity(0.4)
                          : Colors.white,
                      fontFamily: 'Montserrat',
                      fontStyle: FontStyle.normal,
                      fontSize: !isHintTextCleared ? 16 : 18,
                      fontWeight: FontWeight.w400,
                    ),
                    maxLines: null,
                    autocorrect: true,
                    enableInteractiveSelection: true,
                    keyboardType: TextInputType.multiline,
                    textInputAction: TextInputAction.done,
                    focusNode: node,
                    controller: controller,
                    onTap: () {
                      node.requestFocus();
                      subTaskAddBloc.dispatch(SubTaskAddHintClear());
                    },
                    onSubmitted: (newValue) {
                      node.unfocus();
                      subTaskAddBloc.dispatch(SubTaskAdd(newValue));
                    },
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return BlocProvider<TaskDetailsSubTaskAddBloc>(
      builder: (context) {
        return TaskDetailsSubTaskAddBloc(
            taskId: _args._taskId,
            taskRepository: RepositoryProvider.of<TaskRepository>(context));
      },
      child:
          BlocListener<TaskDetailsSubTaskAddBloc, TaskDetailsSubTaskAddState>(
        listener: (context, state) {},
        child:
            BlocBuilder<TaskDetailsSubTaskAddBloc, TaskDetailsSubTaskAddState>(
                builder: (BuildContext context, state) {
          var subTaskAddBloc =
              BlocProvider.of<TaskDetailsSubTaskAddBloc>(context);

          if (state is SubTaskHintCleared) {
            _textEditingController.text = '';
            return _createAddingSubTaskWidget(
                subTaskAddBloc, _node, _textEditingController, true);
          }
          if (state is SubTaskAdding) return LoadingIndicator.color(0);
          if (state is SubTaskAdded) {
            _textEditingController.text = widget._invitationText;
            return _createAddingSubTaskWidget(
                subTaskAddBloc, _node, _textEditingController, false);
          }
          if (state is SubTaskNotAdded) {
            _textEditingController.text = widget._invitationText;
            return _createAddingSubTaskWidget(
                subTaskAddBloc, _node, _textEditingController, false);
          }
          return _createAddingSubTaskWidget(
              subTaskAddBloc, _node, _textEditingController, false);
        }),
      ),
    );
  }

  @override
  void dispose() {
    _node.dispose();
    _textEditingController.dispose();
    super.dispose();
  }
}

class SectionHeader extends StatelessWidget {
  final String _headerTitle;
  final double _leftPadding;

  SectionHeader(this._headerTitle, this._leftPadding);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.fromLTRB(_leftPadding, 4, 0, 8),
      child: Align(
        alignment: Alignment.centerLeft,
        child: Text(
          _headerTitle,
          style: TextStyle(
            color: Colors.white,
            fontFamily: 'Montserrat',
            fontStyle: FontStyle.normal,
            fontSize: 18,
            fontWeight: FontWeight.w700,
          ),
        ),
      ),
    );
  }
}
