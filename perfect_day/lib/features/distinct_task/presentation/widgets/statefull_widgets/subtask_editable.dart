import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:keyboard_visibility/keyboard_visibility.dart';

import '../../../../../core/data/repositories/task_repository/task_repository.dart';
import '../../../blocs/blocs.dart';

class SubTaskEditable extends StatefulWidget {
  final SubTask _subTask;
  final bool _isFocused;
  final void Function(String textContent) _onEditDone;

  SubTaskEditable(this._subTask, this._isFocused, this._onEditDone);

  @override
  _SubTaskEditableState createState() => _SubTaskEditableState();
}

class _SubTaskEditableState extends State<SubTaskEditable> {
  final _textEditingController = TextEditingController();
  final _node = FocusNode();
  bool _isEditable;
  bool _isTextLined;
  bool _isFocused;
  //Offset _tapPosition;

  @override
  void initState() {
    super.initState();
    _isEditable = false;

    KeyboardVisibilityNotification().addNewListener(
      onChange: (bool visible) {
        if (_isEditable == true && visible == false) {
          BlocProvider.of<SubTaskEditableBloc>(context)
            ..dispatch(SubTaskEditableRebuild(false));
        }
      },
    );
  }

//  void _storePosition(TapDownDetails details) {
//    _tapPosition = details.globalPosition;
//  }

  void getFocus() {
    if (_isEditable == true) {
      _node.requestFocus();
      _textEditingController.selection =
          TextSelection.collapsed(offset: _textEditingController.text.length);
    }
  }

  Widget _realEditableBuild() {
    return GestureDetector(
      onTap: () {
        _isEditable = true;
        WidgetsBinding.instance.addPostFrameCallback((_) => getFocus());
        BlocProvider.of<SubTaskEditableBloc>(context)
          ..dispatch(SubTaskEditableRebuild(true));
      },
      // onTapDown: _storePosition,
      child: Container(
        child: _isEditable == true
            ? TextField(
                decoration: InputDecoration(
                  contentPadding: const EdgeInsets.all(0.0),
                  border: InputBorder.none,
                  hintText: null,
                ),
                style: TextStyle(
                  color: Colors.white,
                  fontFamily: 'Montserrat',
                  fontStyle: FontStyle.normal,
                  fontSize: 18,
                  fontWeight: FontWeight.w400,
                  decoration: _isTextLined
                      ? TextDecoration.lineThrough
                      : TextDecoration.none,
                ),
                maxLines: null,
                autocorrect: true,
                keyboardType: TextInputType.multiline,
                textInputAction: TextInputAction.done,
                controller: _textEditingController,
                focusNode: _node,
                onSubmitted: (newValue) {
                  widget._onEditDone(newValue);

//                  setState(() {
//                    isEditable = false;
//                  });
                },
              )
            : Text(
                _textEditingController.text,
                style: TextStyle(
                  color: Colors.white,
                  fontFamily: 'Montserrat',
                  fontStyle: FontStyle.normal,
                  fontSize: 18,
                  fontWeight: FontWeight.w400,
                  decoration: _isTextLined
                      ? TextDecoration.lineThrough
                      : TextDecoration.none,
                ),
              ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    _isTextLined = widget._subTask.isCompleted;
    _textEditingController.text = widget._subTask.content;
    _isFocused = widget._isFocused;

    return BlocBuilder<SubTaskEditableBloc, SubTaskEditableState>(
        builder: (BuildContext context, state) {
      if (state is SubTaskEditableWasRebuild) {
        _isEditable = state.isEditable;
      }

      return _realEditableBuild();
    });
  }

  @override
  void dispose() {
    _node.dispose();
    _textEditingController.dispose();
    super.dispose();
  }
}
